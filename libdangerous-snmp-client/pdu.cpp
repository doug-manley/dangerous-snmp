#include "dangerous/snmp/pdu.hpp"

#include "string-functions.hpp"

#include <iostream>

namespace dangerous { namespace snmp {

/**
 * Default constructor.
 * This creates a PDU with no VarBinds.
 **/
PDU::PDU()
	: requestId(0), errorStatus(0), errorIndex(0)
{
}

/**
 * Copy constructor.
 * This creates a PDU that is an exact copy of the specified PDU.
 * @param pdu The PDU to copy.
 **/
PDU::PDU( const PDU& pdu ) {
	requestId = pdu.requestId;
	errorStatus = pdu.errorStatus;
	errorIndex = pdu.errorIndex;

	for( const auto& varbind : pdu.varbinds ) {
		varbinds.push_back( std::unique_ptr<VarBind>( new VarBind( *varbind.get() ) ) );
	}
}

/**
 * Destructor.
 **/
PDU::~PDU() {
}

/**
 * This adds a new VarBind to the PDU (with a null value).
 * @param oid The OID to add.
 **/
void PDU::addOid( const NumericOid& oid ) {
	varbinds.push_back( std::unique_ptr<VarBind>( new VarBind( oid ) ) );
}

/**
 * This checks to see if the PDU contains the given OID.
 * @param oid The OID to look for.
 * @return true if it is present, false if it is not.
 **/
bool PDU::containsOid( const NumericOid& oid ) {
	// Go through the VarBinds in the PDU and check to see if any of them
	// match the OID that we have been asked to search for.
	for( const auto& varbind : varbinds ) {
		if( varbind->oid.equals( oid ) ) {
			// The OID is present.  We may return success.
			return true;
		}
	}

	// We could not find the OID in the VarBinds for the PDU.
	return false;
}

/**
 * This returns a pointer to a statically allocated string
 * that contains the standard name for the error status
 * given.
 * @param errorStatus The error status value.
 * @return A pointer to the name of the value.
 **/
const char* PDUErrorStatus::name( int errorStatus ) {
	switch( errorStatus ) {
		case PDUErrorStatus::noError:
			return "noError";
		case PDUErrorStatus::tooBig:
			return "tooBig";
		case PDUErrorStatus::noSuchName: //< Only for version-1 compatibility.
			return "noSuchName";
		case PDUErrorStatus::badValue: //< Only for version-1 compatibility.
			return "badValue";
		case PDUErrorStatus::readOnly: //< Only for version-1 compatibility.
			return "readOnly";
		case PDUErrorStatus::genErr:
			return "genErr";
		case PDUErrorStatus::noAccess:
			return "noAccess";
		case PDUErrorStatus::wrongType:
			return "wrongType";
		case PDUErrorStatus::wrongLength:
			return "wrongLength";
		case PDUErrorStatus::wrongEncoding:
			return "wrongEncoding";
		case PDUErrorStatus::wrongValue:
			return "wrongValue";
		case PDUErrorStatus::noCreation:
			return "noCreation";
		case PDUErrorStatus::inconsistentValue:
			return "inconsistentValue";
		case PDUErrorStatus::resourceUnavailable:
			return "resourceUnavailable";
		case PDUErrorStatus::commitFailed:
			return "commitFailed";
		case PDUErrorStatus::undoFailed:
			return "undoFailed";
		case PDUErrorStatus::authorizationError:
			return "authorizationError";
		case PDUErrorStatus::notWritable:
			return "notWritable";
		case PDUErrorStatus::inconsistentName:
			return "inconsistentName";
		default:
			return "unknown error status";
	}
}

/**
 * This prints a PDU to a standard output stream.
 * @param out The standard output stream.
 * @param pdu The PDU to print.
 **/
void printPdu( std::ostream& out, const PDU& pdu ) {
	out << "PDU:" << std::endl;
	out << "requestId: " << pdu.requestId << std::endl;
	out << "errorStatus: " << PDUErrorStatus::name( pdu.errorStatus ) << "(" << pdu.errorStatus << ")" << std::endl;
	out << "errorIndex: " << pdu.errorIndex << std::endl;
	out << "varbinds: " << pdu.varbinds.size() << std::endl;
	for( const auto& varbind : pdu.varbinds ) {
		out << "   ";
		for( const auto& number : varbind->oid.numbers ) {
			out << "." << number;
		}
		out << ": ";
		switch( varbind->value.type() ) {
			/*
			 * ASN.1 native types.
			 */
			case Variant::SIMPLE_INTEGER:
				out << varbind->value.get_int32_t();
				break;
			case Variant::SIMPLE_STRING:
				for( const auto& character : varbind->value.get_string() ) {
					out << character;
				}
				break;
			case Variant::SIMPLE_OID:
				for( const auto& number : varbind->value.get_oid().numbers ) {
					out << "." << number;
				}
				break;
			case Variant::SIMPLE_NULL:
				out << "NULL";
				break;

			/*
			 * SNMP "application" types.
			 */
			case Variant::APPLICATION_IPADDRESS: {
				std::string bytes = varbind->value.get_string();
				for( unsigned int i = 0; i < bytes.length(); i++ ) {
					if( i != 0 ) {
						out << ".";
					}
					out << (int)(uint8_t)bytes[i];
				}
				break;
			}
			case Variant::APPLICATION_COUNTER32:
				out << varbind->value.get_uint32_t();
				break;
			case Variant::APPLICATION_GAUGE32:
				out << varbind->value.get_uint32_t();
				break;
			case Variant::APPLICATION_TIMETICKS: {
				// Timeticks are measured in hundredths of a second.
				// Here, we will be printing out the amount of time in an easily
				// readable format for humans, breaking it down by day, hour, etc.
				
				//! This is the current value of the timeticks.
				//! This will be modified at each pass to remove the part that
				//! was just computed.  For example, when we compute the "days"
				//! in the timeticks, we will then remove that number of hundredths
				//! of a second from the timeticks value so that we can next
				//! compute hours.
				uint32_t timeticks = varbind->value.get_uint32_t();
				//! This is the number of days.
				uint32_t days = timeticks / ( 86400 * 100 );
				timeticks -= days * ( 86400 * 100 );
				//! This is the number of hours.
				uint32_t hours = timeticks / ( 3600 * 100 );
				timeticks -= hours * ( 3600 * 100 );
				//! This is the number of minutes.
				uint32_t minutes = timeticks / ( 60 * 100 );
				timeticks -= minutes * ( 60 * 100 );
				//! This is the number of seconds.
				//! This is a double because we are going to show it as a fractional
				//! value, since timeticks are in hundredths of a second.
				double seconds = (double)timeticks / 100.0;

				out << days << " days, " << hours << " hours, " << minutes << " minutes, " << seconds << " seconds";
				break;
			}
			case Variant::APPLICATION_OPAQUE:
				out << hexadecimal( varbind->value.get_string() );
				break;
			case Variant::APPLICATION_COUNTER64:
				out << varbind->value.get_uint64_t();
				break;

			/*
			 * SNMPv2 VarBind value choices (failure).
			 */
			case Variant::CONTEXT_NOSUCHOBJECT:
				out << "noSuchObject";
				break;
			case Variant::CONTEXT_NOSUCHINSTANCE:
				out << "noSuchInstance";
				break;
			case Variant::CONTEXT_ENDOFMIBVIEW:
				out << "endOfMibView";
				break;
			
			default:
				out << "<unknown>";
		}
		out << std::endl;
	}
}

} }

