#include "dangerous/snmp/securitymodel/usm.hpp"
using namespace dangerous;

#include <iomanip>
#include <iostream>

#include <unittest++/UnitTest++.h>

SUITE(Fast) {

TEST(TestShaPasswordKey) {
	std::string password = "maplesyrup";
	std::string expectedKey1 = { (char)0x9f, (char)0xb5, (char)0xcc, (char)0x03, (char)0x81, (char)0x49, (char)0x7b, (char)0x37, (char)0x93, (char)0x52, (char)0x89, (char)0x39, (char)0xff, (char)0x78, (char)0x8d, (char)0x5d, (char)0x79, (char)0x14, (char)0x52, (char)0x11 };
	std::string engineId = { (char)0x00, (char)0x00, (char)0x00, (char)0x00, (char)0x00, (char)0x00, (char)0x00, (char)0x00, (char)0x00, (char)0x00, (char)0x00, (char)0x02 };
	std::string expectedKey2 = { (char)0x66, (char)0x95, (char)0xfe, (char)0xbc, (char)0x92, (char)0x88, (char)0xe3, (char)0x62, (char)0x82, (char)0x23, (char)0x5f, (char)0xc7, (char)0x15, (char)0x1f, (char)0x12, (char)0x84, (char)0x97, (char)0xb3, (char)0x8f, (char)0x3f };

	snmp::securitymodel::USM usm( "username", snmp::Authentication::SHA, password, snmp::Encryption::NONE, "" );
	usm.authoritativeEngineID( engineId );

	std::cout << std::setw( 2 ) << std::hex;
	for( int i = 0; i < usm.authenticationKey().length(); i++ ) {
		std::cout << (unsigned int)( usm.authenticationKey()[ i ] & 0xFF ) << " ";
	}
	std::cout << std::endl;

	CHECK( usm.authenticationKey().compare( expectedKey2 ) == 0 );
}

}

