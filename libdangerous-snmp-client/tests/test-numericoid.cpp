#include "dangerous/snmp/numericoid.hpp"
using namespace dangerous;

#include <unittest++/UnitTest++.h>

SUITE(Fast) {

TEST(TestNumericOidDefault) {
	snmp::NumericOid oid;

	CHECK( oid.numbers.size() == 0 );
	CHECK( oid.str().compare("") == 0 );
}

TEST(TestNumericOidZero) {
	snmp::NumericOid oid( "0" );

	CHECK( oid.numbers.size() == 1 );
	CHECK( oid.str().compare(".0") == 0 );
}

TEST(TestNumericOidDotZero) {
	snmp::NumericOid oid( ".0" );

	CHECK( oid.numbers.size() == 1 );
	CHECK( oid.str().compare(".0") == 0 );
}

TEST(TestNumericOidOne) {
	snmp::NumericOid oid( "1" );

	CHECK( oid.numbers.size() == 1 );
	CHECK( oid.str().compare(".1") == 0 );
}

TEST(TestNumericOidDotOne) {
	snmp::NumericOid oid( ".1" );

	CHECK( oid.numbers.size() == 1 );
	CHECK( oid.str().compare(".1") == 0 );
}

TEST(TestNumericOidCompare) {
	snmp::NumericOid oid1( ".1.2.3" );
	snmp::NumericOid oid2( ".1.2.3.4" );
	snmp::NumericOid oid3( ".1.2.3.9000" );

	CHECK( oid1.isLessThan( oid2 ) );
	CHECK( oid2.isLessThan( oid3 ) );
	CHECK( oid1.isLessThan( oid3 ) );

	CHECK( ! oid3.isLessThan( oid1 ) );
	CHECK( ! oid3.isLessThan( oid2 ) );

	CHECK( ! oid1.equals( oid2 ) );
	CHECK( ! oid1.equals( oid3 ) );
	CHECK( ! oid2.equals( oid3 ) );
}

TEST(TestNumericOidExample) {
	snmp::NumericOid oid( ".1.3.6.1.2.1.6.13.1.1.0.0.0.0.46038.0.0.0.0.0" );

	CHECK( oid.str().compare( ".1.3.6.1.2.1.6.13.1.1.0.0.0.0.46038.0.0.0.0.0" ) == 0 );
}

}

