#include "messageprocessor-v3.hpp"

#include "dangerous/snmp/exception.hpp"
#include "dangerous/snmp/securitymodel.hpp"
#include "dangerous/snmp/securitymodel/usm.hpp"

#include "asn1/asn1.hpp"
#include "bytestream.hpp"
#include "encoding/encoding.hpp"
#include "encoding/message-v3.hpp"
#include "encoding/usmsecurityparameters.hpp"
#include "encoding/scopedpdu.hpp"

#include "encoder.hpp"
#include "string-functions.hpp"

#include <algorithm>
#include <string>

#include <cstring>

#include <openssl/md5.h>
#if MD5_DIGEST_LENGTH < 12
	#error "MD5_DIGEST_LENGTH must be at least 12 bytes."
#endif
#include <openssl/sha.h>
#if SHA_DIGEST_LENGTH < 12
	#error "SHA_DIGEST_LENGTH must be at least 12 bytes."
#endif
#include <openssl/evp.h>

namespace dangerous { namespace snmp {

/**
 * This generates the string, suitable for use with msgAuthenticationParameters,
 * for the message given by "bytes" using the 16-byte MD5 key given by "authKey".
 * @param authKey The 16-byte MD5 key.
 * @param bytes The bytes to authenticate.
 * @return A string suitable for msgAuthenticationParameters.
 **/
std::string generateMD5AuthenticationParameters( std::string authKey, const std::vector<char>& bytes ) throw( AuthenticationGenerationException ) {
	if( logger.system( Logger::GENERAL ) ) {
		logger.out() << __FUNCTION__ << ": Generating MD5 authentication string for " << bytes.size() << " bytes." << std::endl; 
	}

	unsigned char initialMd5sum[ MD5_DIGEST_LENGTH ] = { 0 };
	unsigned char finalMd5sum[ MD5_DIGEST_LENGTH ] = { 0 };
	MD5_CTX md5context;
	int result = 0;

	// TODO: Ensure that "authKey" is 16 bytes long.

	std::string extendedAuthKey = authKey;
	extendedAuthKey.resize( 64, '\0' );

	std::string K1( 64, 0x00 );
	for( int i = 0; i < 64; i++ ) {
		K1[ i ] = extendedAuthKey[ i ] ^ 0x36;
	}

	std::string K2( 64, 0x00 );
	for( int i = 0; i < 64; i++ ) {
		K2[ i ] = extendedAuthKey[ i ] ^ 0x5C;
	}

	MD5_Init( &md5context );
	result = MD5_Update( &md5context, (unsigned char*)K1.c_str(), 64 );
	if( result == 0 ) {
		throw AuthenticationGenerationException( "Could not compute MD5 sum." );
	}
	result = MD5_Update( &md5context, (unsigned char*)bytes.data(), bytes.size() );
	if( result == 0 ) {
		throw AuthenticationGenerationException( "Could not compute MD5 sum." );
	}

	result = MD5_Final( initialMd5sum, &md5context );
	if( result == 0 ) {
		throw AuthenticationGenerationException( "Could not compute MD5 sum." );
	}

	MD5_Init( &md5context );
	result = MD5_Update( &md5context, (unsigned char*)K2.c_str(), 64 );
	if( result == 0 ) {
		throw AuthenticationGenerationException( "Could not compute MD5 sum." );
	}
	result = MD5_Update( &md5context, initialMd5sum, MD5_DIGEST_LENGTH );
	if( result == 0 ) {
		throw AuthenticationGenerationException( "Could not compute MD5 sum." );
	}

	result = MD5_Final( finalMd5sum, &md5context );
	if( result == 0 ) {
		throw AuthenticationGenerationException( "Could not compute MD5 sum." );
	}

	// Update the authentication parameters.
	std::string authenticationParameters( 12, 0x00 );
	for( int i = 0; i < 12; i++ ) {
		authenticationParameters[ i ] = finalMd5sum[ i ];
	}
	
	if( logger.system( Logger::GENERAL ) ) {
		logger.out() << __FUNCTION__ << ": Generated MD5 authentication string for " << bytes.size() << " bytes." << std::endl; 
		logger.out() << __FUNCTION__ << ": MD5 authentication string: " << hexadecimal( authenticationParameters ) << std::endl; 
	}

	return authenticationParameters;
}

/**
 * This generates the string, suitable for use with msgAuthenticationParameters,
 * for the message given by "bytes" using the 16-byte SHA key given by "authKey".
 * @param authKey The 16-byte SHA key.
 * @param bytes The bytes to authenticate.
 * @return A string suitable for msgAuthenticationParameters.
 **/
std::string generateSHAAuthenticationParameters( std::string authKey, const std::vector<char>& bytes ) {
	if( logger.system( Logger::GENERAL ) ) {
		logger.out() << __FUNCTION__ << ": Generating SHA authentication string for " << bytes.size() << " bytes." << std::endl; 
	}

	unsigned char initialShasum[ SHA_DIGEST_LENGTH ] = { 0 };
	unsigned char finalShasum[ SHA_DIGEST_LENGTH ] = { 0 };
	SHA_CTX shacontext;
	int result = 0;

	// TODO: Ensure that "authKey" is 16 bytes long.

	std::string extendedAuthKey = authKey;
	extendedAuthKey.resize( 64, '\0' );

	std::string K1( 64, 0x00 );
	for( int i = 0; i < 64; i++ ) {
		K1[ i ] = extendedAuthKey[ i ] ^ 0x36;
	}

	std::string K2( 64, 0x00 );
	for( int i = 0; i < 64; i++ ) {
		K2[ i ] = extendedAuthKey[ i ] ^ 0x5C;
	}

	SHA1_Init( &shacontext );
	result = SHA1_Update( &shacontext, (unsigned char*)K1.c_str(), 64 );
	if( result == 0 ) {
		throw AuthenticationGenerationException( "Could not compute SHA sum." );
	}
	result = SHA1_Update( &shacontext, (unsigned char*)bytes.data(), bytes.size() );
	if( result == 0 ) {
		throw AuthenticationGenerationException( "Could not compute SHA sum." );
	}

	result = SHA1_Final( initialShasum, &shacontext );
	if( result == 0 ) {
		throw AuthenticationGenerationException( "Could not compute SHA sum." );
	}

	SHA1_Init( &shacontext );
	result = SHA1_Update( &shacontext, (unsigned char*)K2.c_str(), 64 );
	if( result == 0 ) {
		throw AuthenticationGenerationException( "Could not compute SHA sum." );
	}
	result = SHA1_Update( &shacontext, initialShasum, SHA_DIGEST_LENGTH );
	if( result == 0 ) {
		throw AuthenticationGenerationException( "Could not compute SHA sum." );
	}

	result = SHA1_Final( finalShasum, &shacontext );
	if( result == 0 ) {
		throw AuthenticationGenerationException( "Could not compute SHA sum." );
	}

	// Update the authentication parameters.
	std::string authenticationParameters( 12, 0x00 );
	for( int i = 0; i < 12; i++ ) {
		authenticationParameters[ i ] = finalShasum[ i ];
	}
	
	if( logger.system( Logger::GENERAL ) ) {
		logger.out() << __FUNCTION__ << ": Generated SHA authentication string for " << bytes.size() << " bytes." << std::endl; 
		logger.out() << __FUNCTION__ << ": SHA authentication string: " << hexadecimal( authenticationParameters ) << std::endl; 
	}

	return authenticationParameters;
}

MessageProcessorV3::MessageProcessorV3() {
	_version = SNMPv3;

	// Set the starting message ID to some random (signed) 32-bit number.
	messageId = rand() % 0xFFFFFFFF;

	// Set the random 32-bit integer to some random value.
	myRandomInteger32 = rand()%0xFFFFFFFF;
	
	// Set the random 64-bit integer to some random value.
	myRandomInteger64 = ( ( (uint64_t)rand()%0xFFFFFFFF ) << 32 ) | ( rand()%0xFFFFFFFF );
}

std::vector<char> MessageProcessorV3::encode( const SecurityModel* securityModel, ConfirmedClass::Type confirmedClass, const std::vector<char>& pduBytes ) throw( Exception ) {
	if( securityModel->type() != SecurityModel::USM ) {
		throw Exception( "Incorrect security model." );
	}
	const securitymodel::USM* usm = static_cast<const securitymodel::USM*>( securityModel );

	encoding::SNMPv3Message message;
	// An SNMPv3 message is version "3".
	message.msgVersion = SNMPv3;
	message.msgHeaderData.msgID = ++messageId;
	message.msgHeaderData.msgMaxSize = 1472; //< TODO: Actually determine some real sizing values.

	// Set up the message flags.
	message.msgHeaderData.msgFlags = { 0 };
	// First, set up the "auth" and "priv" bits.
	// Note that "priv" implies "auth".
	if( usm->encryption() != Encryption::NONE ) {
		message.msgHeaderData.msgFlags[ 0 ] = encoding::SNMPv3MessageFlags::AUTHENTICATION | encoding::SNMPv3MessageFlags::ENCRYPTION;
	} else if( usm->authentication() != Authentication::NONE ) {
		message.msgHeaderData.msgFlags[ 0 ] = encoding::SNMPv3MessageFlags::AUTHENTICATION;
	}
	// Then, add in the "reportable" bit.
	// This must be set if the kind of PDU that we are sending is one
	// that expects a response back.
	if( confirmedClass == ConfirmedClass::Confirmed ) {
		message.msgHeaderData.msgFlags[ 0 ] |= encoding::SNMPv3MessageFlags::REPORTABLE;
	}

	// Set up the security information.
	message.msgHeaderData.msgSecurityModel = securityModel->type();

	//! This structure will hold the USM security information.
	//! Note that this may change twice:
	//!    1. For the PDU encryption information.
	//!    2. For the whole-message authentication information.
	encoding::UsmSecurityParameters securityParameters;
	securityParameters.msgAuthoritativeEngineID = usm->authoritativeEngineID();
	securityParameters.msgAuthoritativeEngineBoots = usm->authoritativeEngineBoots();
	securityParameters.msgAuthoritativeEngineTime = usm->authoritativeEngineTime().count();
	securityParameters.msgUserName = usm->username();
	securityParameters.msgAuthenticationParameters = "";
	if( usm->authentication() == Authentication::MD5 || usm->authentication() == Authentication::SHA ) {
		// USM states that if authentication is being used, then the
		// parameters will be set, initially, to 12 bytes of zeros.
		//
		// This field will later be replaced (but NOT resized) by the
		// appropriate authentication information.  Essentially, this
		// will be the first 12 bytes of the MD5 or SHA sum.
		securityParameters.msgAuthenticationParameters.resize( 12, 0x00 );
	}
	securityParameters.msgPrivacyParameters = "";

	encoding::ScopedPDU scopedPdu;
	scopedPdu.contextEngineID = ""; //< TODO
	scopedPdu.contextName = ""; //< TODO
	scopedPdu.pduBytes = pduBytes;

	std::vector<char> scopedPduBytes = Encoder::encodeToVector<encoding::SCOPEDPDU>( scopedPdu );

	uint32_t myEngineBoots = 9; //< TODO: Keep track of this for real; see http://tools.ietf.org/html/rfc2574

	switch( usm->encryption() ) {
		case Encryption::NONE:
			securityParameters.msgPrivacyParameters = "";
			message.pduBytes = scopedPduBytes;
			break;
		case Encryption::DES: {
			//! The DES key that we're going to use is the first 8 bytes of the encryption key.
			std::string key = usm->encryptionKey().substr( 0, 8 );

			//! The final 8 bytes are going to be used as the pre-initialization vector.
			std::string preInitializationVector = usm->encryptionKey().substr( 8, 8 );

			//! This is the "salt" that we will be using.  Since the salt is used to create
			//! the initialization vector, we have a goal to make the salt _different_ for
			//! _each_ message that we send (with the same encryption key).  To to this, we
			//! are going to use an 8-byte value that is the engine boots combined with an
			//! incrementing (random) integer.
			std::string salt( 8, 0x00 );
			salt[ 0 ] = ( myEngineBoots >> 24 ) & 0xFF;
			salt[ 1 ] = ( myEngineBoots >> 16 ) & 0xFF;
			salt[ 2 ] = ( myEngineBoots >> 8 ) & 0xFF;
			salt[ 3 ] = ( myEngineBoots >> 0 ) & 0xFF;
			salt[ 4 ] = ( myRandomInteger32 >> 24 ) & 0xFF;
			salt[ 5 ] = ( myRandomInteger32 >> 16 ) & 0xFF;
			salt[ 6 ] = ( myRandomInteger32 >> 8 ) & 0xFF;
			salt[ 7 ] = ( myRandomInteger32 >> 0 ) & 0xFF;
			myRandomInteger32++;
			
			//! This is the intialization vector; it is built by XORing the pre-initialization
			//! vector with the salt.
			std::string initializationVector( 8, 0x00 );
			for( int i = 0; i < 8; i++ ) {
				initializationVector[ i ] = preInitializationVector[ i ] ^ salt[ i ];
			}

			EVP_CIPHER_CTX cypherContext;
			EVP_CIPHER_CTX_init( &cypherContext );

			EVP_EncryptInit_ex( &cypherContext, EVP_des_cbc(), NULL /* default implementation */, (unsigned char*)key.c_str(), (unsigned char*)initializationVector.c_str() );

			//! This is how large of a buffer we are going to allocate for storing the encrypted data.
			int encryptedDataSize = scopedPduBytes.size() * 2 + EVP_MAX_BLOCK_LENGTH; //< TODO: Perhaps do this better?
			//! This is the buffer that we're going to use for the encrypted data.
			char encryptedData[ encryptedDataSize ];
			//! This is the _current_ length of "encryptedData"; as we encrypt more data, this will increase.
			int encryptedDataLength = 0; //< Nothing has been encrypted, yet.

			//! This is the number of bytes that we wrote on this call.
			int bytesWritten = 0; //< Obviously, we haven't written anything yet.
			int result = EVP_EncryptUpdate( &cypherContext, (unsigned char*)( encryptedData + encryptedDataLength ), &bytesWritten, (unsigned char*)scopedPduBytes.data(), scopedPduBytes.size() );
			if( result != 1 ) {
				throw EncryptionException( "Could not encrypt the data." );
			}
			encryptedDataLength += bytesWritten;

			bytesWritten = 0;
			result = EVP_EncryptFinal_ex( &cypherContext, (unsigned char*)( encryptedData + encryptedDataLength ), &bytesWritten );
			if( result != 1 ) {
				throw EncryptionException( "Could not encrypt the data." );
			}
			encryptedDataLength += bytesWritten;

			EVP_CIPHER_CTX_cleanup( &cypherContext );
						
			if( logger.system( Logger::GENERAL ) ) {
				logger.out() << "Encrypted the ScopedPDU into " << encryptedDataLength << " bytes." << std::endl;
			}

			//! This string will hold the encrypted data.  It is important to have this as
			//! a standard string because we have the ability to _encode_ that string.
			std::string encryptedString( encryptedData, encryptedDataLength );

			// Set the message's PDU bytes to the _encoded_ version of the encrypted data.
			message.pduBytes = Encoder::encodeToVector<encoding::OCTET_STRING>( encryptedString );
			securityParameters.msgPrivacyParameters = salt;
			break;
		}
		case Encryption::AES: {
			//! The AES key that we're going to use is the first 16 bytes of the encryption key.
			//! (That is, all of them.)
			std::string key = usm->encryptionKey().substr( 0, 16 );

			//! This is the intialization vector; it is built by concatenating:
			//!    1. The authoritative SNMP engine "boots".
			//!    2. The authoritative SNMP engine "time".
			//!    3. A random 64-bit integer.
			std::string initializationVector( 16, 0x00 );
			initializationVector[ 0 ] = ( usm->authoritativeEngineBoots() >> 24 ) & 0xFF;
			initializationVector[ 1 ] = ( usm->authoritativeEngineBoots() >> 16 ) & 0xFF;
			initializationVector[ 2 ] = ( usm->authoritativeEngineBoots() >> 8 ) & 0xFF;
			initializationVector[ 3 ] = ( usm->authoritativeEngineBoots() >> 0 ) & 0xFF;
			initializationVector[ 4 ] = ( usm->authoritativeEngineTime().count() >> 24 ) & 0xFF;
			initializationVector[ 5 ] = ( usm->authoritativeEngineTime().count() >> 16 ) & 0xFF;
			initializationVector[ 6 ] = ( usm->authoritativeEngineTime().count() >> 8 ) & 0xFF;
			initializationVector[ 7 ] = ( usm->authoritativeEngineTime().count() >> 0 ) & 0xFF;
			for( int i = 0; i < 8; i++ ) {
				initializationVector[ 8 + i ] = ( myRandomInteger64 >> ( (8-1-i)*8 ) ) & 0xFF;
			}
			myRandomInteger64++;
			
			securityParameters.msgPrivacyParameters = initializationVector.substr( 8, 8 );

			EVP_CIPHER_CTX cypherContext;
			EVP_CIPHER_CTX_init( &cypherContext );

			EVP_EncryptInit_ex( &cypherContext, EVP_aes_128_cfb128(), NULL /* default implementation */, (unsigned char*)key.c_str(), (unsigned char*)initializationVector.c_str() );

			//! This is how large of a buffer we are going to allocate for storing the encrypted data.
			int encryptedDataSize = scopedPduBytes.size() * 2 + EVP_MAX_BLOCK_LENGTH; //< TODO: Perhaps do this better?
			//! This is the buffer that we're going to use for the encrypted data.
			char encryptedData[ encryptedDataSize ];
			//! This is the _current_ length of "encryptedData"; as we encrypt more data, this will increase.
			int encryptedDataLength = 0; //< Nothing has been encrypted, yet.

			//! This is the number of bytes that we wrote on this call.
			int bytesWritten = 0; //< Obviously, we haven't written anything yet.
			int result = EVP_EncryptUpdate( &cypherContext, (unsigned char*)( encryptedData + encryptedDataLength ), &bytesWritten, (unsigned char*)scopedPduBytes.data(), scopedPduBytes.size() );
			if( result != 1 ) {
				throw EncryptionException( "Could not encrypt the data." );
			}
			encryptedDataLength += bytesWritten;

			bytesWritten = 0;
			result = EVP_EncryptFinal_ex( &cypherContext, (unsigned char*)( encryptedData + encryptedDataLength ), &bytesWritten );
			if( result != 1 ) {
				throw EncryptionException( "Could not encrypt the data." );
			}
			encryptedDataLength += bytesWritten;

			EVP_CIPHER_CTX_cleanup( &cypherContext );
						
			if( logger.system( Logger::GENERAL ) ) {
				logger.out() << "Encrypted the ScopedPDU into " << encryptedDataLength << " bytes." << std::endl;
			}

			//! This string will hold the encrypted data.  It is important to have this as
			//! a standard string because we have the ability to _encode_ that string.
			std::string encryptedString( encryptedData, encryptedDataLength );

			// Set the message's PDU bytes to the _encoded_ version of the encrypted data.
			message.pduBytes = Encoder::encodeToVector<encoding::OCTET_STRING>( encryptedString );
			break;
		}
	}


	if( logger.system( Logger::GENERAL ) ) {
		logger.out() << "MessageProcessorV3::" << __FUNCTION__ << ": About to encode 'msgSecurityParameters'." << std::endl;
	}
	message.msgSecurityParameters = Encoder::encodeToString<encoding::USMSECURITYPARAMETERS>( securityParameters ) ;
	if( logger.system( Logger::GENERAL ) ) {
		logger.out() << "MessageProcessorV3::" << __FUNCTION__ << ": msgSecurityParameters(" << message.msgSecurityParameters.size() << "): " << hexadecimal( message.msgSecurityParameters ) << std::endl;
	}


	// Once the entire message has been constructed, we may now authenticate
	// the message.  This is done by taking the MD5 or SHA sum of the whole
	// thing (including the 12 zero bytes for the authentication.  That sum
	// is then copied into the 12 zero bytes for the authentication.
	if( usm->authentication() != Authentication::NONE ) {
		//! This is the byte buffer that we're going to write our message to.
		std::vector<char> bytes = Encoder::encodeToVector<encoding::SNMPV3MESSAGE>( message );

		if( usm->authentication() == Authentication::MD5 ) {
			if( logger.system( Logger::GENERAL ) ) {
				logger.out() << "Performing MD5 sum on the message (length=" << bytes.size() << ")." << std::endl;
			}

			// Compute the proper value of the authentication parameters; this will need
			// to be inserted back into the message once we know it.
			securityParameters.msgAuthenticationParameters = generateMD5AuthenticationParameters( usm->authenticationKey(), bytes );
				
			// Now, update the message with the new security parameters.
			message.msgSecurityParameters = Encoder::encodeToString<encoding::USMSECURITYPARAMETERS>( securityParameters );
		} else if( usm->authentication() == Authentication::SHA ) {
			if( logger.system( Logger::GENERAL ) ) {
				logger.out() << "Performing SHA sum on the message (length=" << bytes.size() << ")." << std::endl;
			}

			// Compute the proper value of the authentication parameters; this will need
			// to be inserted back into the message once we know it.
			securityParameters.msgAuthenticationParameters = generateSHAAuthenticationParameters( usm->authenticationKey(), bytes );
				
			// Now, update the message with the new security parameters.
			message.msgSecurityParameters = Encoder::encodeToString<encoding::USMSECURITYPARAMETERS>( securityParameters );
		} else {
			throw Exception( "Unsupported authentication mechansim." );
		}
	}

	return Encoder::encodeToVector<encoding::SNMPV3MESSAGE>( message );
}

std::unique_ptr<ByteStream> MessageProcessorV3::decode( const SecurityModel* securityModel, ByteStream& byteStream, std::unique_ptr<SecurityModel>& incomingSecurityModel ) throw( Exception ) {
	encoding::SNMPv3Message message;
	incomingSecurityModel.reset();

	BerStream berStream( &byteStream );

	bool success = berStream.read<encoding::SNMPV3MESSAGE>( message );
	if( ! success ) {
		throw Exception( "Could not decode the SNMPv3 message." );
	}

	if( message.msgVersion != SNMPv3 ) {
		if( logger.system( Logger::GENERAL ) ) {
			logger.out() << "Incorrect version: " << message.msgVersion << std::endl;
		}
		throw Exception( "Invalid version." );
	}

	if( message.msgHeaderData.msgSecurityModel != securityModel->type() ) {
		if( logger.system( Logger::GENERAL ) ) {
			logger.out() << "Message security model (" << message.msgHeaderData.msgSecurityModel << ") is not what was expected (" << securityModel->type() << ")." << std::endl;
		}
		throw Exception( "Invalid security model." );
	}

	//! This is the byte stream that we'll be using to store the PDU portion of the message.
	ByteStream scopedPduByteStream;

	switch( securityModel->type() ) {
		case SecurityModel::USM: {
			const securitymodel::USM* usm = static_cast<const securitymodel::USM*>( securityModel );
			
			ByteStream parametersByteStream;
			parametersByteStream.copyFrom( message.msgSecurityParameters, ByteStream::READ_ONLY );

			BerStream parametersStream( &parametersByteStream );
		
			encoding::UsmSecurityParameters securityParameters;
			success = parametersStream.read<encoding::USMSECURITYPARAMETERS>( securityParameters );
			if( ! success ) {
				if( logger.system( Logger::GENERAL ) ) {
					logger.out() << "Could not parse the UsmSecurityParameters." << std::endl;
				}
			}

			// Update the "incoming" security model information.
			securitymodel::USM* incomingUsm = new securitymodel::USM( securityParameters.msgUserName, Authentication::NONE, "", Encryption::NONE, "" ); //< TODO: Copy from "usm"?
			incomingUsm->authoritativeEngineID( securityParameters.msgAuthoritativeEngineID );
			incomingUsm->authoritativeEngineBoots( securityParameters.msgAuthoritativeEngineBoots );
			incomingUsm->authoritativeEngineTime( std::chrono::seconds( securityParameters.msgAuthoritativeEngineTime ) );
			
			incomingSecurityModel.reset( incomingUsm );

			// Perform the authentication.
			if( message.msgHeaderData.msgFlags[0] & encoding::SNMPv3MessageFlags::AUTHENTICATION ) {
				switch( usm->authentication() ) {
					case Authentication::NONE:
						break;
					case Authentication::MD5: {
						//! These are the original authentication parameters from the message.
						//! These will be the first 12 bytes of the result of the MD5 processing
						//! that went into the message originally.
						std::string originalAuthenticationParameters = securityParameters.msgAuthenticationParameters;
						// TODO: Ensure that this is 12 bytes long.
						
						// Reset the authentication parameters to the original 12-byte string of zeros.
						securityParameters.msgAuthenticationParameters = std::string( 12, 0x00 );

						// Feed that back into the message structure.
						message.msgSecurityParameters = Encoder::encodeToString<encoding::USMSECURITYPARAMETERS>( securityParameters );
						
						// Re-encode the message so that we can MD5 it ourselves.
						std::vector<char> bytes = Encoder::encodeToVector<encoding::SNMPV3MESSAGE>( message );

						// Recreate the authentication work on our own.  If our result matches what was
						// given to use, then everything is good.  Otherwise, there was a problem and we
						// cannot trust this message.
						std::string testAuthenticationParameters = generateMD5AuthenticationParameters( usm->authenticationKey(), bytes );

						if( testAuthenticationParameters.compare( originalAuthenticationParameters ) == 0 ) {
							if( logger.system( Logger::GENERAL ) ) {
								logger.out() << "MD5 authentication verified." << std::endl;
							}
						} else {
							if( logger.system( Logger::GENERAL ) ) {
								logger.out() << "MD5 authentication failed." << std::endl;
								logger.out() << "Expected value: " << hexadecimal( testAuthenticationParameters ) << std::endl;
								logger.out() << "Received value: " << hexadecimal( originalAuthenticationParameters ) << std::endl;
							}
							throw AuthenticationFailureException( "MD5 authentication failed." );
						}
						break;
					}
					case Authentication::SHA: {
						//! These are the original authentication parameters from the message.
						//! These will be the first 12 bytes of the result of the SHA processing
						//! that went into the message originally.
						std::string originalAuthenticationParameters = securityParameters.msgAuthenticationParameters;
						// TODO: Ensure that this is 12 bytes long.
						
						// Reset the authentication parameters to the original 12-byte string of zeros.
						securityParameters.msgAuthenticationParameters = std::string( 12, 0x00 );

						// Feed that back into the message structure.
						message.msgSecurityParameters = Encoder::encodeToString<encoding::USMSECURITYPARAMETERS>( securityParameters );
						
						// Re-encode the message so that we can SHA it ourselves.
						std::vector<char> bytes = Encoder::encodeToVector<encoding::SNMPV3MESSAGE>( message );

						// Recreate the authentication work on our own.  If our result matches what was
						// given to use, then everything is good.  Otherwise, there was a problem and we
						// cannot trust this message.
						std::string testAuthenticationParameters = generateSHAAuthenticationParameters( usm->authenticationKey(), bytes );

						if( testAuthenticationParameters.compare( originalAuthenticationParameters ) == 0 ) {
							if( logger.system( Logger::GENERAL ) ) {
								logger.out() << "SHA authentication verified." << std::endl;
							}
						} else {
							if( logger.system( Logger::GENERAL ) ) {
								logger.out() << "SHA authentication failed." << std::endl;
								logger.out() << "Expected value: " << hexadecimal( testAuthenticationParameters ) << std::endl;
								logger.out() << "Received value: " << hexadecimal( originalAuthenticationParameters ) << std::endl;
							}
							throw AuthenticationFailureException( "SHA authentication failed." );
						}
						break;
					}
				}
			}

			// If the message needs to be decrypted, then do so.  Otherwise, in the trivial case,
			// we can simply copy the message's data portion over to our ScopedPDU byte stream.
			if( ! ( message.msgHeaderData.msgFlags[0] & encoding::SNMPv3MessageFlags::ENCRYPTION ) ) {
				// This message is not encrypted; we can just the PDU bytes as-is.
				scopedPduByteStream.copyFrom( message.pduBytes, ByteStream::READ_ONLY );
			} else {
				// This message is encrypted; we must first decrypt it.
				
				ByteStream octetStringByteStream;
				octetStringByteStream.copyFrom( message.pduBytes, ByteStream::READ_ONLY );

				BerStream octetStringStream( &octetStringByteStream );

				std::string octetString;
				bool success = octetStringStream.read<encoding::OCTET_STRING>( octetString );
				if( ! success ) {
					if( logger.system( Logger::GENERAL ) ) {
						logger.out() << "Could not decode the OCTET STRING for the encrypted ScopedPDU." << std::endl;
					}
					throw Exception( "Could not decode the OCTET STRING for the encrypted ScopedPDU." );
				}
						
				std::vector<char> scopedPduBytes( octetString.length() );
				for( int i = 0, iSize = octetString.length(); i < iSize; i++ ) {
					scopedPduBytes[ i ] = octetString[ i ];
				}

				switch( usm->encryption() ) {
					case Encryption::DES: {
						if( securityParameters.msgPrivacyParameters.length() != 8 ) {
							throw Exception( "Invalid size for msgPrivacyParameters." );
						}

						//! The DES key that we're going to use is the first 8 bytes of the encryption key.
						std::string key = usm->encryptionKey().substr( 0, 8 );

						//! The final 8 bytes are going to be used as the pre-initialization vector.
						std::string preInitializationVector = usm->encryptionKey().substr( 8, 8 );

						//! This is the "salt" that we will be using.
						std::string salt = securityParameters.msgPrivacyParameters;
						
						//! This is the intialization vector; it is built by XORing the pre-initialization
						//! vector with the salt.
						std::string initializationVector( 8, 0x00 );
						for( int i = 0; i < 8; i++ ) {
							initializationVector[ i ] = preInitializationVector[ i ] ^ salt[ i ];
						}

						EVP_CIPHER_CTX cypherContext;
						EVP_CIPHER_CTX_init( &cypherContext );

						EVP_DecryptInit_ex( &cypherContext, EVP_des_cbc(), NULL /* default implementation */, (unsigned char*)key.c_str(), (unsigned char*)initializationVector.c_str() );
						EVP_CIPHER_CTX_set_padding( &cypherContext, 0 );

						if( logger.system( Logger::GENERAL ) ) {
							logger.out() << "Encrypted bytes: " << scopedPduBytes.size() << std::endl;
						}
						if( scopedPduBytes.size() % 8 != 0 ) {
							if( logger.system( Logger::GENERAL ) ) {
								logger.out() << "We have gone past 8 by " << ( scopedPduBytes.size() % 8 ) << " bytes." << std::endl;
							}
							throw Exception( "Invalid size for the ScopedPDU bytes." );
						}

						//! This is how large of a buffer we are going to allocate for storing the decrypted data.
						int decryptedDataSize = scopedPduBytes.size() * 2 + EVP_MAX_BLOCK_LENGTH; //< TODO: Perhaps do this better?
						//! This is the buffer that we're going to use for the decrypted data.
						char decryptedData[ decryptedDataSize ];
						//! This is the _current_ length of "decryptedData"; as we decrypt more data, this will increase.
						int decryptedDataLength = 0; //< Nothing has been decrypted, yet.

						//! This is the number of bytes that we wrote on this call.
						int bytesWritten = 0; //< Obviously, we haven't written anything yet.
						int result = EVP_DecryptUpdate( &cypherContext, (unsigned char*)( decryptedData + decryptedDataLength ), &bytesWritten, (unsigned char*)scopedPduBytes.data(), scopedPduBytes.size() );
						if( result != 1 ) {
							throw DecryptionException( "Could not decrypt the data." );
						}
						if( logger.system( Logger::GENERAL ) ) {
							logger.out() << "Partially decrypted " << bytesWritten << " bytes." << std::endl;
						}
						decryptedDataLength += bytesWritten;

						bytesWritten = 0;
						result = EVP_DecryptFinal_ex( &cypherContext, (unsigned char*)( decryptedData + decryptedDataLength ), &bytesWritten );
						if( result != 1 ) {
							throw DecryptionException( "Could not decrypt the data." );
						}
						if( logger.system( Logger::GENERAL ) ) {
							logger.out() << "Partially decrypted " << bytesWritten << " bytes." << std::endl;
						}
						decryptedDataLength += bytesWritten;

						EVP_CIPHER_CTX_cleanup( &cypherContext );

						//! This string will hold the decrypted data.  It is important to have this as
						//! a standard string because we have the ability to _encode_ that string.
						std::string decryptedString( decryptedData, decryptedDataLength );

						/*
						if( logger.system( Logger::GENERAL ) ) {
							logger.out() << "Decrypted the ScopedPDU into " << decryptedDataLength << " bytes." << std::endl;
							for( int i = 0; i < decryptedString.size(); i++ ) {
								logger.out() << "0x" << std::hex << (int)(decryptedString[i]&0xFF) << std::endl;
							}
							logger.out() << std::dec;
						}
						*/
						
						scopedPduByteStream.copyFrom( decryptedString, ByteStream::READ_ONLY );
						break;
					}
					case Encryption::AES: {
						if( securityParameters.msgPrivacyParameters.length() != 8 ) {
							throw Exception( "Invalid size for msgPrivacyParameters." );
						}

						//! The AES key that we're going to use is the first 16 bytes of the encryption key.
						//! (That is, all of them.)
						std::string key = usm->encryptionKey().substr( 0, 16 );

						//! This is the intialization vector; it is built by concatenating:
						//!    1. The authoritative SNMP engine "boots".
						//!    2. The authoritative SNMP engine "time".
						//!    3. A random 64-bit integer.
						std::string initializationVector( 16, 0x00 );
						initializationVector[ 0 ] = ( incomingUsm->authoritativeEngineBoots() >> 24 ) & 0xFF;
						initializationVector[ 1 ] = ( incomingUsm->authoritativeEngineBoots() >> 16 ) & 0xFF;
						initializationVector[ 2 ] = ( incomingUsm->authoritativeEngineBoots() >> 8 ) & 0xFF;
						initializationVector[ 3 ] = ( incomingUsm->authoritativeEngineBoots() >> 0 ) & 0xFF;
						initializationVector[ 4 ] = ( incomingUsm->authoritativeEngineTime().count() >> 24 ) & 0xFF;
						initializationVector[ 5 ] = ( incomingUsm->authoritativeEngineTime().count() >> 16 ) & 0xFF;
						initializationVector[ 6 ] = ( incomingUsm->authoritativeEngineTime().count() >> 8 ) & 0xFF;
						initializationVector[ 7 ] = ( incomingUsm->authoritativeEngineTime().count() >> 0 ) & 0xFF;
						initializationVector[ 8 ] = securityParameters.msgPrivacyParameters[ 0 ];
						initializationVector[ 9 ] = securityParameters.msgPrivacyParameters[ 1 ];
						initializationVector[ 10 ] = securityParameters.msgPrivacyParameters[ 2 ];
						initializationVector[ 11 ] = securityParameters.msgPrivacyParameters[ 3 ];
						initializationVector[ 12 ] = securityParameters.msgPrivacyParameters[ 4 ];
						initializationVector[ 13 ] = securityParameters.msgPrivacyParameters[ 5 ];
						initializationVector[ 14 ] = securityParameters.msgPrivacyParameters[ 6 ];
						initializationVector[ 15 ] = securityParameters.msgPrivacyParameters[ 7 ];

						EVP_CIPHER_CTX cypherContext;
						EVP_CIPHER_CTX_init( &cypherContext );

						EVP_DecryptInit_ex( &cypherContext, EVP_aes_128_cfb128(), NULL /* default implementation */, (unsigned char*)key.c_str(), (unsigned char*)initializationVector.c_str() );
						EVP_CIPHER_CTX_set_padding( &cypherContext, 0 );

						if( logger.system( Logger::GENERAL ) ) {
							logger.out() << "Encrypted bytes: " << scopedPduBytes.size() << std::endl;
						}

						//! This is how large of a buffer we are going to allocate for storing the decrypted data.
						int decryptedDataSize = scopedPduBytes.size() * 2 + EVP_MAX_BLOCK_LENGTH; //< TODO: Perhaps do this better?
						//! This is the buffer that we're going to use for the decrypted data.
						char decryptedData[ decryptedDataSize ];
						//! This is the _current_ length of "decryptedData"; as we decrypt more data, this will increase.
						int decryptedDataLength = 0; //< Nothing has been decrypted, yet.

						//! This is the number of bytes that we wrote on this call.
						int bytesWritten = 0; //< Obviously, we haven't written anything yet.
						int result = EVP_DecryptUpdate( &cypherContext, (unsigned char*)( decryptedData + decryptedDataLength ), &bytesWritten, (unsigned char*)scopedPduBytes.data(), scopedPduBytes.size() );
						if( result != 1 ) {
							throw DecryptionException( "Could not decrypt the data." );
						}
						if( logger.system( Logger::GENERAL ) ) {
							logger.out() << "Partially decrypted " << bytesWritten << " bytes." << std::endl;
						}
						decryptedDataLength += bytesWritten;

						bytesWritten = 0;
						result = EVP_DecryptFinal_ex( &cypherContext, (unsigned char*)( decryptedData + decryptedDataLength ), &bytesWritten );
						if( result != 1 ) {
							throw DecryptionException( "Could not decrypt the data." );
						}
						if( logger.system( Logger::GENERAL ) ) {
							logger.out() << "Partially decrypted " << bytesWritten << " bytes." << std::endl;
						}
						decryptedDataLength += bytesWritten;

						EVP_CIPHER_CTX_cleanup( &cypherContext );

						//! This string will hold the decrypted data.  It is important to have this as
						//! a standard string because we have the ability to _encode_ that string.
						std::string decryptedString( decryptedData, decryptedDataLength );

						/*
						if( logger.system( Logger::GENERAL ) ) {
							logger.out() << "Decrypted the ScopedPDU into " << decryptedDataLength << " bytes." << std::endl;
							for( int i = 0; i < decryptedString.size(); i++ ) {
								logger.out() << "0x" << std::hex << (int)(decryptedString[i]&0xFF) << std::endl;
							}
							logger.out() << std::dec;
						}
						*/
						
						scopedPduByteStream.copyFrom( decryptedString, ByteStream::READ_ONLY );
						break;
					}
					case Encryption::NONE: {
						throw Exception( "No encryption model specified." );
						break;
					}
				}
			}

			break;
		}
		default:
			if( logger.system( Logger::GENERAL ) ) {
				logger.out() << "Unknown security model: " << message.msgHeaderData.msgSecurityModel << std::endl;
			}
			throw Exception( "Invalid security model." );
	}

	encoding::ScopedPDU scopedPdu;

	BerStream scopedPduStream( &scopedPduByteStream );
	success = scopedPduStream.read<encoding::SCOPEDPDU>( scopedPdu );
	if( ! success ) {
		if( logger.system( Logger::GENERAL ) ) {
			logger.out() << "Could not parse the ScopedPDU." << std::endl;
		}
		throw Exception( "Could not parse the ScopedPDU." );
	}
	
	if( logger.system( Logger::GENERAL ) ) {
		logger.out() << "MessageProcessorV3::" << __FUNCTION__ << ": contextEngineID: " << hexadecimal( scopedPdu.contextEngineID ) << std::endl;
		logger.out() << "MessageProcessorV3::" << __FUNCTION__ << ": contextName: " << scopedPdu.contextName << std::endl;
	}

	std::unique_ptr<ByteStream> returnStream( new ByteStream() );
	returnStream->copyFrom( scopedPdu.pduBytes, ByteStream::READ_ONLY );
	return returnStream;
}

} }

