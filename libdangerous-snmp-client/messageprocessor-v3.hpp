#pragma once

#include "messageprocessor.hpp"


namespace dangerous { namespace snmp {

/**
 * TODO: ???
 **/
class MessageProcessorV3 : public MessageProcessor {
public:
	MessageProcessorV3();

	std::vector<char> encode( const SecurityModel* securityModel, ConfirmedClass::Type confirmedClass, const std::vector<char>& pduBytes ) throw( Exception );
	std::unique_ptr<ByteStream> decode( const SecurityModel* securityModel, ByteStream& byteStream, std::unique_ptr<SecurityModel>& incomingSecurityModel ) throw( Exception );

protected:
	//! This is the counter that will be used to generate "msgID" when
	//! sending a new message.
	int32_t messageId;

	//! This is a random 32-bit integer for use with CBC-DES encryption.
	//! See http://tools.ietf.org/html/rfc2574
	uint32_t myRandomInteger32;

	//! This is a random 64-bit integer for use with CFB128-AES-128 encryption.
	//! See http://tools.ietf.org/html/rfc3826
	uint64_t myRandomInteger64;
};

} }

