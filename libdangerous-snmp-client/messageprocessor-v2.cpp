#include "messageprocessor-v2.hpp"

#include "asn1/asn1.hpp"
#include "bytestream.hpp"
#include "encoding/encoding.hpp"
#include "encoder.hpp"
#include "encoding/message.hpp"
#include "securitymodel/snmpv2c.hpp"

#include <algorithm>
#include <string>

namespace dangerous { namespace snmp {

MessageProcessorV2::MessageProcessorV2() {
	_version = SNMPv2;
}

std::vector<char> MessageProcessorV2::encode( const SecurityModel* securityModel, ConfirmedClass::Type confirmedClass, const std::vector<char>& pduBytes ) throw( Exception ) {
	if( securityModel->type() != SecurityModel::SNMPv2c ) {
		throw Exception( "Incorrect security model." );
	}
	const securitymodel::SNMPv2c* snmpv2 = static_cast<const securitymodel::SNMPv2c*>( securityModel );

	encoding::Message message;
	message.version = _version;
	message.communityString = snmpv2->communityString();
	message.pduBytes = pduBytes;

	return Encoder::encodeToVector<encoding::MESSAGE>( message );
}

std::unique_ptr<ByteStream> MessageProcessorV2::decode( const SecurityModel* securityModel, ByteStream& byteStream, std::unique_ptr<SecurityModel>& incomingSecurityModel ) throw( Exception ) {
	if( securityModel->type() != SecurityModel::SNMPv2c ) {
		throw Exception( "Incorrect security model." );
	}
	const securitymodel::SNMPv2c* snmpv2 = static_cast<const securitymodel::SNMPv2c*>( securityModel );

	encoding::Message message;

	BerStream berStream( &byteStream );

	bool success = berStream.read<encoding::MESSAGE>( message );
	if( ! success ) {
		throw Exception( "Could not decode the SNMPv2 message." );
	}

	if( message.version != _version ) {
		if( logger.system( Logger::GENERAL ) ) {
			logger.out() << "Incorrect version: " << message.version << std::endl;
		}
		throw Exception( "Incorrect version." );
	}
	if( message.communityString.compare( snmpv2->communityString() ) != 0 ) {
		if( logger.system( Logger::GENERAL ) ) {
			logger.out() << "Incorrect community string: " << message.communityString << std::endl;
		}
		throw Exception( "Incorrect community string." );
	}

	ByteStream* returnStream = new ByteStream();
	returnStream->copyFrom( message.pduBytes, ByteStream::READ_ONLY );
	return std::unique_ptr<ByteStream>( returnStream );
}

} }

