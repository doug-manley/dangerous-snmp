#pragma once

#include "messageprocessor.hpp"


namespace dangerous { namespace snmp {

/**
 * TODO: ???
 **/
class MessageProcessorV2 : public MessageProcessor {
public:
	MessageProcessorV2();
	
	std::vector<char> encode( const SecurityModel* securityModel, ConfirmedClass::Type confirmedClass, const std::vector<char>& pduBytes ) throw( Exception );

	std::unique_ptr<ByteStream> decode( const SecurityModel* securityModel, ByteStream& byteStream, std::unique_ptr<SecurityModel>& incomingSecurityModel ) throw( Exception );

};

} }

