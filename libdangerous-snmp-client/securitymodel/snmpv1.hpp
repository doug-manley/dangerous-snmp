#pragma once

#include "dangerous/snmp/securitymodel.hpp"

#include <string>

namespace dangerous { namespace snmp { namespace securitymodel {

class SNMPv1 : public SecurityModel {
public:
	SNMPv1( const std::string& communityString );

	bool isAuthenticated() const { return true; }

public:
	std::string communityString() const { return _communityString; }

protected:
	std::string _communityString;
};

} } }

