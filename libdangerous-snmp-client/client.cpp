#include "dangerous/snmp/client.hpp"
#include "dangerous/snmp/logger.hpp"
#include "dangerous/snmp/types.hpp"
#include "dangerous/snmp/numericoid.hpp"
#include "encoding/encoding.hpp"
#include "transport/udp.hpp"
#include "transport/tcp.hpp"

#include "messageprocessor-v1.hpp"
#include "messageprocessor-v2.hpp"
#include "messageprocessor-v3.hpp"

#include "securitymodel/snmpv1.hpp"
#include "securitymodel/snmpv2c.hpp"
#include "dangerous/snmp/securitymodel/usm.hpp"

#include "standardoids.hpp"
#include "string-functions.hpp"

#include <iostream>
#include <iomanip>

namespace dangerous { namespace snmp {

/**
 * This constructs a new Client.
 * Before it can be used, "uri" must be called.
 **/
Client::Client( const Context& context ) {
	this->context = context.privateData;

	// Default the timeout to 5 seconds.
	_timeout = std::chrono::seconds( 5 );

	// Default the URI to the empty string.
	_uri = "";
	
	// Set the starting request ID to some random (signed) 32-bit number.
	requestId = rand() % 0xFFFFFFFF;
}

/**
 * Destructor.
 **/
Client::~Client() {
}

/**
 * This returns the URI for the connection.
 * If the connection has no URI, then the string returned will have
 * zero length.
 * @return The URI for the connection.
 **/
std::string Client::uri() const {
	return _uri;
}

/**
 * This sets the URI for the client.  A URI is of the form:
 *    <transport>://<address>
 * Currently, the following transports are supported:
 *    udp
 *       The address is <ip address or hostname>[:<port number or name>]
 *    tcp
 *       The address is <ip address or hostname>[:<port number or name>]
 * @param uri The URI to use.
 * @throw ParseErrorException, UnknownTransportException, Exception
 **/
void Client::uri( const std::string& uri ) throw( ParseErrorException, UnknownTransportException, Exception ) {
	// Our URI must be of the form:
	//    <transport>://<transport-specfic string>
	//
	// So, we must first determine what the transport is.  If there is no
	// transport specified, then we must fail.
	size_t position = uri.find( "://" );
	if( position == std::string::npos ) {
		throw ParseErrorException( "Could not find the transport string." );
	}

	// Extract the transport string.
	std::string transportString = uri.substr( 0, position );
	if( logger.system( Logger::CLIENT ) ) {
		logger.out() << "Client::" << __FUNCTION__ << ": Transport string: " << transportString << std::endl;
	}

	// Extract the transport-specific connection string.
	std::string connectionString = uri.substr( position + 3 );

	if( transportString.compare( "udp" ) == 0 ) {
		// TODO: this->_uri will be invalid if this fails.
		this->transport = std::unique_ptr<Transport>( new transport::Udp( context, connectionString ) );
	} else if( transportString.compare( "tcp" ) == 0 ) {
		// TODO: this->_uri will be invalid if this fails.
		this->transport = std::unique_ptr<Transport>( new transport::Tcp( context, connectionString ) );
	} else {
		throw UnknownTransportException( "Unknown transport: " + transportString );
	}

	if( ! this->transport->isConnected() ) {
		throw Exception( "Could not connect to transport: " + connectionString );
	}

	// Now that the transport has been set up, set the read timeout.
	this->transport->readerStream->readTimeout( _timeout );

	// Set the internal URI so that it can be returned if the owner
	// of this instance wants it.
	_uri = uri;
}

/**
 * This configures the client for SNMPv1.
 * @param communityString The community string.
 **/
void Client::useVersion1( const std::string& communityString ) {
	this->securityModel = std::unique_ptr<SecurityModel>( new securitymodel::SNMPv1( communityString ) );
	this->messageProcessor = std::unique_ptr<MessageProcessor>( new MessageProcessorV1() );
}

/**
 * This configures the client for SNMPv2c.
 * @param communityString The community string.
 **/
void Client::useVersion2c( const std::string& communityString ) {
	this->securityModel = std::unique_ptr<SecurityModel>( new securitymodel::SNMPv2c( communityString ) );
	this->messageProcessor = std::unique_ptr<MessageProcessor>( new MessageProcessorV2() );
}

/**
 * This configures the client for SNMPv3.
 * @param securityModel The security information to use.
 **/
void Client::useVersion3( const securitymodel::USM& securityModel ) {
	this->securityModel = std::unique_ptr<SecurityModel>( new securitymodel::USM( securityModel ) );
	this->messageProcessor = std::unique_ptr<MessageProcessor>( new MessageProcessorV3() );
}

bool Client::isAuthenticated() {
	return securityModel->isAuthenticated();
}

void Client::authenticate() {
	if( logger.system( Logger::CLIENT ) ) {
		logger.out() << "Client::" << __FUNCTION__ << ": Security-model type is " << securityModel->type() << "." << std::endl;
	}
	switch( securityModel->type() ) {
		case SecurityModel::SNMPv1:
		case SecurityModel::SNMPv2c:
			// Nothing to do here ^_^
			if( logger.system( Logger::CLIENT ) ) {
				logger.out() << "Client::" << __FUNCTION__ << ": (no authentication required)." << std::endl;
			}
			break;
		case SecurityModel::USM: {
			// If we have already authenticated, then we don't need to do so again.
			if( securityModel->isAuthenticated() ) {
				if( logger.system( Logger::CLIENT ) ) {
					logger.out() << "Client::" << __FUNCTION__ << ": (already authenticated)." << std::endl;
				}
				break;
			}

			if( logger.system( Logger::CLIENT ) ) {
				logger.out() << "Client::" << __FUNCTION__ << ": Attempting to authenticate." << std::endl;
			}

			
			//! This is a pointer to our internal USM data.
			securitymodel::USM* clientUsm = (securitymodel::USM*)securityModel.get();


			// We need to authenticate.
			// So, the first thing that we're going to do is determine the target's
			// engine ID.
				
			if( logger.system( Logger::CLIENT ) ) {
				logger.out() << "Client::" << __FUNCTION__ << ": Sending engine-discovery PDU." << std::endl;
			}

			//! This is our empty request PDU.  It doesn't need to do anything special
			//! at all, other than have a request ID and be empty.
			PDU request;
	
			//! This is the noAuthNoPriv instance of USM security.
			//! This is required in order to perform "discovery".
			securitymodel::USM zeroSecurityModel;
			
			// Send the "discovery" PDU.
			std::unique_ptr<RawResponse> response = basicRequest<encoding::GetRequestPDU>( request, &zeroSecurityModel );

			if( ! response->securityModel ) {
				if( logger.system( Logger::CLIENT ) ) {
					logger.out() << "Client::" << __FUNCTION__ << ": No security model was returned." << std::endl;
				}
				return;
			}
			if( response->securityModel->type() != SecurityModel::USM ) {
				if( logger.system( Logger::CLIENT ) ) {
					logger.out() << "Client::" << __FUNCTION__ << ": Expected security model of USM; got " << response->securityModel->type() << "." << std::endl;
				}
				return;
			}
			securitymodel::USM* usm = (securitymodel::USM*)response->securityModel.get();
			if( logger.system( Logger::CLIENT ) ) {
				logger.out() << "Client::" << __FUNCTION__ << ": authoritativeEngineID: " << hexadecimal( usm->authoritativeEngineID() ) << std::endl;
				logger.out() << "Client::" << __FUNCTION__ << ": authoritativeEngineBoots: " << usm->authoritativeEngineBoots() << std::endl;
				logger.out() << "Client::" << __FUNCTION__ << ": authoritativeEngineTime: " << usm->authoritativeEngineTime().count() << " seconds" << std::endl;
			}

			switch( response->pduType ) {
				case encoding::ResponsePDU::TYPE:
					break;
				case encoding::ReportPDU::TYPE:
					for( const auto& varbind : response->pdu.varbinds ) {
						if( varbind->oid.equals( StandardOids::usmStatsUnknownEngineIDs ) ) {
							if( logger.system( Logger::CLIENT ) ) {
								logger.out() << "Client::" << __FUNCTION__ << ": Found OID usmStatsUnknownEngineIDs." << std::endl;
								logger.out() << "Client::" << __FUNCTION__ << ": Setting engine information." << std::endl;
							}
							clientUsm->authoritativeEngineID( usm->authoritativeEngineID() );
							// TODO: I don't think that we can legally set time information yet.
							// TODO: In theory, this may only be valid when an invalid-engine-boots report comes back...
							clientUsm->authoritativeEngineBoots( usm->authoritativeEngineBoots() );
							clientUsm->authoritativeEngineTime( usm->authoritativeEngineTime() );
							break;
						}
					}
					break;
				default:
					if( logger.system( Logger::CLIENT ) ) {
						logger.out() << "Client::" << __FUNCTION__ << ": Unexpected PDU type: " << ((int)response->pduType) << std::endl;
					}
			}

			break;
		}
		default:
			if( logger.system( Logger::CLIENT ) ) {
				logger.out() << "Client::" << __FUNCTION__ << ": Unimplemented security model: " << securityModel->type() << "." << std::endl;
			}
			return;
	}
}

/**
 * This issues a "get" request.
 * The PDU will be sent as-is (with the exception of:
 *    * requestId; this will be set internally.
 * Note that if this returns a Report PDU, then _that_ PDU will be
 * in the response.
 * @param requestPdu The PDU to send.
 * @return A unique pointer to a RawResponse.
 * @throw Exception
 **/
std::unique_ptr<RawResponse> Client::raw_get( PDU& requestPdu ) throw( Exception ) {
	return basicRequest<encoding::GetRequestPDU>( requestPdu );
}

/**
 * This issues a "getnext" request.
 * The PDU will be sent as-is (with the exception of:
 *    * requestId; this will be set internally.
 * Note that if this returns a Report PDU, then _that_ PDU will be
 * in the response.
 * @param requestPdu The PDU to send.
 * @return A unique pointer to a RawResponse.
 * @throw Exception
 **/
std::unique_ptr<RawResponse> Client::raw_getnext( PDU& requestPdu ) throw( Exception ) {
	return basicRequest<encoding::GetNextRequestPDU>( requestPdu );
}

/**
 * This issues a "getbulk" request.
 * The PDU will be sent as-is (with the exception of:
 *    * requestId; this will be set internally.
 * Note that if this returns a Report PDU, then _that_ PDU will be
 * in the response.
 * @param requestPdu The PDU to send.
 * @return A unique pointer to a RawResponse.
 * @throw Exception
 **/
std::unique_ptr<RawResponse> Client::raw_getbulk( PDU& requestPdu ) throw( Exception ) {
	return basicRequest<encoding::GetBulkRequestPDU>( requestPdu );
}

/**
 * This issues a "set" request.
 * The PDU will be sent as-is (with the exception of:
 *    * requestId; this will be set internally.
 * Note that if this returns a Report PDU, then _that_ PDU will be
 * in the response.
 * @param requestPdu The PDU to send.
 * @return A unique pointer to a RawResponse.
 * @throw Exception
 **/
std::unique_ptr<RawResponse> Client::raw_set( PDU& requestPdu ) throw( Exception ) {
	return basicRequest<encoding::SetRequestPDU>( requestPdu );
}

/**
 * This issues a simple "get" request.
 * @param oidList A list of OIDs to get.
 * @return A Response instance.
 **/
std::unique_ptr<Response> Client::get( const std::list<NumericOid>& oidList ) throw( Exception ) {
	//! This is the PDU that we want to send to the agent.
	PDU requestPdu;

	// Add the OIDs that we want to send.
	for( const auto& oid : oidList ) {
		requestPdu.addOid( oid );
	}

	if( logger.system( Logger::CLIENT ) ) {
		logger.out() << "Client::" << __FUNCTION__ << ": Get operation on " << requestPdu.varbinds.size() << " OIDs." << std::endl;
	}

	//! This is whether or not we are going to attempt to issue another request
	//! to accomplish the "get" operation.
	bool tryAgain = true;
	//! This is the _original_ request PDU.  We will need this later if we end
	//! up modifying "requestPdu" while trying to recover from errors.
	PDU originalRequestPdu = requestPdu;
	//! This is the response that we'll be returning.
	std::unique_ptr<Response> finalResponse( new Response() );
	//! This vector maps a 0-indexed VarBind in the final response to the corresponding
	//! 0-indexed VarBind in the original response.
	//!
	//! We will be using this to essentially transform a series of "broken" SNMPv1 responses
	//! into a single, proper SNMPv2 response.
	std::vector<int> mapping( requestPdu.varbinds.size() );

	// Trivially set the mapping.  At the beginning, each item in the request PDU
	// maps directly to an item in the original request PDU.
	for( unsigned int i = 0; i < requestPdu.varbinds.size(); i++ ) {
		mapping[ i ] = i;
	}

	//! This is the number of retries to perform due to timeout errors.
	int timeoutRetriesRemaining = 3; //< TODO: Make this come from a class member.
	while( tryAgain ) {
		// Assume that this is our last run.
		tryAgain = false;

		// If we do not have any OIDs to get, then we are done here.
		if( requestPdu.varbinds.size() == 0 ) {
			break;
		}

		//! This is the response to our get-request.
		std::unique_ptr<RawResponse> currentResponse = nullptr;
		try {
			// Perform the protocol-level get operation.
			currentResponse = raw_get( requestPdu );
			finalResponse->details.operations++;
		} catch( TimeoutException& e ) {
			if( logger.system( Logger::CLIENT ) ) {
				logger.out() << "Client::" << __FUNCTION__ << ": Timed out waiting for a response.  We have " << timeoutRetriesRemaining << " timeout-retries remaining." << std::endl;
			}
			if( timeoutRetriesRemaining > 0 ) {
				timeoutRetriesRemaining--;
				tryAgain = true;
				continue;
			} else {
				// If we have timed out too many times, then throw the timeout exception
				// back to the caller.
				throw;
			}
		} catch( ... ) {
			// This is truly unexpected.  Throw the exception to the caller.
			if( logger.system( Logger::CLIENT ) ) {
				logger.out() << "Client::" << __FUNCTION__ << ": Unexpected exception from 'raw_get'." << std::endl;
			}
			throw;
		}
		switch( currentResponse->pduType ) {
			case encoding::ResponsePDU::TYPE:
				// TODO: Check to make sure that the VarBindList sizes are EQUAL in both cases.

				// Check the "errorStatus" and "errorIndex" to see if this is a simple
				// matter of doing some retries to fill out the rest of the data.
				switch( currentResponse->pdu.errorStatus ) {
					case PDUErrorStatus::noError:
						// Perfect; everything is fine.
						break;
					case PDUErrorStatus::noSuchName:
						// The OID requested could not be found.
						// [fall-through]
					case PDUErrorStatus::badValue:
						// TODO: What does "badValue" mean?

						// If a _valid_ "error index" was returned, then we know that the agent
						// is trying to tell us that the OID at that particular index is "bad" in
						// some way.
						//
						// In that case, we will _remove_ the errored OID from our request and try
						// again.
						//
						// [note that the error index is 1-indexed.]
						if( currentResponse->pdu.errorIndex > 0 && (unsigned int)currentResponse->pdu.errorIndex <= requestPdu.varbinds.size() ) {
						
							if( logger.system( Logger::CLIENT ) ) {
								logger.out() << "Client::" << __FUNCTION__ << ": Erasing errored VarBind #" << currentResponse->pdu.errorIndex << " (error status was " << currentResponse->pdu.errorStatus << " )." << std::endl;
							}

							// Remove the "bad" OID.
							requestPdu.varbinds.erase( requestPdu.varbinds.begin() + ( currentResponse->pdu.errorIndex - 1 ) );
							// Remove the "bad" OID from the mapping vector, as well.
							mapping.erase( mapping.begin() + ( currentResponse->pdu.errorIndex - 1 ) );

							// Since we have made a productive change, we should try this again.
							tryAgain = true;
						}
						break;
					default:
						std::clog << "TODO: Got back an error status of " << currentResponse->pdu.errorStatus << ".  Should try to figure it out." << std::endl;
				}
				break;
			case encoding::ReportPDU::TYPE:
				// TODO: Figure out if this is a "usmStatsNotInTimeWindow" error.
				// TODO: If it is recoverable, then update our state (if it hasn't
				// TODO: already been updated) and try again.
				std::clog << "TODO: Got back a Report PDU.  Should try to figure it out." << std::endl;

				if( currentResponse->pdu.containsOid( StandardOids::usmStatsNotInTimeWindows ) ) {
					// In theory, we have already updated our security model's interpretation of the
					// agent's engine boots and time values.  We should be free to retry.
					tryAgain = true;

					// TODO: MOVE THE UPDATE LOGIC HERE SO THAT WE CAN THROW EXCEPTIONS ON IT IF STUFF GETS BAD.
							
					if( logger.system( Logger::CLIENT ) ) {
						logger.out() << "Client::" << __FUNCTION__ << ": Got a Report of usmStatsNotInTimeWindows; retrying." << std::endl;
					}
				} else if( currentResponse->pdu.containsOid( StandardOids::usmStatsUnknownUserNames ) ) {
					throw Exception( "Invalid username." );
				} else if( currentResponse->pdu.containsOid( StandardOids::usmStatsWrongDigests ) ) {
					throw Exception( "Incorrect digest." );
				} else {
					// TODO: uknown-report-exception?
					if( currentResponse->pdu.varbinds.size() > 0 ) {
						throw Exception( "Unknown Report-PDU type: " + currentResponse->pdu.varbinds[0]->oid.str() );
					} else {
						throw Exception( "Unknown Report-PDU type (with no VarBinds)." );
					}
				}
				break;
			default:
				std::clog << "TODO: Got back an unexpected PDU.  Should try to figure it out." << std::endl;
				throw Exception( "Unexpected PDU type." );
		}

		// If we think that we would benefit from doing another run, then we should do it.
		if( tryAgain ) {
			continue;
		}

		for( const auto& varbind : currentResponse->pdu.varbinds ) {
			finalResponse->pdu.varbinds.push_back( std::unique_ptr<VarBind>( new VarBind( *varbind ) ) );
		}

		// NOTE: "tryAgain" is ALWAYS false here.
	}

	// If our final response does _not_ have all of the VarBinds that we had originally
	// planned to ask for, then they must have failed, and we previously stripped them
	// out during our retries.
	//
	// Now, we're going to put them back in as "noSuchInstance".
	if( mapping.size() < originalRequestPdu.varbinds.size() ) {
		//! This is the VarBindList that we'll be using to replace the one in the final result.
		//!
		//! This is going to be initialized to the list of OIDs in the original request, but all
		//! set to "noSuchInstance".
		VarBindList newVarBindList;
		for( const auto& varbind : originalRequestPdu.varbinds ) {
			Variant variant;
			variant.set_null( Variant::CONTEXT_NOSUCHINSTANCE );
			newVarBindList.push_back( std::unique_ptr<VarBind>( new VarBind( varbind->oid, variant ) ) );
		}

		// Go through all of the legitimate responses that we received, and update the _new_ VarBindList
		// with that data.
		for( unsigned int i = 0; i < mapping.size(); i++ ) {
			int newIndex = mapping[ i ];
			newVarBindList[ newIndex ] = std::unique_ptr<VarBind>( new VarBind( *finalResponse->pdu.varbinds[ i ].get() ) );
		}

		// Finally, swap in the new VarBindList and remove the old one.
		finalResponse->pdu.varbinds.swap( newVarBindList );
	}

	return std::move( finalResponse );
}

/**
 * This issues a simple "getnext" request.
 * @param oidList A list of OIDs to get.
 * @return A Response instance.
 **/
std::unique_ptr<Response> Client::getNext( const std::list<NumericOid>& oidList ) {
	//! This is the PDU that we want to send to the agent.
	PDU requestPdu;

	// Add the OIDs that we want to send.
	for( const auto& oid : oidList ) {
		requestPdu.addOid( oid );
	}

	std::unique_ptr<Response> response( new Response() );

	std::unique_ptr<RawResponse> currentResponse = raw_getnext( requestPdu );
	response->details.operations++;


	for( const auto& varbind : currentResponse->pdu.varbinds ) {
		response->pdu.varbinds.push_back( std::unique_ptr<VarBind>( new VarBind( *varbind ) ) );
	}


	// TODO: Custom processing (like in "get")

	return response;
}

/**
 * This issues a "getbulk" request.
 * @param singleRequestOids A list of OIDs (strings) to get as in "getnext".
 * @param multipleRequestLimit The number of times that "multipleRequestOids" should be requested.
 * @param multipleRequestOids A list of OIDs (strings) to get as in "getnext", but repeated "multipleRequestLimit" times.
 * @return A Response instance.
 **/
std::unique_ptr<Response> Client::getBulk( const std::list<NumericOid>& singleRequestOids, unsigned int multipleRequestLimit, const std::list<NumericOid>& multipleRequestOids ) {
	//! This is the response that we will be returning.
	std::unique_ptr<Response> response( new Response() );

	// We have two major use cases.  In the first case, we will be using SNMPv1 "getnext"
	// requests to _emulate_ a "getbulk" request.  In the second, we will simply be using
	// proper "getbulk" requests.

	if( messageProcessor->version() == MessageProcessor::SNMPv1 ) {
		if( logger.system( Logger::CLIENT ) ) {
			logger.out() << "Client::" << __FUNCTION__ << ": Attempting to emulate 'getbulk' using 'getnext'." << std::endl;
		}

		if( singleRequestOids.size() > 0 ) {
			if( logger.system( Logger::CLIENT ) ) {
				logger.out() << "Client::" << __FUNCTION__ << ": Performing a 'getnext' on the " << singleRequestOids.size() << " single-request OIDs." << std::endl;
			}
			std::unique_ptr<Response> currentResponse = getNext( singleRequestOids );
			response->details.operations += currentResponse->details.operations;
			for( const auto& varbind : currentResponse->pdu.varbinds ) {
				response->pdu.varbinds.push_back( std::unique_ptr<VarBind>( new VarBind( *varbind ) ) );
			}
		}

		if( multipleRequestOids.size() > 0 && multipleRequestLimit > 0 ) {
			if( logger.system( Logger::CLIENT ) ) {
				logger.out() << "Client::" << __FUNCTION__ << ": Performing " << multipleRequestLimit << " 'getnext' operations on the " << multipleRequestOids.size() << " multiple-request OIDs." << std::endl;
			}

			for( const auto& oid : multipleRequestOids ) {
				//! This is the OID that we will be attempting to "getnext" next.
				NumericOid nextOid( oid );

				for( unsigned int i = 0; i < multipleRequestLimit; i++ ) {
					if( logger.system( Logger::CLIENT ) ) {
						logger.out() << "Client::" << __FUNCTION__ << ": Multiple-request OID '" << oid.str() << "', request " << (i+1) << " / " << multipleRequestLimit << ": '" << nextOid.str() << "'." << std::endl;
					}

					std::unique_ptr<Response> currentResponse = getNext( { nextOid.str() } );
					response->details.operations += currentResponse->details.operations;
					if( currentResponse->pdu.errorStatus == PDUErrorStatus::noSuchName || currentResponse->pdu.errorStatus == PDUErrorStatus::badValue ) {
						if( logger.system( Logger::CLIENT ) ) {
							logger.out() << "Client::" << __FUNCTION__ << ": Got back an error (noSuchName or badValue); flagging as endOfMibView." << std::endl;
						}

						Variant value;
						value.set_null( Variant::CONTEXT_ENDOFMIBVIEW );
							
						response->pdu.varbinds.push_back( std::unique_ptr<VarBind>( new VarBind( nextOid, value ) ) );

						break;
					}
					for( const auto& varbind : currentResponse->pdu.varbinds ) {
						response->pdu.varbinds.push_back( std::unique_ptr<VarBind>( new VarBind( *varbind ) ) );
						nextOid = varbind->oid;
						
						if( logger.system( Logger::CLIENT ) ) {
							logger.out() << "Client::" << __FUNCTION__ << ": Next OID is '" << nextOid.str() << "'." << std::endl;
						}
					}
				}
			}
		}
	} else {
		if( logger.system( Logger::CLIENT ) ) {
			logger.out() << "Client::" << __FUNCTION__ << ": Performing a normal 'getbulk' operation." << std::endl;
		}

		//! This is the PDU that we want to send to the agent.
		PDU requestPdu;

		// Add the OIDs that we want to send.
		for( const auto& oid : singleRequestOids ) {
			requestPdu.addOid( oid );
		}
		// In a GetBulkRequest-PDU, the "errorStatus" is actually the "non-repeaters" value.
		requestPdu.errorStatus = singleRequestOids.size();

		if( multipleRequestOids.size() > 0 ) {
			for( const auto& oid : multipleRequestOids ) {
				requestPdu.addOid( oid );
			}
			requestPdu.errorIndex = multipleRequestLimit;
		}

		std::unique_ptr<RawResponse> currentResponse = raw_getbulk( requestPdu );
		response->details.operations++;
	
	
		for( const auto& varbind : currentResponse->pdu.varbinds ) {
			response->pdu.varbinds.push_back( std::unique_ptr<VarBind>( new VarBind( *varbind ) ) );
		}


		// TODO: Custom processing (like in "get")
	}

	return std::move( response );
}

/**
 * This issues a "set" request.
 * Due to the nature of the SNMP "set" command, this will be almost
 * trivially equivalent to the "raw_set" method.  That is, a "set"
 * command _must_ be issued atomically, so there will be no special
 * actions taken within this method.
 *
 * @param varbinds A VarBindList containing the OIDs (and their respective values) to set.
 * @return A Response instance.
 **/
std::unique_ptr<Response> Client::set( const VarBindList& varbinds ) throw( Exception ) {
	//! This is the PDU that we want to send to the agent.
	PDU requestPdu;

	// Add the OIDs that we want to send.
	for( const auto& varbind : varbinds ) {
		requestPdu.varbinds.push_back( std::unique_ptr<VarBind>( new VarBind( *varbind ) ) );
	}

	//! TODO
	std::unique_ptr<Response> response( new Response() );
	
	//! This is whether or not we are going to attempt to issue another request
	//! to accomplish the "get" operation.
	bool tryAgain = true;

	//! This is the number of retries to perform due to timeout errors.
	int timeoutRetriesRemaining = 3; //< TODO: Make this come from a class member.
	while( tryAgain ) {
		// Assume that this is our last run.
		tryAgain = false;

		// If we do not have any OIDs to get, then we are done here.
		if( requestPdu.varbinds.size() == 0 ) {
			break;
		}

		//! This is the response to our get-request.
		std::unique_ptr<RawResponse> currentResponse = nullptr;
		try {
			// Perform the protocol-level get operation.
			currentResponse = raw_set( requestPdu );
			response->details.operations++;
		} catch( TimeoutException& e ) {
			if( logger.system( Logger::CLIENT ) ) {
				logger.out() << "Client::" << __FUNCTION__ << ": Timed out waiting for a response.  We have " << timeoutRetriesRemaining << " timeout-retries remaining." << std::endl;
			}
			if( timeoutRetriesRemaining > 0 ) {
				timeoutRetriesRemaining--;
				tryAgain = true;
				continue;
			} else {
				// If we have timed out too many times, then throw the timeout exception
				// back to the caller.
				throw;
			}
		} catch( ... ) {
			// This is truly unexpected.  Throw the exception to the caller.
			if( logger.system( Logger::CLIENT ) ) {
				logger.out() << "Client::" << __FUNCTION__ << ": Unexpected exception from 'raw_set'." << std::endl;
			}
			throw;
		}
		switch( currentResponse->pduType ) {
			case encoding::ResponsePDU::TYPE:
				// This is exactly what we're expecting.
				//
				// Note that there is no special action to take in the case of a "set" command.
				break;
			case encoding::ReportPDU::TYPE:
				// TODO: Figure out if this is a "usmStatsNotInTimeWindow" error.
				// TODO: If it is recoverable, then update our state (if it hasn't
				// TODO: already been updated) and try again.
				std::clog << "TODO: Got back a Report PDU.  Should try to figure it out." << std::endl;

				if( currentResponse->pdu.containsOid( StandardOids::usmStatsNotInTimeWindows ) ) {
					// In theory, we have already updated our security model's interpretation of the
					// agent's engine boots and time values.  We should be free to retry.
					tryAgain = true;

					// TODO: MOVE THE UPDATE LOGIC HERE SO THAT WE CAN THROW EXCEPTIONS ON IT IF STUFF GETS BAD.
							
					if( logger.system( Logger::CLIENT ) ) {
						logger.out() << "Client::" << __FUNCTION__ << ": Got a Report of usmStatsNotInTimeWindows; retrying." << std::endl;
					}
				} else if( currentResponse->pdu.containsOid( StandardOids::usmStatsUnknownUserNames ) ) {
					throw Exception( "Invalid username." );
				} else if( currentResponse->pdu.containsOid( StandardOids::usmStatsWrongDigests ) ) {
					throw Exception( "Incorrect digest." );
				} else {
					// TODO: uknown-report-exception?
					if( currentResponse->pdu.varbinds.size() > 0 ) {
						throw Exception( "Unknown Report-PDU type: " + currentResponse->pdu.varbinds[0]->oid.str() );
					} else {
						throw Exception( "Unknown Report-PDU type (with no VarBinds)." );
					}
				}
				break;
			default:
				std::clog << "TODO: Got back an unexpected PDU.  Should try to figure it out." << std::endl;
				throw Exception( "Unexpected PDU type." );
		}

		// If we think that we would benefit from doing another run, then we should do it.
		if( tryAgain ) {
			continue;
		}

		for( const auto& varbind : currentResponse->pdu.varbinds ) {
			response->pdu.varbinds.push_back( std::unique_ptr<VarBind>( new VarBind( *varbind ) ) );
		}

		// NOTE: "tryAgain" is ALWAYS false here.
	}

	return std::move( response );
}

/**
 * This performs a "walk" operation.  A walk is a conceptual operation that
 * takes an OID as a starting point and returns a list of OIDs (and their values)
 * that are "under" the starting OID.  That is, the results are guaranteed to
 * _begin_ with exactly the same OID sequence that was given.
 *
 * This operation is conceptually understood, in the simplest case, as performing
 * "getnext" requests until the resulting OID no longer begins with the starting
 * OID.
 *
 * This operation also includes the concept of "paging"; that is, a limit can be
 * given to limit the number of OIDs that are returned.  If the limit is reached,
 * then an additional call can be made using the last OID in the result as the
 * "startingOid" for the new operation.
 * @param oid The starting OID.
 * @param limit If set, then this will return only this many results.
 * @param startingOid If set, then this will begin the walk starting from this OID.
 * @return A Response instance.
 **/
std::unique_ptr<Response> Client::walk( const NumericOid& oid, unsigned int limit, const NumericOid& startingOid ) {
	// TODO: Figure out what kind of options are available to the client (getnext vs getbulk)
	
	if( logger.system( Logger::CLIENT ) ) {
		logger.out() << "Client::" << __FUNCTION__ << ": Walk operation on OID '" << oid.str() << "', limit = " << limit << ", starting from '" << startingOid.str() << "'." << std::endl;
	}

	std::unique_ptr<Response> response( new Response() );

	NumericOid nextOid;
	if( startingOid.numbers.size() == 0 ) {
		nextOid = oid;
	} else {
		nextOid = startingOid;
	}
	
	unsigned int resultCount = 0;
	while( true ) {
		if( limit > 0 && resultCount >= limit ) {
			break;
		}

		if( logger.system( Logger::CLIENT ) ) {
			logger.out() << "Client::" << __FUNCTION__ << ": getnext operation #" << resultCount << "; OID '" << nextOid.str() << "'." << std::endl;
		}

		unsigned int stepSize = 10; //< TODO: member setting for this?
		if( limit - resultCount < stepSize ) {
			stepSize = limit - resultCount;
		}
		std::unique_ptr<Response> currentResponse = getBulk( std::list<NumericOid>(), stepSize, std::list<NumericOid>( { nextOid.str() } ) );
		response->details.operations += currentResponse->details.operations;
		bool outOfRange = false;
		for( const auto& varbind : currentResponse->pdu.varbinds ) {
			if( varbind->oid.beginsWith( oid ) ) {
				if( varbind->oid.isLessThan( nextOid ) || varbind->oid.equals( nextOid ) ) {
					if( logger.system( Logger::CLIENT ) ) {
						logger.out() << "Client::" << __FUNCTION__ << ": OID '" << varbind->oid.str() << "' is not increasing." << std::endl;
					}
					outOfRange = true;
					break;
				}

				response->pdu.varbinds.push_back( std::unique_ptr<VarBind>( new VarBind( *varbind ) ) );
				nextOid = varbind->oid;
			} else {
				if( logger.system( Logger::CLIENT ) ) {
					logger.out() << "Client::" << __FUNCTION__ << ": OID '" << varbind->oid.str() << "' is out of range." << std::endl;
				}
				outOfRange = true;
			}
		}
		if( outOfRange ) {
			if( logger.system( Logger::CLIENT ) ) {
				logger.out() << "Client::" << __FUNCTION__ << ": Ending due to out-of-range." << std::endl;
			}
			break;
		}

		resultCount++;
	}

	return response;
}

template<typename PduEncoder>
void Client::lowLevelSend( SecurityModel* outgoingSecurityModel, PDU& requestPdu ) throw( Exception ) {
	// The first step is to convert the logical PDU into an array of bytes
	// that we can then hand over to our Message Processor.  He won't care
	// about the _content_ of the message.


	//! This is the number of bytes that the PDU will consume,
	//! once encoded.
	unsigned int pduBytesSize = transport->maximumOutgoingMessageSize();
	if( logger.system( Logger::CLIENT ) ) {
		logger.out() << "Client::" << __FUNCTION__ << ": Setting maximum PDU size to " << pduBytesSize << "." << std::endl;
	}
	//! This is the buffer to store the BER-encoded PDU.
	std::vector<char> pduBytes( pduBytesSize );

	// Encode the PDU.
	ByteStream requestByteStream;
	requestByteStream.linkFrom( pduBytes.data(), pduBytesSize, ByteStream::READ_WRITE );

	BerStream requestStream( &requestByteStream );

	bool success = requestStream.write<PduEncoder>( requestPdu );
	if( ! success ) {
		if( logger.system( Logger::CLIENT ) ) {
			logger.out() << "Client::" << __FUNCTION__ << ": Could not encode the PDU." << std::endl;
		}
		throw Exception( "Could not encode the PDU." );
	}
	// Our byte-buffer may have been too large, so resize it, if necessary.
	pduBytes.resize( requestStream.remainingReadLength() );


	// Next, we have the Message Processor encode the data however it sees fit,
	// which gives us another array of bytes.  This array will be given to the
	// transport to be sent.


	// Add the MessageProcessor envelope.
	std::vector<char> bytes = messageProcessor->encode( outgoingSecurityModel, PduEncoder::confirmed, pduBytes );
	if( logger.system( Logger::CLIENT ) ) {
		logger.out() << "Client::" << __FUNCTION__ << ": Message processor encoded " << bytes.size() << " bytes." << std::endl;
		// DEBUG:
		logger.out() << std::hex;
		for( auto byte : bytes ) {
			logger.out() << std::setw(2) << std::setfill('0') << (int)( (unsigned char)byte ) << " ";
		}
		logger.out() << std::dec << std::endl;
		// :GUBED
	}

	// Send the bytes over the transport.
	unsigned int bytesWritten = 0;
	success = transport->writerStream->writeBytes( bytes.data(), bytes.size(), bytesWritten );
	if( ! success ) {
		throw Exception( "Could not send the message over the transport." );
	}
	transport->commitWrite();
}

std::unique_ptr<ByteStream> Client::lowLevelReceive( std::unique_ptr<SecurityModel>& incomingSecurityModel ) throw( Exception ) {
	if( logger.system( Logger::CLIENT ) ) {
		logger.out() << "Client::" << __FUNCTION__ << ": Attempting to read a TLV entity." << std::endl;
	}
	std::vector<char> bytes;

	BerStream berStream( &(*transport->readerStream) );

	bool success = berStream.readFullTlv( bytes );
	if( ! success ) {
		if( logger.system( Logger::CLIENT ) ) {
			logger.out() << "Client::" << __FUNCTION__ << ": Could not receive a TLV entity." << std::endl;
		}
		if( transport->readerStream->lastReadTimedOut() ) {
			if( logger.system( Logger::CLIENT ) ) {
				logger.out() << "Client::" << __FUNCTION__ << ": Time out." << std::endl;
			}
			throw TimeoutException( "Timed out." );
		}

		// TODO: INCREMENT: snmpInASNParseErrs (RFC-3412)
		throw Exception( "Could not receive a TLV entity." );
	}
	if( logger.system( Logger::CLIENT ) ) {
		logger.out() << "Client::" << __FUNCTION__ << ": Read a TLV entity." << std::endl;
		// DEBUG:
		logger.out() << std::hex;
		for( auto byte : bytes ) {
			logger.out() << std::setw(2) << std::setfill('0') << (int)( (unsigned char)byte ) << " ";
		}
		logger.out() << std::dec << std::endl;
		// :GUBED
	}

	//! This will be used to read the bytes that were returned
	//! from the transport.
	ByteStream byteStream;
	// Copy the transport bytes into the ByteStream.
	byteStream.copyFrom( bytes, ByteStream::READ_ONLY ); //< TODO: This could be done with "linkFrom" to save a malloc.

	//! Remove the MessageProcessor envelope.
	//! This is the ByteStream for the payload within the message.
	std::unique_ptr<ByteStream> responseStream = messageProcessor->decode( securityModel.get(), byteStream, incomingSecurityModel );
	if( responseStream == nullptr ) {
		if( logger.system( Logger::CLIENT ) ) {
			logger.out() << "Could not extract payload from message." << std::endl;
		}
		
		// TODO: INCREMENT: snmpInASNParseErrs (RFC-3412)
		throw Exception( "Could not extract payload from message." );
	}

	return responseStream;
}

template<typename PduEncoder>
std::unique_ptr<RawResponse> Client::basicRequest( PDU& requestPdu, SecurityModel* desiredSecurityModel ) throw( Exception ) {
	// Set the request ID on the PDU by incrementing our internal
	// "requestId" variable.
	requestPdu.requestId = ++requestId;

	// If this is SNMPv3/USM, then we will use this opportunity to
	// update our internal understanding of the agent's engine time.
	switch( securityModel->type() ) {
		case SecurityModel::USM: {
			//! This is a pointer to our representation of the security model.
			securitymodel::USM* clientUsm = (securitymodel::USM*)securityModel.get();

			clientUsm->updateTimeFromClock();
			break;
		}
		default:
			// We don't need to do anything for other security models.
			;
	}

	// Actually send the PDU.
	lowLevelSend<PduEncoder>( desiredSecurityModel ?: securityModel.get(), requestPdu );
	
	// Wait for a response to come back.
	std::unique_ptr<SecurityModel> incomingSecurityModel( nullptr );
	std::unique_ptr<ByteStream> responseByteStream = lowLevelReceive( incomingSecurityModel );

	// Since we know that we got back the right kind of PDU, we are
	// going to extract the contents of the PDU.
	std::unique_ptr<RawResponse> response( new RawResponse() );
	response->securityModel = std::move( incomingSecurityModel );

	BerStream responseStream( &(*responseByteStream) );

	//! This is the value of the "type" of PDU that came back.
	uint8_t type = 0;
	// Extract the type from the stream, but don't advance the stream.  We just want
	// to know what it is because we have different use cases for different types.
	bool success = responseStream.peekType( type );
	if( ! success ) {
		if( logger.system( Logger::CLIENT ) ) {
			logger.out() << "Client::" << __FUNCTION__ << ": Could not peek at the type." << std::endl;
		}
		throw Exception( "Could not extract the PDU type." );
	}

	response->pduType = type;

	switch( type ) {
		case encoding::ResponsePDU::TYPE:
			success = responseStream.read<encoding::ResponsePDU>( response->pdu );
			if( ! success ) {
				if( logger.system( Logger::CLIENT ) ) {
					logger.out() << "Client::" << __FUNCTION__ << ": Could not read Response-PDU." << std::endl;
				}
				throw ParseErrorException( "Could not read Response-PDU." );
			}
			break;
		case encoding::ReportPDU::TYPE:
			success = responseStream.read<encoding::ReportPDU>( response->pdu );
			if( ! success ) {
				if( logger.system( Logger::CLIENT ) ) {
					logger.out() << "Client::" << __FUNCTION__ << ": Could not read Report-PDU." << std::endl;
				}
				throw ParseErrorException( "Could not read Report-PDU." );
			}
			break;
		default:
			if( logger.system( Logger::CLIENT ) ) {
				logger.out() << "Client::" << __FUNCTION__ << ": Unexpected PDU type: " << ((int)type) << std::endl;
			}
			throw Exception( "Unknown PDU type." );
	}

	// Since we made it this far, then our response must be valid
	// and meets our expectations.
	
	// If this is SNMPv3/USM, then we will use this opportunity to
	// synchronize the engine boots and time.
	switch( securityModel->type() ) {
		case SecurityModel::USM: {
			//! This is a pointer to our representation of the security model.
			securitymodel::USM* clientUsm = (securitymodel::USM*)securityModel.get();

			//! This is a pointer to the values of the security model from the response.
			securitymodel::USM* usm = (securitymodel::USM*)response->securityModel.get();
			
			if( logger.system( Logger::CLIENT ) ) {
				logger.out() << "Client::" << __FUNCTION__ << ": authoritativeEngineID: " << hexadecimal( usm->authoritativeEngineID() ) << std::endl;
			}

			// TODO: Ensure that engine boots and time are valid (not zero, not all-ones) before trusting it so easily.

			if( usm->authoritativeEngineBoots() > clientUsm->authoritativeEngineBoots() ) {
				if( logger.system( Logger::CLIENT ) ) {
					logger.out() << "Client::" << __FUNCTION__ << ": The agent has updated his boots." << std::endl;
					logger.out() << "Client::" << __FUNCTION__ << ": authoritativeEngineBoots: " << usm->authoritativeEngineBoots() << std::endl;
					logger.out() << "Client::" << __FUNCTION__ << ": authoritativeEngineTime: " << usm->authoritativeEngineTime().count() << " seconds" << std::endl;
				}

				clientUsm->authoritativeEngineBoots( usm->authoritativeEngineBoots() );
				clientUsm->authoritativeEngineTime( usm->authoritativeEngineTime() );
			} else if( usm->authoritativeEngineBoots() == clientUsm->authoritativeEngineBoots() ) {
				if( logger.system( Logger::CLIENT ) ) {
					logger.out() << "Client::" << __FUNCTION__ << ": The agent has updated his time." << std::endl;
					logger.out() << "Client::" << __FUNCTION__ << ": authoritativeEngineTime: " << usm->authoritativeEngineTime().count() << " seconds" << std::endl;
				}

				// TODO: Check for making sure that it's within 150 seconds; otherwise, it may be an illegal change.

				clientUsm->authoritativeEngineTime( usm->authoritativeEngineTime() );
			} else {
				// The agent is in the "past", which means that we should not trust him.
				// TODO: INSERT RFC REFERENCE HERE.

				// TODO: Error?
			}
			
			break;
		}
		default:
			// Nothing special to do for other security models.
			;
	}

	return response;
}

} }

