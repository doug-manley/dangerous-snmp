#include "standardoids.hpp"

namespace dangerous { namespace snmp {

const NumericOid StandardOids::usmStatsUnsupportedSecLevels( ".1.3.6.1.6.3.15.1.1.1.0" );
const NumericOid StandardOids::usmStatsNotInTimeWindows( ".1.3.6.1.6.3.15.1.1.2.0" );
const NumericOid StandardOids::usmStatsUnknownUserNames( ".1.3.6.1.6.3.15.1.1.3.0" );
const NumericOid StandardOids::usmStatsUnknownEngineIDs( ".1.3.6.1.6.3.15.1.1.4.0" );
const NumericOid StandardOids::usmStatsWrongDigests( ".1.3.6.1.6.3.15.1.1.5.0" );
const NumericOid StandardOids::usmStatsDecryptionErrors( ".1.3.6.1.6.3.15.1.1.6.0" );

} }

