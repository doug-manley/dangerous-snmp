#include "messageprocessor-v1.hpp"

#include "asn1/asn1.hpp"
#include "bytestream.hpp"
#include "encoder.hpp"
#include "encoding/encoding.hpp"
#include "encoding/message.hpp"
#include "securitymodel/snmpv1.hpp"

#include <algorithm>
#include <string>

namespace dangerous { namespace snmp {

MessageProcessorV1::MessageProcessorV1() {
	_version = SNMPv1;
}

std::vector<char> MessageProcessorV1::encode( const SecurityModel* securityModel, ConfirmedClass::Type confirmedClass, const std::vector<char>& pduBytes ) throw( Exception ) {
	if( securityModel->type() != SecurityModel::SNMPv1 ) {
		throw Exception( "Incorrect security model." );
	}
	const securitymodel::SNMPv1* snmpv1 = static_cast<const securitymodel::SNMPv1*>( securityModel );

	encoding::Message message;
	message.version = _version;
	message.communityString = snmpv1->communityString();
	message.pduBytes = pduBytes;

	return Encoder::encodeToVector<encoding::MESSAGE>( message );
}

std::unique_ptr<ByteStream> MessageProcessorV1::decode( const SecurityModel* securityModel, ByteStream& byteStream, std::unique_ptr<SecurityModel>& incomingSecurityModel ) throw( Exception ) {
	if( securityModel->type() != SecurityModel::SNMPv1 ) {
		throw Exception( "Incorrect security model." );
	}
	const securitymodel::SNMPv1* snmpv1 = static_cast<const securitymodel::SNMPv1*>( securityModel );

	encoding::Message message;

	BerStream berStream( &byteStream );

	bool success = berStream.read<encoding::MESSAGE>( message );
	if( ! success ) {
		throw Exception( "Could not decode the SNMPv1 message." );
	}

	if( message.version != _version ) {
		if( logger.system( Logger::GENERAL ) ) {
			logger.out() << "Incorrect version: " << message.version << std::endl;
		}
		throw Exception( "Incorrect version." );
	}
	if( message.communityString.compare( snmpv1->communityString() ) != 0 ) {
		if( logger.system( Logger::GENERAL ) ) {
			logger.out() << "Incorrect community string: " << message.communityString << std::endl;
		}
		throw Exception( "Incorrect community string." );
	}

	ByteStream* returnStream = new ByteStream();
	returnStream->copyFrom( message.pduBytes, ByteStream::READ_ONLY );
	return std::unique_ptr<ByteStream>( returnStream );
}

} }

