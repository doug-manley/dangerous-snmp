#include "dangerous/snmp/exception.hpp"

namespace dangerous { namespace snmp {

/**
 * Constructor.
 *
 * @param text The exception text.
 **/
Exception::Exception( const char* text ) noexcept : _text( text ) {}
/**
 * Constructor.
 *
 * @param text The exception text.
 **/
Exception::Exception( const std::string& text ) noexcept : _text( text ) {}

/**
 * This returns the text for the exception.
 *
 * @return The exception's text.
 **/
const char* Exception::what() const noexcept {
	return _text.c_str();
}

/**
 * Constructor.
 *
 * @param text The exception text.
 **/
AuthenticationFailureException::AuthenticationFailureException( const char* text ) noexcept : Exception( text ) {}
/**
 * Constructor.
 *
 * @param text The exception text.
 **/
AuthenticationFailureException::AuthenticationFailureException( const std::string& text ) noexcept : Exception( text ) {}

/**
 * Constructor.
 *
 * @param text The exception text.
 **/
AuthenticationGenerationException::AuthenticationGenerationException( const char* text ) noexcept : Exception( text ) {}
/**
 * Constructor.
 *
 * @param text The exception text.
 **/
AuthenticationGenerationException::AuthenticationGenerationException( const std::string& text ) noexcept : Exception( text ) {}

/**
 * Constructor.
 *
 * @param text The exception text.
 **/
DecryptionException::DecryptionException( const char* text ) noexcept : Exception( text ) {}
/**
 * Constructor.
 *
 * @param text The exception text.
 **/
DecryptionException::DecryptionException( const std::string& text ) noexcept : Exception( text ) {}

/**
 * Constructor.
 *
 * @param text The exception text.
 **/
EncryptionException::EncryptionException( const char* text ) noexcept : Exception( text ) {}
/**
 * Constructor.
 *
 * @param text The exception text.
 **/
EncryptionException::EncryptionException( const std::string& text ) noexcept : Exception( text ) {}

/**
 * Constructor.
 *
 * @param text The exception text.
 **/
InvalidTypeException::InvalidTypeException( const char* text ) noexcept : Exception( text ) {}
/**
 * Constructor.
 *
 * @param text The exception text.
 **/
InvalidTypeException::InvalidTypeException( const std::string& text ) noexcept : Exception( text ) {}

/**
 * Constructor.
 *
 * @param text The exception text.
 **/
ParseErrorException::ParseErrorException( const char* text ) noexcept : Exception( text ) {}
/**
 * Constructor.
 *
 * @param text The exception text.
 **/
ParseErrorException::ParseErrorException( const std::string& text ) noexcept : Exception( text ) {}

/**
 * Constructor.
 *
 * @param text The exception text.
 **/
TimeoutException::TimeoutException( const char* text ) noexcept : Exception( text ) {}
/**
 * Constructor.
 *
 * @param text The exception text.
 **/
TimeoutException::TimeoutException( const std::string& text ) noexcept : Exception( text ) {}

/**
 * Constructor.
 *
 * @param text The exception text.
 **/
UnknownTransportException::UnknownTransportException( const char* text ) noexcept : Exception( text ) {}
/**
 * Constructor.
 *
 * @param text The exception text.
 **/
UnknownTransportException::UnknownTransportException( const std::string& text ) noexcept : Exception( text ) {}

} }

