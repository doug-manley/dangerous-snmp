#include "dangerous/snmp/logger.hpp"

#include "time-functions.hpp"
#include "transport/udp.hpp"

#include <string>

#include <iostream>

#include <fcntl.h> //< For "fcntl".
#include <netdb.h> //< For "getaddrinfo".
#include <string.h> //< For "memset".
#include <unistd.h> //< For "close".

namespace dangerous { namespace snmp { namespace transport {

Udp::Udp( std::weak_ptr<Context::PrivateData> context, const std::string& connectionString ) throw( Exception ) :
	BasicSocketTransport( context )
{
	_maximumIncomingMessageSize = RECOMMENDED_MESSAGE_SIZE;
	_maximumOutgoingMessageSize = RECOMMENDED_MESSAGE_SIZE;

	if( logger.system( Logger::TRANSPORT ) ) {
		logger.out() << "Initializing UDP transport with '" << connectionString << "'." << std::endl;
	}

	// Attempt to find the port string.
	
	//! This is the port string.  We will default this to "161" unless
	//! we are told to do otherwise.
	port = "161";

	//! This is the location from which we'll begin looking for our ":"
	//! for the port number.
	size_t startPosition = 0;

	// IPv6 hostnames can be in "bracket notation", especially when a port
	// number is involved (because IPv6 addresses may have colons in them
	// already).
	//
	// This can look like:
	//    [1234:5678::1]:161
	//
	// Here, we take the port number to be whatever has followed the ":"
	// after the final "]". If there is no final "]", then we'll look for
	// the colon starting from the beginning.
	size_t lastBracketPosition = connectionString.rfind( ']' );
	if( lastBracketPosition != std::string::npos ) {
		startPosition = lastBracketPosition;
	}

	size_t colonPosition = connectionString.find( ':', startPosition );
	if( colonPosition != std::string::npos ) {
		port = connectionString.substr( colonPosition + 1 );
	}

	//! This is either the IPv4 address, IPv6 address, or hostname of the node.
	//! Remember, either "colonPosition" is legitimate, in which case we DO
	//! want to stop early, or it's not, in which case this will cause us to
	//! read up until the end of the string, which is what we want.
	address = connectionString.substr( 0, colonPosition );

	if( logger.system( Logger::TRANSPORT ) ) {
		logger.out() << "Node: " << address << std::endl;
		logger.out() << "Port: " << port << std::endl;
	}

	connect();
}

int Udp::socketOptions() const {
	return SOCK_DGRAM;
}

int Udp::protocolNumber() const {
	return 17;
}

} } }

