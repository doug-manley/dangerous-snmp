#include "dangerous/snmp/numericoid.hpp"

#include "dangerous/snmp/logger.hpp"

#include <algorithm>

#include <iostream>
#include <sstream>

namespace dangerous { namespace snmp {

/**
 * Copy constructor; this creates a NumericOid identical to the one given.
 * @param oid The NumericOid to copy.
 **/
NumericOid::NumericOid( const NumericOid& oid ) {
	numbers.resize( oid.numbers.size() );
	for( int i = 0, iSize = oid.numbers.size(); i < iSize; i++ ) {
		numbers[ i ] = oid.numbers[ i ];
	}
}

/**
 * String constructor; this creates a NumericOid representing the text
 * that was given.  In the event that the text does not represent a
 * numeric OID, this will throw a ParseErrorException.
 * @param oidString The string to parse.
 * @throw ParseErrorException
 **/
NumericOid::NumericOid( const std::string& oidString ) throw( ParseErrorException ) {
	if( logger.system( Logger::PARSING ) ) {
		logger.out() << "Attempting to parse numeric OID from '" << oidString << "'." << std::endl;
	}

	// In the event of a "null" OID, we may simply stop now; our list
	// of numbers is already set to be empty.
	if( oidString.length() == 0 ) {
		return;
	}

	unsigned int dotCount = 0;
	bool numberPresent = false;
	char previousCharacter = '\0';
	for( int i = 0, iSize = oidString.length(); i < iSize; i++ ) {
		// Ignore the leading ".", if there is one.
		if( i == 0 && oidString[i] == '.' ) {
			continue;
		}

		if( oidString[i] == '.' ) {
			dotCount++;
		} else if( oidString[i] >= '0' && oidString[i] <= '9' ) {
			numberPresent = true;
		} else {
			throw ParseErrorException( "Invalid character: " + oidString[i] );
		}

		if( previousCharacter == '.' && oidString[i] == '.' ) {
			throw ParseErrorException( "Missing number between dots." );
		}

		previousCharacter = oidString[i];
	}

	if( ! numberPresent ) {
		throw ParseErrorException( "No numbers found." );
	}

	numbers.resize( dotCount + 1, 0 );
	int numberIndex = 0;
	for( int i = 0, iSize = oidString.length(); i < iSize; i++ ) {
		// Ignore the leading ".", if there is one.
		if( i == 0 && oidString[i] == '.' ) {
			continue;
		}

		if( oidString[i] == '.' ) {
			numberIndex++;
			continue;
		}

		numbers[ numberIndex ] *= 10;
		numbers[ numberIndex ] += ( oidString[i] - '0' );
	}
	
	if( logger.system( Logger::PARSING ) ) {
		logger.out() << "Parsed numeric OID:" << std::endl;
		for( const auto& number : numbers ) {
			logger.out() << "   " << number << std::endl;
		}
	}
}

/**
 * C-string constructor; this creates a NumericOid representing the text
 * that was given.  In the event that the text does not represent a
 * numeric OID, this will throw a ParseErrorException.
 * @param oidString The string to parse.
 * @throw ParseErrorException
 **/
NumericOid::NumericOid( const char* oidString ) throw( ParseErrorException ) : NumericOid( std::string( oidString ) ) {
}

/**
 * This compares this NumericOid to another one.
 * If they are equal, then this returns true.
 * @param oid The comparison OID.
 * @return true if they are equal; false otherwise.
 **/
bool NumericOid::equals( const NumericOid& oid ) const {
	if( numbers.size() != oid.numbers.size() ) {
		return false;
	}
	return std::equal( numbers.begin(), numbers.end(), oid.numbers.begin() );
}

/**
 * This compares this NumericOid to another one.
 * If this one is less than the other one, then this
 * returns true.
 * @param oid The comparison OID.
 * @return true if this one is smaller; false otherwise.
 **/
bool NumericOid::isLessThan( const NumericOid& oid ) const {
	return std::lexicographical_compare( numbers.begin(), numbers.end(), oid.numbers.begin(), oid.numbers.end() );
}

/**
 * This checks to see if _this OID_ begins with the exact sequence
 * of the OID given.
 * @param oid The comparison OID.
 * @return true if this one begins with the OID given; false otherwise.
 **/
bool NumericOid::beginsWith( const NumericOid& oid ) const {
	return numbers.size() >= oid.numbers.size() && std::equal( oid.numbers.begin(), oid.numbers.end(), numbers.begin() );
}

/**
 * This returns a string representing the OID.
 * @return A string that can be used to represent the OID.
 **/
std::string NumericOid::str() const {
	//! This is the string stream that we'll be using to construct the string
	//! for this OID.
	std::stringstream stream;

	for( const auto number : numbers ) {
		stream << "." << number;
	}

	// Return the string representation of the stream.
	return stream.str();
}

} }

