#include "dangerous/snmp/context.hpp"

#include "context-private.hpp"
#include "dangerous/snmp/logger.hpp"

#include <thread>

namespace dangerous { namespace snmp {

Context::Context() {
	privateData = std::unique_ptr<PrivateData>( new PrivateData() );
}
Context::~Context() {
	if( logger.system( Logger::GENERAL ) ) {
		logger.out() << "Context::~Context: Destroying I/O System." << std::endl;
	}
	privateData.reset();

	if( logger.system( Logger::GENERAL ) ) {
		logger.out() << "Context::~Context: Destructor complete." << std::endl;
	}
}

} }

