#include "asn1/asn1.hpp"
#include "bytestream.hpp"

#include "dangerous/snmp/logger.hpp"

#include <algorithm>
#include <cstring>
#include <iomanip>
#include <iostream>
#include <sstream>

namespace dangerous { namespace snmp {

/**
 * Default constructor; this creates a new, empty ByteStream.
 **/
ByteStream::ByteStream() {
	_isErrored = false;
	_lastReadTimedOut = false;
	_isLive = false;

	bytesSize = 0;
	bytes = nullptr;
	ownBytes = true;

	readIndex = 0;
	writeIndex = 0;

	_readTimeout = std::chrono::seconds( 5 );
}

/**
 * Destructor.
 **/
ByteStream::~ByteStream() {
	freeBytes();
}

/**
 * This copies the bytes from the given string into the ByteStream's character buffer.
 * @param bytes The string to copy.
 * @param access The kind of access that the ByteStream should have.
 **/
void ByteStream::copyFrom( const std::string& bytes, Access access ) {
	// At this point, we _know_ that our data is ready.  However, we do _not_
	// necessarily know where it is.  The "reset" method may have been called,
	// which would re-organized our storage a bit.  Or it may be in the process
	// of being called right now.
	//
	// So, we're going to obtain a lock to make sure that the character buffer
	// is not modified while we're here.
	if( logger.system( Logger::BYTESTREAM ) ) {
		logger.out() << "ByteStream::copyFrom: Locking liveLock." << std::endl;
	}
	std::unique_lock<std::mutex> writeLock( liveLock );

	// Since this will reset our character buffer, we will first need to free it.
	freeBytes();

	boundaries.clear();

	bytesSize = bytes.length();
	readIndex = 0;
	writeIndex = ( access == READ_ONLY ? bytesSize : 0 );
	this->bytes = new char[ bytesSize ];
	memcpy( this->bytes, bytes.c_str(), bytesSize );
	
	// Because we allocated the memory for the character buffer, we own the bytes.
	ownBytes = true;
}

/**
 * This copies the bytes from the given vector into the ByteStream's character buffer.
 * @param bytes The vector to copy.
 * @param access The kind of access that the ByteStream should have.
 **/
void ByteStream::copyFrom( const std::vector<char>& bytes, Access access ) {
	// At this point, we _know_ that our data is ready.  However, we do _not_
	// necessarily know where it is.  The "reset" method may have been called,
	// which would re-organized our storage a bit.  Or it may be in the process
	// of being called right now.
	//
	// So, we're going to obtain a lock to make sure that the character buffer
	// is not modified while we're here.
	if( logger.system( Logger::BYTESTREAM ) ) {
		logger.out() << "ByteStream::copyFrom: Locking liveLock." << std::endl;
	}
	std::unique_lock<std::mutex> writeLock( liveLock );

	// Since this will reset our character buffer, we will first need to free it.
	freeBytes();

	boundaries.clear();

	bytesSize = bytes.size();
	readIndex = 0;
	writeIndex = ( access == READ_ONLY ? bytesSize : 0 );
	this->bytes = new char[ bytesSize ];
	memcpy( this->bytes, bytes.data(), bytesSize );
	
	// Because we allocated the memory for the character buffer, we own the bytes.
	ownBytes = true;
}

/**
 * This copies the bytes from the given ByteStream into the ByteStream's character buffer.
 * Note that this copies the _remaining_ bytes in the ByteStream.
 * @param bytes The ByteStream to copy.
 * @param access The kind of access that the ByteStream should have.
 **/
void ByteStream::copyFrom( const ByteStream& byteStream, Access access ) {
	// At this point, we _know_ that our data is ready.  However, we do _not_
	// necessarily know where it is.  The "reset" method may have been called,
	// which would re-organized our storage a bit.  Or it may be in the process
	// of being called right now.
	//
	// So, we're going to obtain a lock to make sure that the character buffer
	// is not modified while we're here.
	if( logger.system( Logger::BYTESTREAM ) ) {
		logger.out() << "ByteStream::copyFrom: Locking liveLock." << std::endl;
	}
	std::unique_lock<std::mutex> writeLock( liveLock );

	// Since this will reset our character buffer, we will first need to free it.
	freeBytes();

	boundaries.clear();

	bytesSize = byteStream.remainingReadLength_nolock();
	readIndex = 0;
	writeIndex = ( access == READ_ONLY ? bytesSize : 0 );
	this->bytes = new char[ bytesSize ];

	std::vector<char> buffer;
	byteStream.copyTo( buffer );
	// TODO: Check to see if byteStream.remainingReadLength_nolock() is >= bytesSize?
	memcpy( this->bytes, buffer.data(), bytesSize );
	
	// Because we allocated the memory for the character buffer, we own the bytes.
	ownBytes = true;
}

/**
 * This links the bytes given to the ByteStream's character buffer.
 * This is essentially creating a simple ByteStream wrapper around the buffer that already exists.
 * You _must not_ free "bytes" until this ByteStream is destroyed (or is no longer used).
 * @param bytes The character array to use.
 * @param bytesSize The length of the character array to use.
 * @param access The kind of access that the ByteStream should have.
 **/
void ByteStream::linkFrom( char* bytes, unsigned int bytesSize, Access access ) {
	// At this point, we _know_ that our data is ready.  However, we do _not_
	// necessarily know where it is.  The "reset" method may have been called,
	// which would re-organized our storage a bit.  Or it may be in the process
	// of being called right now.
	//
	// So, we're going to obtain a lock to make sure that the character buffer
	// is not modified while we're here.
	if( logger.system( Logger::BYTESTREAM ) ) {
		logger.out() << "ByteStream::linkFrom: Locking liveLock." << std::endl;
	}
	std::unique_lock<std::mutex> writeLock( liveLock );

	// Since this will reset our character buffer, we will first need to free it.
	freeBytes();

	boundaries.clear();

	this->bytesSize = bytesSize;
	this->bytes = bytes;
	readIndex = 0;
	writeIndex = ( access == READ_ONLY ? bytesSize : 0 );
	
	// Since we are simply pointing our bytes at the array that was passed in, we do not own
	// the contents of the "bytes" array.
	ownBytes = false;
}

/**
 * This copies the portion of the ByteStream that's currently available for reading
 * to the specified target.
 * @param buffer The vector to replace.
 **/
void ByteStream::copyTo( std::vector<char>& buffer ) const {
	// At this point, we _know_ that our data is ready.  However, we do _not_
	// necessarily know where it is.  The "reset" method may have been called,
	// which would re-organized our storage a bit.  Or it may be in the process
	// of being called right now.
	//
	// So, we're going to obtain a lock to make sure that the character buffer
	// is not modified while we're here.
	if( logger.system( Logger::BYTESTREAM ) ) {
		logger.out() << "ByteStream::copyTo: Locking liveLock." << std::endl;
	}
	std::unique_lock<std::mutex> writeLock( liveLock );

	unsigned int newLength = remainingReadLength_nolock();
	const char* newBytes = bytes + readIndex;
	buffer.resize( newLength );
	for( unsigned int i = 0; i < newLength; i++ ) {
		buffer[ i ] = newBytes[ i ];
	}
}

/**
 * This copies the portion of the ByteStream that's currently available for reading
 * to the specified target.
 * @param buffer The string to replace.
 **/
void ByteStream::copyTo( std::string& buffer ) const {
	// At this point, we _know_ that our data is ready.  However, we do _not_
	// necessarily know where it is.  The "reset" method may have been called,
	// which would re-organized our storage a bit.  Or it may be in the process
	// of being called right now.
	//
	// So, we're going to obtain a lock to make sure that the character buffer
	// is not modified while we're here.
	if( logger.system( Logger::BYTESTREAM ) ) {
		logger.out() << "ByteStream::copyTo: Locking liveLock." << std::endl;
	}
	std::unique_lock<std::mutex> writeLock( liveLock );

	unsigned int newLength = remainingReadLength_nolock();
	const char* newBytes = bytes + readIndex;
	buffer.resize( newLength );
	for( unsigned int i = 0; i < newLength; i++ ) {
		buffer[ i ] = newBytes[ i ];
	}
}

/**
 * This compares the ByteStream to the given character buffer and returns the equivalent
 * of "memcmp".
 * @param buffer The character buffer to compare it with.
 * @param bufferLength The length of the character buffer.
 * @return -1 if the ByteStream is smaller, 0 if it is equal, or +1 if it is greater than the buffer.
 **/
int ByteStream::compareWith( const char* buffer, unsigned int bufferLength ) const {
	// At this point, we _know_ that our data is ready.  However, we do _not_
	// necessarily know where it is.  The "reset" method may have been called,
	// which would re-organized our storage a bit.  Or it may be in the process
	// of being called right now.
	//
	// So, we're going to obtain a lock to make sure that the character buffer
	// is not modified while we're here.
	if( logger.system( Logger::BYTESTREAM ) ) {
		logger.out() << "ByteStream::compareWith: Locking liveLock." << std::endl;
	}
	std::unique_lock<std::mutex> writeLock( liveLock );

	unsigned int newLength = remainingReadLength_nolock();
	const char* newBytes = bytes + readIndex;

	return memcmp( newBytes, buffer, std::min( newLength, bufferLength ) );
}

/**
 * This sets the "live" state of the stream.
 * @param isLive The new live state of the stream.
 **/
void ByteStream::setLive( bool isLive ) {
	_isLive = isLive;

	// If we are _no longer live_, then anyone who was listening (waiting) for
	// new data to come in should be woken up and informed that he won't be
	// getting any more.
	if( ! _isLive ) {
		if( logger.system( Logger::BYTESTREAM ) ) {
			logger.out() << "ByteStream::setLive: Locking liveLock." << std::endl;
		}
		std::unique_lock<std::mutex> writeLock( liveLock );

		if( logger.system( Logger::BYTESTREAM ) ) {
			logger.out() << "ByteStream::setLive: Notifying all." << std::endl;
		}
		liveConditionVariable.notify_all();
	}
}

/**
 * This returns the number of bytes remaining in the stream.
 * @return The number of bytes remaining.
 **/
unsigned int ByteStream::remainingReadLength() const {
	// At this point, we _know_ that our data is ready.  However, we do _not_
	// necessarily know where it is.  The "reset" method may have been called,
	// which would re-organized our storage a bit.  Or it may be in the process
	// of being called right now.
	//
	// So, we're going to obtain a lock to make sure that the character buffer
	// is not modified while we're here.
	if( logger.system( Logger::BYTESTREAM ) ) {
		logger.out() << "ByteStream::remainingReadLength: Locking liveLock." << std::endl;
	}
	std::unique_lock<std::mutex> writeLock( liveLock );

	return remainingReadLength_nolock();
}

/**
 * This returns the number of bytes remaining in the stream.
 * @return The number of bytes remaining.
 **/
unsigned int ByteStream::remainingWriteLength() const {
	// At this point, we _know_ that our data is ready.  However, we do _not_
	// necessarily know where it is.  The "reset" method may have been called,
	// which would re-organized our storage a bit.  Or it may be in the process
	// of being called right now.
	//
	// So, we're going to obtain a lock to make sure that the character buffer
	// is not modified while we're here.
	if( logger.system( Logger::BYTESTREAM ) ) {
		logger.out() << "ByteStream::remainingWriteLength: Locking liveLock." << std::endl;
	}
	std::unique_lock<std::mutex> writeLock( liveLock );

	return remainingWriteLength_nolock();
}

/**
 * This resets the internal character buffer (if owned by the ByteStream).
 * This will _not_ free any memory, but it will copy the current data in
 * the ByteStream to the _beginning_ of the buffer; this way, it will not
 * continue to grow unchecked indefinitely.
 **/
void ByteStream::reset() {
	// If we are not the true owners of the buffer, then we cannot do anything
	// to mess with it.
	if( ! ownBytes ) {
		return;
	}

	if( logger.system( Logger::BYTESTREAM ) ) {
		logger.out() << "ByteStream::reset: Locking liveLock." << std::endl;
	}
	std::unique_lock<std::mutex> writeLock( liveLock );

	unsigned int copyLength = remainingReadLength_nolock();
	if( copyLength >= readIndex ) {
		if( logger.system( Logger::BYTESTREAM ) ) {
			logger.out() << "ByteStream::reset: There is not enough space yet to copy the remaining bytes." << std::endl;
		}
		return;
	}
	
	unsigned int offset = readIndex;
	if( logger.system( Logger::BYTESTREAM ) ) {
		logger.out() << "ByteStream::reset: Offset: " << offset << " (that's how many bytes will be reclaimed)." << std::endl;
	}

	if( offset < 500 * 1000 ) {
		if( logger.system( Logger::BYTESTREAM ) ) {
			logger.out() << "ByteStream::reset: We will only attempt to reclaim 500KB or more, but we only have " << ( offset / 1000 ) << "KB right now." << std::endl;
		}
		return;
	}

	// Simply copy the memory from the wherever it is in the buffer to the front.
	// (We have already made sure that nothing bad can happen, here.)
	memcpy( bytes, bytes + readIndex, copyLength );

	// Update all of the boundaries.
	for( auto& boundary : boundaries ) {
		if( logger.system( Logger::BYTESTREAM ) ) {
			logger.out() << "ByteStream::reset: Boundary " << boundary << " shifting to " << ( boundary - offset ) << "." << std::endl;
		}
		boundary -= offset;
	}

	readIndex -= offset;
	writeIndex -= offset;
	if( logger.system( Logger::BYTESTREAM ) ) {
		logger.out() << "ByteStream::reset: New read index: " << readIndex << "." << std::endl;
		logger.out() << "ByteStream::reset: New write index: " << writeIndex << "." << std::endl;
	}

	if( logger.system( Logger::BYTESTREAM ) ) {
		logger.out() << "ByteStream::reset: Notifying all." << std::endl;
	}
	liveConditionVariable.notify_all();
}

/**
 * This reads a byte from the ByteStream.
 * However, this does _not_ advance the read head of the ByteStream.
 * @param byte A reference to the byte to read.
 * @return true on success, false on failure.
 **/
bool ByteStream::peekByte( char& byte ) {
	if( isErrored() ) {
		return false;
	}

	// We expect this function to be run at the same time as the "write" functions.
	// However, we do _not_ want to lock this early because "waitForReading" will
	// obtain its own lock.  Since that's going to happen anyway, we're also going
	// to use the locking version of "remainingReadLength", too.

	if( remainingReadLength() < 1 && ! waitForReading(1) ) {
		return false;
	}

	// At this point, we _know_ that our data is ready.  However, we do _not_
	// necessarily know where it is.  The "reset" method may have been called,
	// which would re-organized our storage a bit.  Or it may be in the process
	// of being called right now.
	//
	// So, we're going to obtain a lock to make sure that the character buffer
	// is not modified while we're here.
	if( logger.system( Logger::BYTESTREAM ) ) {
		logger.out() << "ByteStream::peekByte: Locking liveLock." << std::endl;
	}
	std::unique_lock<std::mutex> writeLock( liveLock );

	// Read the byte _without_ updating the read index.
	byte = bytes[ readIndex ];

	return true;
}

/**
 * This reads a byte from the ByteStream.
 * @param byte A reference to the character to store the byte.
 * @return true on success, false on failure.
 **/
bool ByteStream::readByte( char& byte ) {
	unsigned int bytesRead = 0;
	
	return readBytes( &byte, sizeof(byte), bytesRead );
}

/**
 * This reads an arbitrary amount of bytes into the specified character buffer.
 * @param buffer The character buffer in which to store the read bytes.
 * @param bufferSize The number of bytes to read.  "buffer" must be _at least_ this size.
 * @param bytesRead A reference to an integer to hold the number of bytes that were actually read.
 **/
bool ByteStream::readBytes( char* buffer, unsigned int bufferSize, unsigned int& bytesRead ) {
	if( isErrored() ) {
		return false;
	}

	_lastReadTimedOut = false;

	// We expect this function to be run at the same time as the "write" functions.
	// However, we do _not_ want to lock this early because "waitForReading" will
	// obtain its own lock.  Since that's going to happen anyway, we're also going
	// to use the locking version of "remainingReadLength", too.

	bytesRead = 0;
	unsigned int currentLength = remainingReadLength();
	bytesRead = bufferSize;
	if( currentLength < bufferSize ) {
		unsigned int difference = bufferSize - currentLength;
		bool success = waitForReading( difference );
		if( ! success ) {
			// TODO: ERROR MESSAGE?  ERRORED?
			return false;
		}

		currentLength = remainingReadLength();
		if( currentLength < bufferSize ) {
			bytesRead = currentLength;
		}
	}

	// At this point, we _know_ that our data is ready.  However, we do _not_
	// necessarily know where it is.  The "reset" method may have been called,
	// which would re-organized our storage a bit.  Or it may be in the process
	// of being called right now.
	//
	// So, we're going to obtain a lock to make sure that the character buffer
	// is not modified while we're here.
	if( logger.system( Logger::BYTESTREAM ) ) {
		logger.out() << "ByteStream::readBytes: Locking liveLock." << std::endl;
	}
	std::unique_lock<std::mutex> writeLock( liveLock );

	memcpy( buffer, bytes + readIndex, bytesRead );
	readIndex += bytesRead;

	// TODO: TEST THIS OUT FOR REAL.
	// TODO: IF WE HAVE JUST READ ALL OF THE BYTES THAT WE NEEDED TO,
	// TODO: WE SHOULD FIRST REMOVE THE FIRST BOUNDARY IF IT HAPPENS
	// TODO: TO BE THE ONE THAT WE'RE STANDING ON.
	if( logger.system( Logger::BYTESTREAM ) ) {
		logger.out() << "ByteStream::readBytes: readIndex: " << readIndex << ", boundaries: " << boundaries.size() << ", first is at " << ( boundaries.size() > 0 ? boundaries.front() : 0 ) << std::endl;
	}
	if( boundaries.size() > 0 && readIndex == boundaries.front() ) {
		if( logger.system( Logger::BYTESTREAM ) ) {
			logger.out() << "ByteStream::readBytes: Popping off first boundary." << std::endl;
		}
		boundaries.pop_front();
	}

	return bytesRead == bufferSize;
}

/**
 * This writes a byte to the ByteStream.
 * @param byte The byte to write.
 * @return true on success, false on failure.
 **/
bool ByteStream::writeByte( char byte ) {
	unsigned int bytesWritten = 0;

	return writeBytes( &byte, sizeof(byte), bytesWritten );
}

/**
 * This writes the bytes given to the ByteStream.
 * @param buffer The bytes to write.
 * @param bufferSize The number of bytes to write.  "buffer" must be _at least_ this size.
 * @param bytesWritten A reference to an integer to store the number of bytes actually written.
 * @return true on success, false on failure.
 **/
bool ByteStream::writeBytes( const char* buffer, unsigned int bufferSize, unsigned int& bytesWritten ) {
	// Initialize the number of bytes written to zero, since we haven't written any yet.
	bytesWritten = 0;

	if( logger.system( Logger::BYTESTREAM ) ) {
		logger.out() << "ByteStream::writeBytes: Locking liveLock." << std::endl;
	}
	std::unique_lock<std::mutex> writeLock( liveLock );

	// TODO: Allow partial writes?
	if( remainingWriteLength_nolock() < bufferSize && ! grow( bufferSize ) ) {
		return false;
	}

	// TODO: Use memcpy?
	for( unsigned int i = 0; i < bufferSize; i++ ) {
		bytes[ writeIndex ] = *buffer;
		writeIndex++;
		buffer++;
		bytesWritten++;

		// TODO: Overflow?
	}
	if( logger.system( Logger::BYTESTREAM ) ) {
		logger.out() << "ByteStream::writeBytes: Wrote " << bufferSize << " bytes." << std::endl;
	}
	
	if( logger.system( Logger::BYTESTREAM ) ) {
		logger.out() << "ByteStream::writeBytes: Notifying all." << std::endl;
	}
	liveConditionVariable.notify_all();

	return true;
}

/**
 * This ends the current packet and starts a new one.
 **/
void ByteStream::endPacket() {
	if( logger.system( Logger::BYTESTREAM ) ) {
		logger.out() << "ByteStream::endPacket: Locking liveLock." << std::endl;
	}
	std::unique_lock<std::mutex> writeLock( liveLock );

	if( logger.system( Logger::BYTESTREAM ) ) {
		logger.out() << "ByteStream::endPacket: New boundary at " << writeIndex << "." << std::endl;
	}
	boundaries.push_back( writeIndex );

	if( logger.system( Logger::BYTESTREAM ) ) {
		logger.out() << "ByteStream::endPacket: Notifying all." << std::endl;
	}
	liveConditionVariable.notify_all();
}

/**
 * This returns whether or not there is a next packet.
 * @return true if there is a next packet, false if there is not.
 **/
bool ByteStream::hasNextPacket() {
	// At this point, we _know_ that our data is ready.  However, we do _not_
	// necessarily know where it is.  The "reset" method may have been called,
	// which would re-organized our storage a bit.  Or it may be in the process
	// of being called right now.
	//
	// So, we're going to obtain a lock to make sure that the character buffer
	// is not modified while we're here.
	if( logger.system( Logger::BYTESTREAM ) ) {
		logger.out() << "ByteStream::hasNextPacket: Locking liveLock." << std::endl;
	}
	std::unique_lock<std::mutex> writeLock( liveLock );

	return boundaries.size() > 0 && boundaries.front() > readIndex;
}

/**
 * This advances the ByteStream to the next packet, if there is one.
 * @return true if the ByteStream actually advanced to a new packet, false if it did not.
 **/
bool ByteStream::nextPacket() {
	// At this point, we _know_ that our data is ready.  However, we do _not_
	// necessarily know where it is.  The "reset" method may have been called,
	// which would re-organized our storage a bit.  Or it may be in the process
	// of being called right now.
	//
	// So, we're going to obtain a lock to make sure that the character buffer
	// is not modified while we're here.
	if( logger.system( Logger::BYTESTREAM ) ) {
		logger.out() << "ByteStream::nextPacket: Locking liveLock." << std::endl;
	}
	std::unique_lock<std::mutex> writeLock( liveLock );

	if( logger.system( Logger::BYTESTREAM ) ) {
		logger.out() << "ByteStream::nextPacket: There are " << boundaries.size() << " boundary points." << std::endl;
	}
	while( boundaries.size() > 0 ) {
		unsigned int nextIndex = boundaries.front();
		boundaries.pop_front();
		if( nextIndex < readIndex ) {
			if( logger.system( Logger::BYTESTREAM ) ) {
				logger.out() << "ByteStream::nextPacket: Ignoring too-soon boundary point " << nextIndex << " (vs. " << readIndex << ")." << std::endl;
			}
			continue;
		} else if( nextIndex == readIndex ) {
			if( logger.system( Logger::BYTESTREAM ) ) {
				logger.out() << "ByteStream::nextPacket: Moving past current boundary point " << nextIndex << "." << std::endl;
			}
			continue;
		}

		if( logger.system( Logger::BYTESTREAM ) ) {
			logger.out() << "ByteStream::nextPacket: Resetting readIndex to " << nextIndex << " (vs. " << readIndex << " )." << std::endl;
		}
		readIndex = nextIndex;
		_isErrored = false;
		return true;
	}

	return false;
}

/**
 * This returns a string that can be printed in order to see debugging information about
 * the ByteStream, including the position of the read and write heads, as well as a hexadecimal
 * printout of the stream (wrapped every 50 bytes).
 * @return A string with debugging information.
 **/
std::string ByteStream::debugString() {
	// At this point, we _know_ that our data is ready.  However, we do _not_
	// necessarily know where it is.  The "reset" method may have been called,
	// which would re-organized our storage a bit.  Or it may be in the process
	// of being called right now.
	//
	// So, we're going to obtain a lock to make sure that the character buffer
	// is not modified while we're here.
	if( logger.system( Logger::BYTESTREAM ) ) {
		logger.out() << "ByteStream::debugString: Locking liveLock." << std::endl;
	}
	std::unique_lock<std::mutex> writeLock( liveLock );

	std::stringstream stream;
	for( unsigned int i = 0; i < bytesSize; i++ ) {
		if( i % 50 == 0 && i > 0 ) {
			stream << "\n";
		}

		char symbol = ' ';
		if( readIndex == i ) {
			symbol = '>';
		} else if( writeIndex == i ) {
			symbol = '<';
		} else {
			for( auto boundary : boundaries ) {
				if( boundary == i ) {
					symbol = '*';
					break;
				}
			}
		}
		stream << (char)symbol;
		stream << std::setw( 2 ) << std::hex;
		stream << (int)bytes[ i ];
	}

	return stream.str();
}

/**
 * This frees the memory associated with the internal character buffer.
 * Note, however, that this has _no effect_ if the ByteStream does _not_
 * own its own buffer.
 *
 * If its bytes are simply linked, then this function does nothing.
 **/
void ByteStream::freeBytes() {
	// If we own our character buffer, then we will need to free it.
	if( ownBytes ) {
		delete [] bytes;
		bytesSize = 0;
		readIndex = 0;
		writeIndex = 0;
	}
}

/**
 * This returns the number of bytes remaining in the stream.
 * @return The number of bytes remaining.
 **/
unsigned int ByteStream::remainingReadLength_nolock() const {
	//! This is our current end point.  Ordinarily, this is the point
	//! where the write head is.
	unsigned int endPoint = writeIndex;
	
	// However, if there are any boundaries, then we need to take those
	// into account.  We'll take the *first* boundary and use that if
	// it's clsoer than the write head.
	if( boundaries.size() > 0 ) {
		if( boundaries.front() <= endPoint ) {
			endPoint = boundaries.front();
		}
	}
	return endPoint - readIndex;
}

/**
 * This returns the number of bytes remaining in the stream.
 * @return The number of bytes remaining.
 **/
unsigned int ByteStream::remainingWriteLength_nolock() const {
	return bytesSize - writeIndex;
}

/**
 * This attempts to wait for the given number of bytes to become available
 * for reading.  If they all could not become available, then this returns
 * false.
 * Note that if this stream is not "live", then no amount of waiting will
 * help; the bytes are either there, or they're not.  And if we've called
 * *this* function, then we know that they're not there.
 * @return true if "increment" bytes may now be read; false otherwise.
 **/
bool ByteStream::waitForReading( unsigned int increment ) {
	// Waiting for input is only valid for streaming ByteStreams.
	// If this one is not streaming, then we're going to have to
	// return failure.
	if( ! isLive() ) {
		return false;
	}

	// Lock the mutex so that we may have access to:
	//    * boundaries
	//    * readIndex
	std::unique_lock<std::mutex> uniqueLock( liveLock );

	// TODO: TEST THIS OUT FOR REAL.
	// TODO: IF WE ARE GOING TO WAIT FOR ANY BYTES TO COME IN,
	// TODO: WE SHOULD FIRST REMOVE THE FIRST BOUNDARY IF IT HAPPENS
	// TODO: TO BE THE ONE THAT WE'RE STANDING ON.
	if( boundaries.size() > 0 && boundaries.front() == readIndex ) {
		boundaries.pop_front();
	}


	// TODO: TEST THIS TOO
	if( boundaries.size() > 0 && boundaries.front() - readIndex < increment ) {
		if( logger.system( Logger::BYTESTREAM ) ) {
			logger.out() << "ByteStream::waitForReading: The first boundary at " << boundaries.front() << " prevents us from waiting for " << increment << " bytes." << std::endl;
		}
		_isErrored = true;
		return false;
	}


	if( logger.system( Logger::BYTESTREAM ) ) {
		logger.out() << "ByteStream::waitForReading(" << increment << "): Waiting for data." << std::endl;
	}
	_lastReadTimedOut = false;
	bool success = liveConditionVariable.wait_for(
		uniqueLock,
		_readTimeout,
		[ this, increment ]() {
			if( isErrored() ) {
				if( logger.system( Logger::BYTESTREAM ) ) {
					logger.out() << "ByteStream::waitForReading(" << increment << "): Woke up to an error :(" << std::endl;
				}
				return true; //< We need to return true in order to get out of "wait".
			}
	
			if( boundaries.size() > 0 ) {
				if( logger.system( Logger::BYTESTREAM ) ) {
					logger.out() << "ByteStream::waitForReading(" << increment << "): Next boundary is at " << boundaries.front() << " vs. " << readIndex << "." << std::endl;
				}
				if( readIndex + increment >= boundaries.front() ) {
					if( logger.system( Logger::BYTESTREAM ) ) {
						logger.out() << "ByteStream::waitForReading(" << increment << "): Hit a boundary :(" << std::endl;
					}
					_isErrored = true;
					return true; //< We need to return true in order to get out of "wait".
				}
			}
		
			if( logger.system( Logger::BYTESTREAM ) ) {
				logger.out() << "ByteStream::waitForReading(" << increment << "): Otherwise, remaining read length is " << remainingReadLength_nolock() << "." << std::endl;
				logger.out() << "ByteStream::waitForReading(" << increment << "): readIndex: " << readIndex << ", writeIndex: " << writeIndex << std::endl;
			}
			return remainingReadLength_nolock() >= increment;
		}
	);
	if( ! success ) {
		if( logger.system( Logger::BYTESTREAM ) ) {
			logger.out() << "ByteStream::waitForReading(" << increment << "): Done waiting for data (timeout)." << std::endl;
		}
		_lastReadTimedOut = true;
		return false;
	}
	if( isErrored() ) {
		if( logger.system( Logger::BYTESTREAM ) ) {
			logger.out() << "ByteStream::waitForReading(" << increment << "): Done waiting for data (error)." << std::endl;
		}
		return false;
	}
	
	if( logger.system( Logger::BYTESTREAM ) ) {
		logger.out() << "ByteStream::waitForReading(" << increment << "): Done waiting for data (success)." << std::endl;
	}

	return true;
}
	
/**
 * This attempts to increase the size of the buffer by the specified
 * amount.  Note, however, that if this ByteStream doesn't OWN its
 * buffer, then this call will fail.
 **/
bool ByteStream::grow( unsigned int increment ) {
	// If we trivially have enough extra space, then we don't actually
	// need to grow our buffer at all.
	if( writeIndex + increment < bytesSize ) {
		if( logger.system( Logger::BYTESTREAM ) ) {
			logger.out() << "ByteStream::grow(" << increment << "): No growth necessary." << std::endl;
		}
		return true;
	}

	if( ! ownBytes ) {
		if( logger.system( Logger::BYTESTREAM ) ) {
			logger.out() << "ByteStream::grow(" << increment << "): We are forbidden to grow." << std::endl;
		}
		return false;
	}

	unsigned int originalBytesSize = bytesSize;
	char* originalBytes = bytes;
	
	bytesSize *= 2;
	if( bytesSize < writeIndex + increment ) {
		bytesSize = writeIndex + increment;
	}
	
	if( logger.system( Logger::BYTESTREAM ) ) {
		logger.out() << "ByteStream::grow(" << increment << "): Increasing from " << originalBytesSize << " to " << bytesSize << "." << std::endl;
	}
	
	bytes = new char[ bytesSize ];
	memcpy( bytes, originalBytes, originalBytesSize );

	delete [] originalBytes;

	return true;
}

} }

