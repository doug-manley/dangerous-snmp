#include <iostream>
#include <thread>
#include <sstream>
#include <stdexcept>
using namespace std;
	    
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>

#include "bytestream.hpp"
using namespace dangerous;

#include <unittest++/UnitTest++.h>

SUITE(Fast) {

TEST(Test_ByteStream_RO) {
	snmp::ByteStream byteStream;
	byteStream.copyFrom( std::string("12345678"), snmp::ByteStream::READ_ONLY );

	char buffer[ 256 ];
	unsigned int bytesRead = 0;
	CHECK( byteStream.readBytes( buffer, 4, bytesRead ) );
	CHECK( bytesRead == 4 );
	
	CHECK( byteStream.readBytes( buffer, 2, bytesRead ) );
	CHECK( bytesRead == 2 );
	
	CHECK( byteStream.readByte( buffer[6] ) );
	CHECK( byteStream.readByte( buffer[7] ) );
}

}

