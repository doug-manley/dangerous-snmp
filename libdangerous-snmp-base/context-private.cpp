#include "context-private.hpp"

namespace dangerous { namespace snmp {

Context::PrivateData::PrivateData() {
	io.start();
}

Context::PrivateData::~PrivateData() {
	io.stop();
}

} }

