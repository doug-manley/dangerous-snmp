#include <cstdlib>
#include <iostream>
#include <list>
#include <string>
#include <thread>
using namespace std;

#include <dangerous/snmp/client.hpp>
#include <dangerous/snmp/logger.hpp>
using namespace dangerous;

void usage() {
	std::cout <<
		"Usage:\n"
		"   snmp-client [<option>[ ...]] <address> <command> <command-specific parameters>\n"
		"\n"
		"This is a general-purpose SNMP client tool.\n"
		"\n"
		"<command> must be one of:\n"
		"   get <oid>[ ...]\n"
		"   getnext <oid>[ ...]\n"
		"   getbulk <oid>[ ...] [--repetitions <#> <oid>[ ...]]\n"
		"   walk <oid>[ ...]\n"
		"   set <oid> <type> <value>[ ...]\n"
		"   raw-get <oid>[...]\n"
		"   raw-getnext <oid>[ ...]\n"
		"   raw-getbulk <oid>[ ...] [--repetitions <#> <oid>[ ...]]\n"
		"   raw-set <oid> <type> <value>[ ...]\n"
		"\n"
		"<type> must be one of:\n"
		"   n: null (NULL)\n"
		"   i: 32-bit integer (INTEGER)\n"
		"   u: 32-bit unsigned integer (Gauge32 or Integer32)\n"
		"   c: 32-bit counter (Counter32)\n"
		"   C: 64-bit counter (Counter64)\n"
		"   t: time ticks (TimeTicks)\n"
		"   o: OID (OBJECT IDENTIFIER)\n"
		"   s: byte string (OCTET STRING)\n"
		"   4: IPv4 address (IpAddress)\n"
		"   q: opaque (OPAQUE)\n"
		"Note that when using 'n' (null), you must still specify a throwaway value.\n"
		"\n"
		"<option> may include:\n"
		"   -v1 \n"
		"      -c<string>    | Use a community string of <string>.\n"
		"   -v2[c] \n"
		"      -c<string>    | Use a community string of <string>.\n"
		"   -v3 \n"
		"      -u<name>      | Use a user name of <name>.\n"
		"      [if using authentication]\n"
		"      -a{MD5,SHA}   | Set the appropriate authentication method.\n"
		"      -A<password>  | Set the authentication password.\n"
		"      [if using encryption]\n"
		"      -x{DES,AES}   | Set the appropriate encryption method.\n"
		"      -X<password>  | Set the encryption password.\n"
		"\n"
		"   --timeout <#>    | Set the timeout to <#> milliseconds.\n"
		"\n"
		"   --loop <#>       | Execute <command> <#> times (default: 1).\n"
		"   --loop-delay <#> | Wait <#> seconds between loop iterations (default: 1).\n"
		"\n"
		"   --log <name>     | Enable logging for <name>.\n"
		"      none          |    Disable all logging.\n"
		"      all           |    Enable all logging.\n"
		"      [specifics]   |\n"
		"      bytestream    |    Enable ByteStream logging.\n"
		"      client        |    Enable Client logging.\n"
		"      decoding      |    Enable decoding logging.\n"
		"      encoding      |    Enable encoding logging.\n"
		"      general       |    Enable general logging.\n"
		"      io            |    Enable I/O logging.\n"
		"      parsing       |    Enable parsing logging.\n"
		"      transport     |    Enable Transport logging.\n"
		"";
	exit( EXIT_FAILURE );
}

int main( int argc, char** argv ) {
	//! This is the SNMP version to use.
	int version = 0;
	//! This is the SNMPv1 or SNMPv2c community string to use.
	std::string communityString;
	//! This is the SNMPv3/USM username.
	std::string username;
	//! This is the SNMPv3/USM authentication type.
	//! This is not required, and will default to "no authentication"
	//! if no relevant arguments are given.
	snmp::Authentication::Type authentication = snmp::Authentication::NONE;
	//! This is the authentication "password".
	//! This will be used to generate a "key" of known length.
	std::string authenticationPassword;
	//! This is the SNMPv3/USM encryption type.
	//! This is not required, and will default to "no encryption"
	//! if no relevant arguments are given.
	snmp::Encryption::Type encryption = snmp::Encryption::NONE;
	//! This is the encryption "password".
	//! This will be used to generate a "key" of known length.
	std::string encryptionPassword;
	
	//! This is whether or not we have been given an address.
	bool hasAddress = false;
	//! This is the address (connection string) to use.
	std::string address;

	//! This is whether or not we have been given a timeout.
	bool hasTimeout = false;
	//! This is the timeout to use for the client.
	std::chrono::milliseconds timeout;

	//! This is whether or not we have been given a command.
	bool hasCommand = false;
	//! This is the command that we're going to use.
	std::string command;

	//! This is the number of times to execute the command.
	int loop = 1; //< By default, just do it once.
	//! This is the number of seconds to wait between loop iterations.
	std::chrono::seconds loopDelay( 1 );
	
	//! This is the list of OIDs that we'll be putting into the PDU.
	std::list<snmp::NumericOid> oids;
	//! If "repetitions" is nonzero, then this is the the list of OIDs
	//! that will be repeated in a "getbulk" request.
	std::list<snmp::NumericOid> multipleRequestOids;
	//! This is the number of times to repeat "multipleRequestOids" in
	//! a "getbulk" request.
	unsigned int repetitions = 0;
	//! For "set" operations: This is the list of OID types; it is
	//! indexed identically to the "oids" array.
	std::list<std::string> oidTypes;
	//! For "set" operations: This is the list of OID values; it is
	//! indexed identically to the "oids" array.
	std::list<std::string> oidValues;

	// Go through the parameters that were passed in.
	for( int i = 1; i < argc; i++ ) {
		//! This is the current parameter.
		std::string parameter( argv[i] );

		// There are two classes of parameters:
		//    1. Those that start with "-".
		//    2. Those that don't.
		if( parameter.length() > 0 && parameter[0] == '-' ) {
			// This parameter starts with "-", which means that it is
			// some kind of option.
			//
			// If there is another "-" following it, then it is a "long"
			// option (that is, one that begins with "--").
			if( parameter.length() > 1 && parameter[1] == '-' ) {
				// Note: this is a long option.
				
				if( parameter.compare( "--help" ) == 0 ) {
					usage();
				} else if( parameter.compare( "--repetitions" ) == 0 ) {
					if( i + 1 >= argc ) {
						usage();
					}
					parameter = argv[ ++i ];
					repetitions = atoi( parameter.c_str() );
					if( repetitions == 0 ) {
						usage();
					}
				} else if( parameter.compare( "--timeout" ) == 0 ) {
					if( i + 1 >= argc ) {
						usage();
					}
					parameter = argv[ ++i ];
					timeout = std::chrono::milliseconds( atoi( parameter.c_str() ) );
					hasTimeout = true;
				} else if( parameter.compare( "--log" ) == 0 ) {
					if( i + 1 >= argc ) {
						usage();
					}
					parameter = argv[ ++i ];

					if( parameter.compare( "all" ) == 0 ) {
						for( int i = 0; i < snmp::Logger::MAX; i++ ) {
							snmp::logger.system( (snmp::Logger::System)i, true );
						}
					} else if( parameter.compare( "none" ) == 0 ) {
						for( int i = 0; i < snmp::Logger::MAX; i++ ) {
							snmp::logger.system( (snmp::Logger::System)i, false );
						}
					} else if( parameter.compare( "agentx" ) == 0 ) {
						snmp::logger.system( snmp::Logger::AGENTX, true );
					} else if( parameter.compare( "bytestream" ) == 0 ) {
						snmp::logger.system( snmp::Logger::BYTESTREAM, true );
					} else if( parameter.compare( "client" ) == 0 ) {
						snmp::logger.system( snmp::Logger::CLIENT, true );
					} else if( parameter.compare( "decoding" ) == 0 ) {
						snmp::logger.system( snmp::Logger::DECODING, true );
					} else if( parameter.compare( "encoding" ) == 0 ) {
						snmp::logger.system( snmp::Logger::ENCODING, true );
					} else if( parameter.compare( "general" ) == 0 ) {
						snmp::logger.system( snmp::Logger::GENERAL, true );
					} else if( parameter.compare( "io" ) == 0 ) {
						snmp::logger.system( snmp::Logger::IO, true );
					} else if( parameter.compare( "parsing" ) == 0 ) {
						snmp::logger.system( snmp::Logger::PARSING, true );
					} else if( parameter.compare( "transport" ) == 0 ) {
						snmp::logger.system( snmp::Logger::TRANSPORT, true );
					}
				} else if( parameter.compare( "--loop" ) == 0 ) {
					if( i + 1 >= argc ) {
						usage();
					}
					parameter = argv[ ++i ];
					loop = atoi( parameter.c_str() );
					if( loop <= 0 ) {
						usage();
					}
				} else if( parameter.compare( "--loop-delay" ) == 0 ) {
					if( i + 1 >= argc ) {
						usage();
					}
					parameter = argv[ ++i ];
					loopDelay = std::chrono::seconds( atoi( parameter.c_str() ) );
				}
			} else {
				// Note: this is a short option.
				
				// A single "-" is not a valid option.
				if( parameter.length() < 2 ) {
					usage();
				}

				switch( parameter[1] ) {
					case 'v':
						if( parameter.length() < 3 ) {
							usage();
						}
						if( parameter[2] == '1' ) {
							version = 1;
						} else if( parameter[2] == '2' ) {
							version = 2;
						} else if( parameter[2] == '3' ) {
							version = 3;
						} else {
							usage();
						}
						break;
					case 'c':
						communityString = parameter.substr( 2 );
						break;
					case 'u':
						username = parameter.substr( 2 );
						break;
					case 'a':
						if( parameter.substr( 2 ) == "SHA" ) {
							authentication = snmp::Authentication::SHA;
						} else if( parameter.substr( 2 ) == "MD5" ) {
							authentication = snmp::Authentication::MD5;
						} else {
							usage();
						}
						break;
					case 'A':
						authenticationPassword = parameter.substr( 2 );
						break;
					case 'x':
						if( parameter.substr( 2 ) == "DES" ) {
							encryption = snmp::Encryption::DES;
						} else if( parameter.substr( 2 ) == "AES" ) {
							encryption = snmp::Encryption::AES;
						} else {
							usage();
						}
						break;
					case 'X':
						encryptionPassword = parameter.substr( 2 );
						break;
					default:
						usage();
				}
			}
		} else {
			// This is just a normal parameter.  It is not a special flag
			// or anything.

			if( ! hasAddress ) {
				address = parameter;
				hasAddress = true;
			} else if( ! hasCommand ) {
				command = parameter;
				hasCommand = true;
			} else {
				if( command.compare("raw-set") == 0 || command.compare("set") == 0 ) {
					if( oidTypes.size() < oids.size() ) {
						oidTypes.push_back( parameter );
					} else if( oidValues.size() < oids.size() ) {
						oidValues.push_back( parameter );
					} else {
						try {
							snmp::NumericOid oid( parameter );
							oids.push_back( oid );
						} catch( snmp::Exception& e ) {
							std::cout << "Could not determine OID from '" << parameter << "': " << e.what() << std::endl;
							exit( EXIT_FAILURE );
						}
					}
				} else {
					if( repetitions == 0 ) {
						try {
							snmp::NumericOid oid( parameter );
							oids.push_back( oid );
						} catch( snmp::Exception& e ) {
							std::cout << "Could not determine OID from '" << parameter << "': " << e.what() << std::endl;
							exit( EXIT_FAILURE );
						}
					} else {
						try {
							snmp::NumericOid oid( parameter );
							multipleRequestOids.push_back( oid );
						} catch( snmp::Exception& e ) {
							std::cout << "Could not determine OID from '" << parameter << "': " << e.what() << std::endl;
							exit( EXIT_FAILURE );
						}
					}
				}
			}
		}
	}

	if( ! hasAddress ) {
		std::cout << "No address specified." << std::endl;
		usage();
	}
	if( ! hasCommand ) {
		std::cout << "No command specified." << std::endl;
		usage();
	}
	if( command.compare("get") == 0 ) {
		// No specific checks.
	} else if( command.compare("getnext") == 0 ) {
		// No specific checks.
	} else if( command.compare("getbulk") == 0 ) {
		// No specific checks.
	} else if( command.compare("walk") == 0 ) {
		// No specific checks.
	} else if( command.compare("raw-get") == 0 ) {
		// No specific checks.
	} else if( command.compare("raw-getnext") == 0 ) {
		// No specific checks.
	} else if( command.compare("raw-getbulk") == 0 ) {
		// No specific checks.
	} else if( command.compare("raw-set") == 0 || command.compare("set") == 0 ) {
		if( oids.size() != oidTypes.size() || oids.size() != oidValues.size() ) {
			usage();
		}
	} else {
		std::cout <<
			"Unknown command '" << command << "'.\n"
			"Available commands are:\n"
			"   get\n"
			"   getnext\n"
			"   getbulk\n"
			"   walk\n"
			"   set\n"
			"   raw-get\n"
			"   raw-getnext\n"
			"   raw-getbulk\n"
			"   raw-set\n"
			"";
		usage();
	}


	// TODO: Handle this based on the command.
	if( oids.size() < 1 && multipleRequestOids.size() < 1 ) {
		std::cout << "No OID specified." << std::endl;
		usage();
	}

	//! This is the list of VarBinds.
	//! (Currently only used for "set" commands.)
	snmp::VarBindList varbinds;

	if( command.compare("raw-set") == 0 || command.compare("set") == 0 ) {
		auto oidIterator = oids.begin();
		auto typeIterator = oidTypes.begin();
		auto valueIterator = oidValues.begin();

		for( ; oidIterator != oids.end(); oidIterator++, typeIterator++, valueIterator++ ) {
			std::unique_ptr<snmp::VarBind> varbind( new snmp::VarBind() );
			
			try {
				snmp::NumericOid oid( *oidIterator );
				varbind->oid = oid;
			} catch( snmp::Exception& e ) {
				std::cout << "Could not translate OID: " << oidIterator->str() << std::endl;
				exit( EXIT_FAILURE );
			}

			if( (*typeIterator).length() != 1 ) {
				std::cout << "Invalid <type> specified." << std::endl;
				exit( EXIT_FAILURE );
			}
			switch( (*typeIterator)[0] ) {
				case 'n': //!< null (NULL)
					varbind->value.set_null( snmp::Variant::SIMPLE_NULL );
					break;
				case 'i': //!< 32-bit integer (INTEGER)
					try {
						varbind->value.set_int32_t( snmp::Variant::SIMPLE_INTEGER, std::stol( *valueIterator ) );
					} catch( std::exception& e ) {
						std::cout << "Could not convert value to integer: " << *valueIterator << std::endl;
						exit( EXIT_FAILURE );
					}
					break;
				case 'u': //!< 32-bit unsigned integer (Gauge32 or Integer32)
					try {
						varbind->value.set_uint32_t( snmp::Variant::APPLICATION_GAUGE32, std::stoul( *valueIterator ) );
					} catch( std::exception& e ) {
						std::cout << "Could not convert value to integer: " << *valueIterator << std::endl;
						exit( EXIT_FAILURE );
					}
					break;
				case 'c': //!< 32-bit counter (Counter32)
					try {
						varbind->value.set_uint32_t( snmp::Variant::APPLICATION_COUNTER32, std::stoul( *valueIterator ) );
					} catch( std::exception& e ) {
						std::cout << "Could not convert value to integer: " << *valueIterator << std::endl;
						exit( EXIT_FAILURE );
					}
					break;
				case 'C': //!< 64-bit counter (Counter64)
					try {
						varbind->value.set_uint64_t( snmp::Variant::APPLICATION_COUNTER64, std::stoull( *valueIterator ) );
					} catch( std::exception& e ) {
						std::cout << "Could not convert value to integer: " << *valueIterator << std::endl;
						exit( EXIT_FAILURE );
					}
					break;
				case 't': //!< time ticks (TimeTicks)
					try {
						varbind->value.set_uint32_t( snmp::Variant::APPLICATION_TIMETICKS, std::stoul( *valueIterator ) );
					} catch( std::exception& e ) {
						std::cout << "Could not convert value to integer: " << *valueIterator << std::endl;
						exit( EXIT_FAILURE );
					}
					break;
				case 'o': //!< OID (OBJECT IDENTIFIER)
					try {
						snmp::NumericOid oid( *valueIterator );
						varbind->value.set_oid( snmp::Variant::SIMPLE_OID, oid );
					} catch( snmp::Exception& e ) {
						std::cout << "Could not translate OID: " << *valueIterator << std::endl;
						exit( EXIT_FAILURE );
					}
					break;
				case 's': //!< byte string (OCTET STRING)
					varbind->value.set_string( snmp::Variant::SIMPLE_STRING, *valueIterator );
					break;
				case '4': { //!< IPv4 address (IpAddress)
					int part0 = 0;
					int part1 = 0;
					int part2 = 0;
					int part3 = 0;
					
					int parts = sscanf( (*valueIterator).c_str(), "%d.%d.%d.%d", &part0, &part1, &part2, &part3 );
					if( parts != 4 ) {
						std::cout << "Invalid IPv4 address: " << *valueIterator << std::endl;
						exit( EXIT_FAILURE );
					}
					
					std::string addressString( 4, '\0' );
					addressString[ 0 ] = (char)( part0 & 0xFF );
					addressString[ 1 ] = (char)( part1 & 0xFF );
					addressString[ 2 ] = (char)( part2 & 0xFF );
					addressString[ 3 ] = (char)( part3 & 0xFF );

					varbind->value.set_string( snmp::Variant::APPLICATION_IPADDRESS, addressString );
					break;
				}
				case 'q': //!< opaque (OPAQUE)
					varbind->value.set_string( snmp::Variant::APPLICATION_OPAQUE, *valueIterator );
					break;
			}
		
			varbinds.push_back( std::move( varbind ) );
		}
	}

	//snmp::logger.system( snmp::Logger::BYTESTREAM, true );


	//! This is the Dangerous SNMP context that we'll be using for our client.
	snmp::Context context;
	//! This is the Dangerous SNMP client.
	snmp::Client client( context );

	// Set the timeout for the client.
	if( hasTimeout ) {
		client.timeout( timeout );
	}

	// Set up the client.
	try {
		client.uri( address );
		switch( version ) {
			case 1:
				client.useVersion1( communityString );
				break;
			case 2:
				client.useVersion2c( communityString );
				break;
			case 3:
				client.useVersion3( snmp::securitymodel::USM( username, authentication, authenticationPassword, encryption, encryptionPassword ) );
				break;
		}
	} catch( snmp::Exception& e ) {
		cerr << "Could not set up client: " << e.what() << endl;
		return 1;
	}

	try {
		if( ! client.isAuthenticated() ) {
			client.authenticate();
		}

		// We allow a "loop" option here so that the same command can
		// be run over and over (with a delay).
		for( int i = 0; i < loop; i++ ) {
			// There are two kinds of commands: "raw" commands and logical commands.
			// This distinction is made because each type has a different result
			// structure.
			//
			// Raw commands return a "RawResponse", while logical commands return
			// a "Response".

			//! This is the prefix that we'll be using to determine which kind
			//! of command this is.
			std::string rawHeader = "raw-";

			if( std::equal( rawHeader.begin(), rawHeader.end(), command.begin() ) ) {
				//! This is the PDU that we'll be sending.
				snmp::PDU requestPdu;
				//! This is the response.
				std::unique_ptr<snmp::RawResponse> response;

				if( command.compare("raw-get") == 0 ) {
					// Add the OIDs that we want to send.
					for( const auto& oidString : oids ) {
						snmp::NumericOid oid( oidString );
						requestPdu.addOid( oid );
					}

					response = client.raw_get( requestPdu );
				} else if( command.compare("raw-getnext") == 0 ) {
					// Add the OIDs that we want to send.
					for( const auto& oidString : oids ) {
						snmp::NumericOid oid( oidString );
						requestPdu.addOid( oid );
					}
					
					response = client.raw_getnext( requestPdu );
				} else if( command.compare("raw-getbulk") == 0 ) {
					// Add the OIDs that we want to send.
					for( const auto& oidString : oids ) {
						snmp::NumericOid oid( oidString );
						requestPdu.addOid( oid );
					}
					// In a GetBulkRequest-PDU, the "errorStatus" is actually the "non-repeaters" value.
					requestPdu.errorStatus = oids.size();

					if( multipleRequestOids.size() > 0 ) {
						for( const auto& oidString : multipleRequestOids ) {
							snmp::NumericOid oid( oidString );
							requestPdu.addOid( oid );
						}
						requestPdu.errorIndex = repetitions;
					}
		
					response = client.raw_getbulk( requestPdu );
				} else if( command.compare("raw-set") == 0 ) {
					// Add the OIDs that we want to send.
					for( auto& varbind : varbinds ) {
						requestPdu.varbinds.push_back( std::move( varbind ) );
					}
					
					response = client.raw_set( requestPdu );
				}
				if( ! response ) {
					cerr << "Got back a null response." << endl;
				} else {
					cout << "Response (type=" << ((int)response->pduType) << "):" << endl;
					snmp::printPdu( std::cout, response->pdu );
				}
			} else {
				std::unique_ptr<snmp::Response> response;
				if( command.compare("get") == 0 ) {
					response = client.get( oids );
				} else if( command.compare("getnext") == 0 ) {
					response = client.getNext( oids );
				} else if( command.compare("getbulk") == 0 ) {
					response = client.getBulk( oids, repetitions, multipleRequestOids );
				} else if( command.compare("walk") == 0 ) {
					response = client.walk( oids.front() );
				} else if( command.compare("set") == 0 ) {
					response = client.set( varbinds );
				}

				if( ! response ) {
					cerr << "Got back a null response." << endl;
				} else {
					cout << "Response (operations=" << response->details.operations << "):" << endl;
					snmp::printPdu( std::cout, response->pdu );
				}
			}

			// If this isn't the last loop iteration, then sleep for the
			// required amount of time before continuing.
			if( i+1 < loop ) {
				std::this_thread::sleep_for( loopDelay );
			}
		}
	} catch( snmp::Exception& e ) {
		cerr << "Could not get a response: " << e.what() << endl;
		return 1;
	}

	return 0;
}

