#include <iostream>
#include <thread>

#include <fstream>
#include <cstring>
#include <sys/types.h>
#include <dirent.h>

#include <dangerous/snmp/agentx.hpp>
#include <dangerous/snmp/logger.hpp>
using namespace dangerous;

int main( int argc, char** argv ) {
	snmp::logger.system( snmp::Logger::AGENTX, true );
	for( int i = 0; i < snmp::Logger::MAX; i++ ) {
//		snmp::logger.system( (snmp::Logger::System)i, true );
	}

	snmp::Context context;
	snmp::AgentX agentx( context );

	agentx.description( "Test Agent" );

	agentx.uri( "tcp://localhost:705" );

	agentx.addAgentCapabilities( ".1.3.6.1.4.1.41326.1.1000", "Dangerous SNMP test / 1000" );
	agentx.addAgentCapabilities( ".1.3.6.1.4.1.41326.1.2000", "Dangerous SNMP test / 2000" );

	snmp::Variant value1;
	snmp::AgentX::OidRegistrationEntry oidRegistrationEntry1;
	oidRegistrationEntry1.getFunction = [ &value1 ]( const snmp::NumericOid& oid, snmp::Variant& value ) -> bool {
		value = value1;
		return true;
	};

	value1.set_int32_t( snmp::Variant::SIMPLE_INTEGER, 9001 );
	agentx.registerOid( "1.3.6.1.4.1.41326.2.1", oidRegistrationEntry1 );
	std::cout << "AgentX: ID #1: registered." << std::endl;


	snmp::Variant value2;
	snmp::AgentX::OidRegistrationEntry oidRegistrationEntry2;
	oidRegistrationEntry2.getFunction = [ &value2 ]( const snmp::NumericOid& oid, snmp::Variant& value ) -> bool {
		value = value2;
		return true;
	};

	value2.set_int32_t( snmp::Variant::SIMPLE_INTEGER, 42 );
	agentx.registerOid( "1.3.6.1.4.1.41326.2.2", oidRegistrationEntry2 );
	std::cout << "AgentX: ID #2: registered." << std::endl;


	snmp::Variant value3;
	snmp::AgentX::OidRegistrationEntry oidRegistrationEntry3;
	oidRegistrationEntry3.getFunction = [ &value3 ]( const snmp::NumericOid& oid, snmp::Variant& value ) -> bool {
		value = value3;
		return true;
	};

	value3.set_string( snmp::Variant::SIMPLE_STRING, "hello!" );
	agentx.registerOid( "1.3.6.1.4.1.41326.2.3", oidRegistrationEntry3 );
	std::cout << "AgentX: ID #3: registered." << std::endl;


	std::this_thread::sleep_for( std::chrono::seconds( 20 ) );

	return 0;
}

