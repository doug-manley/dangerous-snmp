#pragma once

#include "dangerous/snmp/exception.hpp"

#include "basicsockettransport.hpp"


namespace dangerous { namespace snmp { namespace transport {

/**
 * This is the (typical) UDP transport.
 **/
class Udp : public BasicSocketTransport {
public:
	const unsigned int MINIMUM_MESSAGE_SIZE = 484; //< See http://tools.ietf.org/html/rfc3417, section 3.2.
	const unsigned int RECOMMENDED_MESSAGE_SIZE = 1472; //< See http://tools.ietf.org/html/rfc3417, section 3.2.

public:
	/**
	 * This instantiates a new UDP transport instance.  The connection string
	 * must be of the form:
	 *    <address>[:<port>]
	 * However, <address> can be an actual IPv4 or IPv6 address, and it can also
	 * be a domain name.  <port> can be an actual port number or text that will
	 * resolve to a port number.  If omitted, the port will be 161.
	 * @param connectionString The string to use to establish a connection.
	 **/
	Udp( std::weak_ptr<Context::PrivateData> context, const std::string& connectionString ) throw( Exception );

public:
	virtual int socketOptions() const;
	virtual int protocolNumber() const;
};

} } }

