#pragma once

#include "berstream.hpp"
#include "bytestream.hpp"

#include <string>
#include <vector>

#include <cstring>

namespace dangerous { namespace snmp {

/**
 * This class is a "helper" class that takes some kind of thing and returns
 * a standard string that represents its BER encoding.
 *
 * Since we do this by setting up a ByteStream and doing the work there,
 * it ends up being a hassle to actually do this over and over again.
 **/
class Encoder {
public:
	/**
	 * This BER-encodes a value and returns a standard string of the result.
	 * @template The encoder class.
	 * @param value The value to encode.
	 * @return A string with the encoded data.
	 **/
	template <typename EncodingClass>
	static std::string encodeToString( typename EncodingClass::value_type& value ) {
		//! This byte stream will be used to _write_ (that is, "encode") the
		//! value that we have been given.
		ByteStream byteStream;
		BerStream berStream( &byteStream );

		// Encode the value.
		bool success = berStream.write<EncodingClass>( value );
		if( ! success ) {
			throw Exception( std::string("Could not encode ") + EncodingClass::NAME );
		}

		//! This is the string that we'll be returning.
		//! We're going to copy the encoded data _into_ this string.
		std::string returnValue;
		// Copy the encoded data from the byte stream to the string.
		byteStream.copyTo( returnValue );

		return returnValue;
	}

	/**
	 * This BER-encodes a value and returns a standard vector of the result.
	 * @template The encoder class.
	 * @param value The value to encode.
	 * @return A vector with the encoded data.
	 **/
	template <typename EncodingClass>
	static std::vector<char> encodeToVector( typename EncodingClass::value_type& value ) {
		//! This byte stream will be used to _write_ (that is, "encode") the
		//! value that we have been given.
		ByteStream byteStream;
		BerStream berStream( &byteStream );

		// Encode the value.
		bool success = berStream.write<EncodingClass>( value );
		if( ! success ) {
			throw Exception( std::string("Could not encode ") + EncodingClass::NAME );
		}

		//! This is the string that we'll be returning.
		//! We're going to copy the encoded data _into_ this string.
		std::vector<char> returnValue;
		// Copy the encoded data from the byte stream to the vector.
		byteStream.copyTo( returnValue );

		return returnValue;
	}
};

} }
