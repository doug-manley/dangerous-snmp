#pragma once

#include "dangerous/snmp/logger.hpp"

#include "bytestream.hpp"

#include <vector>

namespace dangerous { namespace snmp {

/**
 * A BerStream represents a BER-encoded stream of data.  It is
 * a wrapper around a ByteStream instance, allowing only BER
 * encoding and decoding operations.
 **/
class BerStream {
public:
	/**
	 * TODO
	 **/
	BerStream( ByteStream* byteStream );

	/**
	 * TODO
	 **/
	bool copyTo( std::vector<char>& buffer );

	/**
	 * This returns the number of bytes remaining in the stream.
	 * @return The number of bytes remaining.
	 **/
	unsigned int remainingReadLength() const { return _byteStream->remainingReadLength(); }
	
	/**
	 * This returns the number of bytes remaining in the stream.
	 * @return The number of bytes remaining.
	 **/
	unsigned int remainingWriteLength() const { return _byteStream->remainingWriteLength(); }

	/*
	 * Peek functions.
	 */

	/**
	 * This reads an ASN.1 type from the BerStream.
	 * However, this does _not_ advance the read head of the BerStream.
	 * @param type A reference to the integer to store the type.
	 * @return true on success, false on failure.
	 **/
	bool peekType( uint8_t& type );
	
	/*
	 * Read functions.
	 * All of these functions will advance the read head.
	 */

	/**
	 * This reads an ASN.1 type from the BerStream.
	 * @param type A reference to the integer to store the type.
	 * @return true on success, false on failure.
	 **/
	bool readType( uint8_t& type );

	/**
	 * This reads an ASN.1 length from the BerStream.
	 * @param length A reference to the integer to store the length.
	 * @return true on success, false on failure.
	 **/
	bool readLength( unsigned int& length );

	/**
	 * This performs a full, logical read of a kind of encoded data.
	 * @template EncodingClass The encoding class to use.
	 * @param value A reference to the value in which to store the data.
	 * @return true on success, false on failure.
	 **/
	template<typename EncodingClass>
	bool read( typename EncodingClass::value_type& value );

	/**
	 * This reads an arbitrary amount of bytes into the specified character buffer.
	 * @param buffer The character buffer in which to store the read bytes.
	 * @param bufferSize The number of bytes to read.  "buffer" must be _at least_ this size.
	 * @param bytesRead A reference to an integer to hold the number of bytes that were actually read.
	 **/
	bool readBytes( char* buffer, unsigned int bufferSize, unsigned int& bytesRead );

	/**
	 * This reads a full TLV set of data and fills out the _whole thing_ (including the
	 * type, the length, and the value) exactly as read.
	 * @param buffer The vector in which to store the data.
	 * @return true on success, false on failure.
	 **/
	bool readFullTlv( std::vector<char>& buffer );

	/*
	 * Write functions.
	 * All of these functions will advance the write head.
	 */

	/**
	 * This writes the given ASN.1 type to the buffer in BER.
	 * @param type The type to write.
	 * @return true on success, false on failure.
	 **/
	bool writeType( uint8_t type );

	/**
	 * This writes the length to the buffer in BER.
	 * @param encodedLength The length to write.
	 * @return true on success, false on failure.
	 **/
	bool writeLength( unsigned int encodedLength );

	/**
	 * This performs a full, logical write of a kind of encoded data.
	 * @template EncodingClass The encoding class to use.
	 * @param value A reference to the value in to write.
	 * @return true on success, false on failure.
	 **/
	template<typename EncodingClass>
	bool write( const typename EncodingClass::value_type& value );

	/**
	 * This writes the bytes given to the BerStream.
	 * @param buffer The bytes to write.
	 * @param bufferSize The number of bytes to write.  "buffer" must be _at least_ this size.
	 * @param bytesWritten A reference to an integer to store the number of bytes actually written.
	 * @return true on success, false on failure.
	 **/
	bool writeBytes( const char* buffer, unsigned int bufferSize, unsigned int& bytesWritten );

protected:
	//! TODO
	ByteStream* _byteStream;
};

/**
 * This performs a full, logical read of a kind of encoded data.
 * @template EncodingClass The encoding class to use.
 * @param value A reference to the value in which to store the data.
 * @return true on success, false on failure.
 **/
template<typename EncodingClass>
bool BerStream::read( typename EncodingClass::value_type& value ) {
	uint8_t type = 0;
	bool status = readType( type );
	if( ! status ) {
		if( logger.system( Logger::BYTESTREAM ) ) {
			logger.out() << "BerStream: read<" << EncodingClass::NAME << ">: Could not read the type." << std::endl;
		}
		return false;
	}

	if( logger.system( Logger::BYTESTREAM ) ) {
		logger.out() << "BerStream: read<" << EncodingClass::NAME << ">: Looking for a type of " << EncodingClass::TYPE << "." << std::endl;
	}
	if( type != EncodingClass::TYPE ) {
		return false;
	}

	unsigned int increment = 0;
	status = readLength( increment );
	if( ! status ) {
		if( logger.system( Logger::BYTESTREAM ) ) {
			logger.out() << "BerStream: read<" << EncodingClass::NAME << ">: Could not read the length." << std::endl;
		}
		return false;
	}
	if( logger.system( Logger::BYTESTREAM ) ) {
		logger.out() << "BerStream: read<" << EncodingClass::NAME << ">: Got a length of " << increment << "." << std::endl;
	}
	if( increment < 1 ) {
		return true;
	}

	if( logger.system( Logger::BYTESTREAM ) ) {
		logger.out() << "BerStream: read<" << EncodingClass::NAME << ">: Reading " << increment << " bytes." << std::endl;
	}
	unsigned int bytesRead = 0;
	char buffer[ increment ];
	status = readBytes( buffer, increment, bytesRead );
	if( ! status ) {
		if( logger.system( Logger::BYTESTREAM ) ) {
			logger.out() << "BerStream: read<" << EncodingClass::NAME << ">: Could not read the value bytes." << std::endl;
		}
		return false;
	}
	status = EncodingClass::read( value, buffer, increment );
	if( ! status ) {
		if( logger.system( Logger::BYTESTREAM ) ) {
			logger.out() << "BerStream: read<" << EncodingClass::NAME << ">: Could not parse the value bytes." << std::endl;
		}
		return false;
	}

	return true;
}

/**
 * This performs a full, logical write of a kind of encoded data.
 * @template EncodingClass The encoding class to use.
 * @param value A reference to the value in to write.
 * @return true on success, false on failure.
 **/
template<typename EncodingClass>
bool BerStream::write( const typename EncodingClass::value_type& value ) {
	if( logger.system( Logger::BYTESTREAM ) ) {
		logger.out() << "BerStream: write<" << EncodingClass::NAME << ">: Writing a type of " << EncodingClass::TYPE << "." << std::endl;
	}
	bool status = writeType( EncodingClass::TYPE );
	if( ! status ) {
		if( logger.system( Logger::BYTESTREAM ) ) {
			logger.out() << "BerStream: write<" << EncodingClass::NAME << ">: Could not write the type." << std::endl;
		}
		return false;
	}

	unsigned int encodedLength = EncodingClass::length( value );
	if( logger.system( Logger::BYTESTREAM ) ) {
		logger.out() << "BerStream: write<" << EncodingClass::NAME << ">: Got a length of " << encodedLength << "." << std::endl;
	}
	status = writeLength( encodedLength );
	if( ! status ) {
		if( logger.system( Logger::BYTESTREAM ) ) {
			logger.out() << "BerStream: write<" << EncodingClass::NAME << ">: Could not write the length." << std::endl;
		}
		return false;
	}

	if( logger.system( Logger::BYTESTREAM ) ) {
		logger.out() << "BerStream: write<" << EncodingClass::NAME << ">: Writing " << encodedLength << " bytes." << std::endl;
	}
	
	char buffer[ encodedLength ];
	status = EncodingClass::write( value, buffer, encodedLength );
	if( ! status ) {
		if( logger.system( Logger::BYTESTREAM ) ) {
			logger.out() << "BerStream: write<" << EncodingClass::NAME << ">: Could not encode the value." << std::endl;
		}
		return false;
	}
	
	unsigned int bytesWritten = 0;
	status = writeBytes( buffer, encodedLength, bytesWritten );
	if( ! status ) {
		if( logger.system( Logger::BYTESTREAM ) ) {
			logger.out() << "BerStream: write<" << EncodingClass::NAME << ">: Could not write the value." << std::endl;
		}
		return false;
	}
	
	return true;
}

} }

