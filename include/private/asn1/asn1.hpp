#pragma once

#include "dangerous/snmp/types.hpp"
#include "dangerous/snmp/logger.hpp"

#include <type_traits>

#include <iostream>

namespace dangerous { namespace snmp { namespace asn1 {

namespace helper {

class length {
public:
	static unsigned int byteSize( unsigned int length ) {
		unsigned int size = 1;
		// Anything less than 128 (that is, 0-127) can fit in a single byte.
		// Thus, their size will be "1".
		// However, anything 128 or larger MUST be encoded in multiple bytes.
		// The FIRST byte is "1xxxxxxx", where "xxxxxxx" is the number of bytes
		// to read.
		if( length >= 128 ) {
			// So, in addition to the one byte that we already have (for the count of bytes),
			// we're also going to need a byte for storing the actual size.
			size++;
			if( length > 0xFFFFFFFF ) {
				size += 4;
			} else if( length > 0xFFFFFF ) {
				size += 3;
			} else if( length > 0xFFFF ) {
				size += 2;
			} else if( length > 0xFF ) {
				size += 1;
			}
		}
		return size;
	}

	static unsigned int bytes( unsigned int length, char* buffer, unsigned int bufferLength ) {
		unsigned int size = byteSize( length );
		if( logger.system( Logger::ENCODING ) ) {
			logger.out() << "length::bytes: length=" << length << ": size=" << size << std::endl;
		}
		if( bufferLength < size ) {
			return 0;
		}

		if( length < 128 ) {
			*buffer = length & 0xFF;
			if( logger.system( Logger::ENCODING ) ) {
				logger.out() << "length::bytes: length=" << length << ": A -> " << (int)( *buffer ) << std::endl;
			}
			buffer++;
		} else {
			*buffer = 0x80 | ( size - 1 );
			if( logger.system( Logger::ENCODING ) ) {
				logger.out() << "length::bytes: length=" << length << ": B -> " << (int)( *buffer ) << std::endl;
			}
			buffer++;

			for( unsigned int i = 0; i < size - 1; i++ ) {
				*buffer = ( length >> ( ( (size-1) - 1 - i ) * 8 ) ) & 0xFF;
				if( logger.system( Logger::ENCODING ) ) {
					logger.out() << "length::bytes: length=" << length << ": C -> " << (int)( *buffer ) << std::endl;
				}
				buffer++;
			}
		}

		return size;
	}
};

class SEQUENCE {
public:
	static const int TYPE = 0x30;

	static unsigned int byteSize( unsigned int contentLength ) {
		unsigned int size = 1; //< For the type, which is 0x30.
		size += helper::length::byteSize( contentLength );
		return size;
	}

	static unsigned int bytes( unsigned int contentLength, char* buffer, unsigned int bufferSize ) {
		unsigned int size = byteSize( contentLength );
		if( bufferSize < size ) {
			return 0;
		}

		*buffer = (char)TYPE;
		buffer++;
		bufferSize--;

		unsigned int increment = helper::length::bytes( contentLength, buffer, bufferSize );
		buffer += increment;
		bufferSize -= increment;

		return size;
	}
};

}

class Class {
public:
	static const int UNIVERSAL = 0b00;
	static const int APPLICATION = 0b01;
	static const int CONTEXT_SPECIFIC = 0b10;
	static const int PRIVATE = 0b11;
};

class Content {
public:
	static const int PRIMITIVE = 0b0;
	static const int CONSTRUCTED = 0b1;
};








template<typename T>
unsigned int encodedSize( const typename T::value_type& value ) {
	unsigned int length = T::length( value );
	
	unsigned int size = 0;
	size += 1;
	size += helper::length::byteSize( length );
	size += length;

	return size;
}








namespace type {

template< int classBits, int contentBit, int tag, typename myType >
class INTEGER {
public:
	static const int TYPE = ( classBits << 6 ) | ( contentBit << 5 ) | ( tag & 0b11111 );
	typedef myType value_type;

	static unsigned int length( const value_type& value ) {
		// Start at the front (most significant part of) the integer
		// and work backward (toward the least-significant part).
		// 
		// We'll be throwing out all-zero bytes if the the number is
		// positive, and we'll be throwing out all-one bytes if it's
		// negative.
		//
		// We'll stop as soon as we hit a significant value, since that
		// will tell us the minimum number of bytes required to encode
		// it.

		//! This is whether or not the current value is negative.
		//! The current value is negative when:
		//!    1. The type allows negative values; and
		//!    2. The first bit of the value is "1".
		bool isNegative = std::is_signed<myType>::value && ( ( value >> ( ( sizeof(myType) - 1 ) * 8 ) ) & 0x80 ) == 0x80;
		if( logger.system( Logger::ENCODING ) ) {
			logger.out() << "Integer " << value << " is " << ( isNegative ? "negative" : "positive" ) << std::endl;
		}

		//! This is how many bytes we need to encode the integer.
		//! We are going to start off by assuming 
		unsigned int size = sizeof(myType);
		for( unsigned int i = 0; i < sizeof(myType); i++ ) {
			//! This is the current byte that we're looking at.
			uint8_t byte = ( value >> ( ( sizeof(myType) - 1 - i ) * 8 ) ) & 0xFF;
			if( logger.system( Logger::ENCODING ) ) {
				logger.out() << "Integer " << value << ", byte [ " << i << " ] has a value of " << std::hex << (int)byte << std::dec << "." << std::endl;
			}
			
			if( ( ! isNegative && byte == 0x00 ) || ( isNegative && byte == 0xFF ) ) {
				size--;
				continue;
			}

			// We have hit a significant byte.  We're done.

			// Positive values only:
			// But, before we stop, we need to see if our significant
			// byte begins with a "1".  If it does, then we'll need to add
			// another byte on at the beginning to demonstrate that this
			// is not a negative number.
			if( ! isNegative && ( byte & 0x80 ) == 0x80 ) {
				size++;
			}
			break;
		}
		if( size == 0 ) {
			size = 1;
		}
		if( logger.system( Logger::ENCODING ) ) {
			logger.out() << "Integer " << value << " has an encoded length of " << size << "." << std::endl;
		}

		return size;
	}

	static bool write( const value_type& value, char* buffer, unsigned int bufferSize ) {
		if( logger.system( Logger::ENCODING ) ) {
			logger.out() << "INTEGER<...>::write: Value is " << value << "." << std::endl;
		}
		//! This is whether or not the current value is negative.
		//! The current value is negative when:
		//!    1. The type allows negative values; and
		//!    2. The first bit of the value is "1".
		bool isNegative = std::is_signed<myType>::value && ( ( value >> ( ( sizeof(myType) - 1 ) * 8 ) ) & 0x80 ) == 0x80;
		if( logger.system( Logger::ENCODING ) ) {
			logger.out() << "INTEGER<...>::write: Sign is '" << ( isNegative ? "-" : "+" ) << "'." << std::endl;
		}

		for( unsigned int i = 0; i < sizeof(myType); i++ ) {
			//! This is the current byte that we're looking at.
			uint8_t byte = ( value >> ( ( sizeof(myType) - 1 - i ) * 8 ) ) & 0xFF;
			
			if( ( ! isNegative && byte == 0x00 ) || ( isNegative && byte == 0xFF ) ) {
				// We would ordinarily skip is byte, but this is the LAST
				// byte that's going to happen, so we have to write it.
				if( i + 1 == sizeof(myType) ) {
					if( logger.system( Logger::ENCODING ) ) {
						logger.out() << "INTEGER<...>::write: Adding last [trivial] byte " << ((int)byte) << "." << std::endl;
					}
					*buffer = (char)byte;
					buffer++;
					bufferSize--;
					break;
				}
				continue;
			}

			// We have hit a significant byte.  It's time to actually start
			// encoding this integer..

			// Positive values only:
			// But, before we start, we need to see if our significant
			// byte begins with a "1".  If it does, then we'll need to add
			// another byte on at the beginning to demonstrate that this
			// is not a negative number.
			if( ! isNegative && ( byte & 0x80 ) == 0x80 ) {
				if( logger.system( Logger::ENCODING ) ) {
					logger.out() << "INTEGER<...>::write: Adding first byte 0x00." << std::endl;
				}
				*buffer = 0x00;
				buffer++;
				bufferSize--;
			}

			for( ; i < sizeof(myType); i++ ) {
				//! This is the current byte that we're looking at.
				uint8_t byte = ( value >> ( ( sizeof(myType) - 1 - i ) * 8 ) ) & 0xFF;
				if( logger.system( Logger::ENCODING ) ) {
					logger.out() << "INTEGER<...>::write: Adding byte " << ((int)byte) << "." << std::endl;
				}
	
				*buffer = (char)byte;
				buffer++;
				bufferSize--;
			}

			break;
		}

		return true;
	}

	static value_type read( value_type& value, const char* buffer, unsigned int bufferSize ) {
		value = 0;
		
		bool isNegative = false;	
		for( unsigned int i = 0; i < bufferSize; i++ ) {
			uint8_t byte = buffer[ i ];
			if( logger.system( Logger::DECODING ) ) {
				logger.out() << "INTEGER: read: byte[ " << i << " ]: " << (int)byte << std::endl;
			}

			if( i == 0 ) {
				isNegative = std::is_signed<myType>::value && ( byte & 0x80 ) == 0x80;
				// If this value is negative, we're going to strip the leading bit from it.
				// We'll use it at the end.
				if( isNegative ) {
					byte &= 0x7f;
				}
			}

			value <<= 8;
			value |= ( byte & 0xFF );
		}

		if( value != 0 && isNegative ) {
			value -= 0x80 << ( ( bufferSize - 1 ) * 8 );
		}

		return true;
	}
};

template< int classBits, int contentBit, int tag >
class OCTET_STRING {
public:
	static const int TYPE = ( classBits << 6 ) | ( contentBit << 5 ) | ( tag & 0b11111 );
	typedef std::string value_type;

	static unsigned int length( const value_type& text ) {
		return text.length();
	}

	static bool write( const value_type& text, char* buffer, unsigned int bufferSize ) {
		for( unsigned int i = 0, iSize = text.length(); i < iSize; i++ ) {
			*buffer = text[i];
			buffer++;
			bufferSize--;
		}

		return true;
	}

	static bool read( value_type& value, const char* buffer, unsigned int bufferSize ) {
		if( logger.system( Logger::DECODING ) ) {
			logger.out() << "Clearing value." << std::endl;
		}
		value.clear();
		if( logger.system( Logger::DECODING ) ) {
			logger.out() << "Resizing string to length=" << bufferSize << "." << std::endl;
		}
		value.resize( bufferSize );
		
		for( unsigned int i = 0; i < bufferSize; i++ ) {
			uint8_t byte = buffer[ i ];
			if( logger.system( Logger::DECODING ) ) {
				logger.out() << "OCTET_STRING: read: byte[ " << i << " ]: " << (int)byte << std::endl;
			}

			value[ i ] = byte;
		}

		return true;
	}
};

/**
 * This class allows for the encoding and decoding of null values.
 **/
template< int classBits, int contentBit, int tag >
class NULL_TYPE {
public:
	static const int TYPE = ( classBits << 6 ) | ( contentBit << 5 ) | ( tag & 0b11111 );

	typedef uint8_t value_type; //< This is actually bogus, but it must fit our model.

	static uint8_t throwaway; //< You can pass this as the valeu to all of the NULL_TYPE functions.

	static unsigned int length( const value_type& value ) {
		return 0;
	}

	static bool write( const value_type& value, char* buffer, unsigned int bufferSize ) {
		return true;
	}

	/**
	 * In terms of reading an ASN.1 NULL value goes, by the time that we've gotten to this
	 * function, we've already handled the type and length parts, so there is literally
	 * nothing to do.
	 **/
	static bool read( value_type& value, const char* buffer, unsigned int bufferSize ) {
		return true;
	}
};

template< int classBits, int contentBit, int tag >
uint8_t NULL_TYPE<classBits,contentBit,tag>::throwaway = 0;

template< int classBits, int contentBit, int tag >
class OBJECT_IDENTIFIER {
public:
	static const int TYPE = ( classBits << 6 ) | ( contentBit << 5 ) | ( tag & 0b11111 );

	//! This class operates on a NumericType.
	typedef NumericOid value_type;

	static unsigned int length( const value_type& oid ) {
		unsigned int size = 0;

		if( oid.numbers.size() >= 1 ) {
			size += 1;
		}
		for( unsigned int i = 2, iSize = oid.numbers.size(); i < iSize; i++ ) {
			unsigned int number = oid.numbers[ i ];
			size += 1;
			number >>= 7;
			while( number > 0 ) {
				size += 1;
				number >>= 7;
			}
		}
		
		return size;
	}

	static bool write( const value_type& oid, char* buffer, unsigned int bufferSize ) {
		// If this OID, somehow, has zero length, then we truly cannot actually _write_
		// anything.  In this case, we will simply return with success.
		if( oid.numbers.size() == 0 ) {
			return true; //< TODO: Return false?  Is this an error condition?
		}
		
		// An OID MUST have at least two numbers in it in order to be encoded.
		if( oid.numbers.size() < 2 ) {
			return false;
		}

		// The BER for an OID include a special provision for the first two numbers in
		// the OID.  They are to be encoded in a _single byte_.
		//    Byte #1 := ( 40 * number1 ) + ( number2 )
		//
		// To accomplish this, we will be first extracting those two numbers, handling
		// them, and then moving on to the rest of the OID proper.

		//! This is the first number in the OID.
		unsigned int number1 = oid.numbers[ 0 ];
		//! This is the second number in the OID.
		unsigned int number2 = oid.numbers[ 1 ];

		if( number1 >= 40 ) {
			if( logger.system( Logger::ENCODING ) ) {
				logger.out() << "OID component #1 (" << number1 << ") is too large (must be between 0 and 39)." << std::endl;
			}
			return false;
		}
		if( number2 >= 40 ) {
			if( logger.system( Logger::ENCODING ) ) {
				logger.out() << "OID component #2 (" << number2 << ") is too large (must be between 0 and 39)." << std::endl;
			}
			return false;
		}

		//DEBUG:std::cout << "OBJECT_IDENTIFIER::write: buffer is " << (void*)buffer << "; bufferSize is " << bufferSize << std::endl;
		*buffer = 40 * number1 + number2;

		buffer += 1;
		bufferSize -= 1;

		for( unsigned int i = 2, iSize = oid.numbers.size(); i < iSize; i++ ) {
			unsigned int number = oid.numbers[ i ];
			
			int length = 1;
			number >>= 7;
			while( number > 0 ) {
				length += 1;
				number >>= 7;
			}
			
			number = oid.numbers[ i ];

			// We're going to start from the right-hand side of the number and
			// move to the left, 7-bits at a time.  For the farthest-right encoded
			// byte, the first bit _must_ be 0, which means that there are _no more_
			// bytes.
			//
			// As we move left (that is, "l" is greater than zero), the first bit
			// _must_ be 1 in order to reflect the fact there are more encoded bytes
			// to the right.
			for( int l = 0; l < length; l++ ) {
				// The current byte is equal to:
				//    {0,1} followed by the last 7-bits of the current number.
				//    * "0" if this is the farthest-right bit.
				//    * "1" otherwise.
				buffer[ length - 1 - l ] = ( ( l > 0 ) ? 0x80 : 0x00 ) | ( number & 0x7F );
				number >>= 7;
			}


			buffer += length;
			bufferSize -= length;
		}

		return true;
	}

	static bool read( value_type& oid, const char* buffer, unsigned int bufferSize ) {
		if( bufferSize < 1 ) {
			return false;
		}

		// Read the first two numbers, since they're both encoded on
		// the first byte.
		unsigned int number2 = (int)(char)(*buffer) % 40;
		unsigned int number1 = ( (int)(char)(*buffer) - number2 ) / 40;
		if( logger.system( Logger::DECODING ) ) {
			logger.out() << "." << number1 << "." << number2 << std::endl;
		}
		oid.numbers.push_back( number1 );
		oid.numbers.push_back( number2 );
		
		buffer++;
		bufferSize--;

		while( bufferSize > 0 ) {
			unsigned int number = 0;
			while( *buffer & 0x80 ) {
				number <<= 7;
				number |= ( *buffer & 0x7F );

				buffer++;
				bufferSize--;
				if( bufferSize == 0 ) {
					return false;
				}
			}
			number <<= 7;
			number |= ( *buffer & 0x7F );
			oid.numbers.push_back( number );
			if( logger.system( Logger::DECODING ) ) {
				logger.out() << "   ." << number << std::endl;
			}

			buffer++;
			bufferSize--;
		}
		if( logger.system( Logger::DECODING ) ) {
			logger.out() << "done." <<  std::endl;
		}

		return true;
	}
};

}

} } }

