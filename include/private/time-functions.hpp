#pragma once

#include <chrono>

static inline void chronoToTimeval( std::chrono::milliseconds myTimeout, struct timeval& tv ) {
	tv.tv_sec = std::chrono::duration_cast< std::chrono::seconds >( myTimeout ).count();
	myTimeout -= std::chrono::seconds( tv.tv_sec );
	tv.tv_usec = std::chrono::duration_cast< std::chrono::microseconds >( myTimeout ).count();
}

