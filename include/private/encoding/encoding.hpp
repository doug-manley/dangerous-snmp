#pragma once

#include "dangerous/snmp/logger.hpp"
#include "dangerous/snmp/types.hpp"
#include "dangerous/snmp/pdu.hpp"

#include "berstream.hpp"

#include "../asn1/asn1.hpp"

#include <iostream>


namespace dangerous { namespace snmp { namespace encoding {

/*
 * ASN.1 native types.
 */

class INTEGER : public asn1::type::INTEGER< asn1::Class::UNIVERSAL, asn1::Content::PRIMITIVE, 2, int32_t > {
public:
	static constexpr const char* NAME = "INTEGER";
};

class OCTET_STRING : public asn1::type::OCTET_STRING< asn1::Class::UNIVERSAL, asn1::Content::PRIMITIVE, 4 > {
public:
	static constexpr const char* NAME = "OCTET STRING";
};

class NULL_TYPE : public asn1::type::NULL_TYPE< asn1::Class::UNIVERSAL, asn1::Content::PRIMITIVE, 5 > {
public:
	static constexpr const char* NAME = "NULL";
};

class OBJECT_IDENTIFIER : public asn1::type::OBJECT_IDENTIFIER< asn1::Class::UNIVERSAL, asn1::Content::PRIMITIVE, 6 > {
public:
	static constexpr const char* NAME = "OBJECT IDENTIFIER";
};

/*
 * SNMP "application" types.
 */

class IPADDRESS : public asn1::type::OCTET_STRING< asn1::Class::APPLICATION, asn1::Content::PRIMITIVE, 0 > {
public:
	static constexpr const char* NAME = "IpAddress";
};

class COUNTER32 : public asn1::type::INTEGER< asn1::Class::APPLICATION, asn1::Content::PRIMITIVE, 1, uint32_t > {
public:
	static constexpr const char* NAME = "Counter32";
};

class GAUGE32 : public asn1::type::INTEGER< asn1::Class::APPLICATION, asn1::Content::PRIMITIVE, 2, uint32_t > {
public:
	static constexpr const char* NAME = "Gauge32"; //< Also known as "Unsigned32".
};

class TIMETICKS : public asn1::type::INTEGER< asn1::Class::APPLICATION, asn1::Content::PRIMITIVE, 3, uint32_t > {
public:
	static constexpr const char* NAME = "TimeTicks";
};

class OPAQUE : public asn1::type::OCTET_STRING< asn1::Class::APPLICATION, asn1::Content::PRIMITIVE, 4 > {
public:
	static constexpr const char* NAME = "Opaque";
};

// Note: There is no [APPLICATION 5].

class COUNTER64 : public asn1::type::INTEGER< asn1::Class::APPLICATION, asn1::Content::PRIMITIVE, 6, uint64_t > {
public:
	static constexpr const char* NAME = "Counter64";
};

/*
 * SNMPv2 VarBind value choices (failure).
 */

class NOSUCHOBJECT : public asn1::type::NULL_TYPE< asn1::Class::CONTEXT_SPECIFIC, asn1::Content::PRIMITIVE, 0 > {
public:
	static constexpr const char* NAME = "noSuchObject";
};

class NOSUCHINSTANCE : public asn1::type::NULL_TYPE< asn1::Class::CONTEXT_SPECIFIC, asn1::Content::PRIMITIVE, 1 > {
public:
	static constexpr const char* NAME = "noSuchInstance";
};

class ENDOFMIBVIEW : public asn1::type::NULL_TYPE< asn1::Class::CONTEXT_SPECIFIC, asn1::Content::PRIMITIVE, 2 > {
public:
	static constexpr const char* NAME = "endOfMibView";
};

/**
 * This class allows for the encoding and decoding of an arbitrary value.
 **/
class VARIANT {
public:
	typedef Variant value_type;

	/**
	 * The byte size of a null value is always 2:
	 *    1. The null type.
	 *    2. A length of zero.
	 * @return The number of bytes that are needed.
	 **/
	static unsigned int length( const Variant& variant ) {
		switch( variant.type() ) {
			/*
			 * ASN.1 native types.
			 */
			case Variant::SIMPLE_INTEGER:
				return INTEGER::length( variant.get_int32_t() );
			case Variant::SIMPLE_OID:
				return OBJECT_IDENTIFIER::length( variant.get_oid() );
			case Variant::SIMPLE_STRING:
				return OCTET_STRING::length( variant.get_string() );
			case Variant::SIMPLE_NULL:
				return NULL_TYPE::length( NULL_TYPE::throwaway );

			/*
			 * SNMP "application" types.
			 */
			case Variant::APPLICATION_IPADDRESS:
				return IPADDRESS::length( variant.get_string() );
			case Variant::APPLICATION_COUNTER32:
				return COUNTER32::length( variant.get_uint32_t() );
			case Variant::APPLICATION_GAUGE32:
				return GAUGE32::length( variant.get_uint32_t() );
			case Variant::APPLICATION_TIMETICKS:
				return TIMETICKS::length( variant.get_uint32_t() );
			case Variant::APPLICATION_OPAQUE:
				return OPAQUE::length( variant.get_string() );
			case Variant::APPLICATION_COUNTER64:
				return COUNTER64::length( variant.get_uint32_t() );

			/*
			 * SNMPv2 VarBind value choices (failure).
			 */
			case Variant::CONTEXT_NOSUCHOBJECT:
				return NOSUCHOBJECT::length( NOSUCHOBJECT::throwaway );
			case Variant::CONTEXT_NOSUCHINSTANCE:
				return NOSUCHINSTANCE::length( NOSUCHINSTANCE::throwaway );
			case Variant::CONTEXT_ENDOFMIBVIEW:
				return ENDOFMIBVIEW::length( ENDOFMIBVIEW::throwaway );

			default:
				if( logger.system( Logger::ENCODING ) ) {
					logger.out() << "Warning: Unknown variant type: " << (int)variant.type() << "; using null." << std::endl;
				}
				return NULL_TYPE::length( NULL_TYPE::throwaway );
		}
	}
};

/**
 * This class allows for the encoding and decoding of VarBind pointers.
 **/
class VARBIND {
public:
	//! This is the name of this entity.
	static constexpr const char* NAME = "VarBind";

	//! A VarBind is encoded as a sequence with two children:
	//!    1. The OID.
	//!    2. The value.
	static const int TYPE = asn1::helper::SEQUENCE::TYPE;

	//! This class operates on unique VarBind pointers.
	typedef std::unique_ptr<VarBind> value_type; 

	static unsigned int length( const value_type& varbind ) {
		unsigned int size = 0;

		if( logger.system( Logger::ENCODING ) ) {
			logger.out() << "VARBIND: length: OID part: " << asn1::encodedSize<OBJECT_IDENTIFIER>( varbind->oid ) << std::endl;
			logger.out() << "VARBIND: length: VAR part: " << asn1::encodedSize<VARIANT>( varbind->value ) << std::endl;
		}

		size += asn1::encodedSize<OBJECT_IDENTIFIER>( varbind->oid );
		size += asn1::encodedSize<VARIANT>( varbind->value );

		return size;
	}

	static bool write( const value_type& varbind, char* buffer, unsigned int bufferSize ) {
		//! This is the ByteStream for processing the VarBind.
		ByteStream byteStream;
		byteStream.linkFrom( (char*)buffer, bufferSize, ByteStream::READ_WRITE ); //< TODO: Consider doing this in a "const" fashion.

		BerStream berStream( &byteStream );

		bool success = berStream.write<OBJECT_IDENTIFIER>( varbind->oid );
		if( ! success ) {
			return false;
		}

		switch( varbind->value.type() ) {
			/*
			 * ASN.1 native types.
			 */
			case Variant::SIMPLE_INTEGER:
				success = berStream.write<INTEGER>( varbind->value.get_int32_t() );
				break;
			case Variant::SIMPLE_OID:
				success = berStream.write<OBJECT_IDENTIFIER>( varbind->value.get_oid() );
				break;
			case Variant::SIMPLE_STRING:
				success = berStream.write<OCTET_STRING>( varbind->value.get_string() );
				break;
			case Variant::SIMPLE_NULL:
				success = berStream.write<NULL_TYPE>( NULL_TYPE::throwaway );
				break;

			/*
			 * SNMP "application" types.
			 */
			case Variant::APPLICATION_IPADDRESS:
				success = berStream.write<IPADDRESS>( varbind->value.get_string() );
				break;
			case Variant::APPLICATION_COUNTER32:
				success = berStream.write<COUNTER32>( varbind->value.get_uint32_t() );
				break;
			case Variant::APPLICATION_GAUGE32:
				success = berStream.write<GAUGE32>( varbind->value.get_uint32_t() );
				break;
			case Variant::APPLICATION_TIMETICKS:
				success = berStream.write<TIMETICKS>( varbind->value.get_uint32_t() );
				break;
			case Variant::APPLICATION_OPAQUE:
				success = berStream.write<OPAQUE>( varbind->value.get_string() );
				break;
			case Variant::APPLICATION_COUNTER64:
				success = berStream.write<COUNTER64>( varbind->value.get_uint64_t() );
				break;

			/*
			 * SNMPv2 VarBind value choices (failure).
			 */
			case Variant::CONTEXT_NOSUCHOBJECT:
				success = berStream.write<NOSUCHOBJECT>( NOSUCHOBJECT::throwaway );
				break;
			case Variant::CONTEXT_NOSUCHINSTANCE:
				success = berStream.write<NOSUCHINSTANCE>( NOSUCHINSTANCE::throwaway );
				break;
			case Variant::CONTEXT_ENDOFMIBVIEW:
				success = berStream.write<ENDOFMIBVIEW>( ENDOFMIBVIEW::throwaway );
				break;

			default:
				if( logger.system( Logger::ENCODING ) ) {
					logger.out() << "Warning: Unknown varbind->value type: " << (int)varbind->value.type() << "; using null." << std::endl;
				}
				success = berStream.write<NULL_TYPE>( NULL_TYPE::throwaway );
				break;
		}
		if( ! success ) {
			return false;
		}

		if( berStream.remainingWriteLength() > 0 ) {
			if( logger.system( Logger::ENCODING ) ) {
				logger.out() << "VARBIND: write: Somehow, there are " << berStream.remainingWriteLength() << " bytes left over." << std::endl;
				logger.out() << "VARBIND: write:    Original length: " << bufferSize << std::endl;
			}
			return false;
		}

		return true;
	}
	
	static bool read( value_type& varbind, char* buffer, unsigned int bufferSize ) {
		// Since a "VarBind" is composed of two entries, we're going to
		// set up a ByteStream to handle it.
		
		//! This is the ByteStream for processing the VarBind.
		ByteStream byteStream;
		byteStream.linkFrom( (char*)buffer, bufferSize, ByteStream::READ_ONLY ); //< TODO: Consider doing this in a "const" fashion.

		BerStream berStream( &byteStream );

		// Read the OID and its value.

		bool success = true;

		success = berStream.read<OBJECT_IDENTIFIER>( varbind->oid );
		if( ! success ) {
			return false;
		}
		
		uint8_t currentType;
		success = berStream.peekType( currentType );
		if( ! success ) {
			return false;
		}

		switch( currentType ) {
			/*
			 * ASN.1 native types.
			 */
			case INTEGER::TYPE: {
				int32_t value = 0;
				success = berStream.read<INTEGER>( value );
				varbind->value.set_int32_t( Variant::SIMPLE_INTEGER, value );
				break;
			}
			case OCTET_STRING::TYPE: {
				std::string value;
				success = berStream.read<OCTET_STRING>( value );
				varbind->value.set_string( Variant::SIMPLE_STRING, value );
				break;
			}
			case OBJECT_IDENTIFIER::TYPE: {
				NumericOid value;
				success = berStream.read<OBJECT_IDENTIFIER>( value );
				varbind->value.set_oid( Variant::SIMPLE_OID, value );
				break;
			}
			case NULL_TYPE::TYPE:
				varbind->value.set_null( Variant::SIMPLE_NULL );
				success = berStream.read<NULL_TYPE>( NULL_TYPE::throwaway );
				break;
			/*
			 * SNMP "application" types.
			 */
			case IPADDRESS::TYPE: {
				std::string value;
				success = berStream.read<IPADDRESS>( value );
				varbind->value.set_string( Variant::APPLICATION_IPADDRESS, value );
				break;
			}
			case COUNTER32::TYPE: {
				uint32_t value = 0;
				success = berStream.read<COUNTER32>( value );
				varbind->value.set_uint32_t( Variant::APPLICATION_COUNTER32, value );
				break;
			}
			case GAUGE32::TYPE: {
				uint32_t value = 0;
				success = berStream.read<GAUGE32>( value );
				varbind->value.set_uint32_t( Variant::APPLICATION_GAUGE32, value );
				break;
			}
			case TIMETICKS::TYPE: {
				uint32_t value = 0;
				success = berStream.read<TIMETICKS>( value );
				varbind->value.set_uint32_t( Variant::APPLICATION_TIMETICKS, value );
				break;
			}
			case OPAQUE::TYPE: {
				std::string value;
				success = berStream.read<OPAQUE>( value );
				varbind->value.set_string( Variant::APPLICATION_OPAQUE, value );
				break;
			}
			case COUNTER64::TYPE: {
				uint64_t value = 0;
				success = berStream.read<COUNTER64>( value );
				varbind->value.set_uint64_t( Variant::APPLICATION_COUNTER64, value );
				break;
			}

			/*
			 * SNMPv2 VarBind value choices (failure).
			 */
			case NOSUCHOBJECT::TYPE:
				varbind->value.set_null( Variant::CONTEXT_NOSUCHOBJECT );
				success = berStream.read<NOSUCHOBJECT>( NOSUCHOBJECT::throwaway );
				break;
			case NOSUCHINSTANCE::TYPE:
				varbind->value.set_null( Variant::CONTEXT_NOSUCHINSTANCE );
				success = berStream.read<NOSUCHINSTANCE>( NOSUCHINSTANCE::throwaway );
				break;
			case ENDOFMIBVIEW::TYPE:
				varbind->value.set_null( Variant::CONTEXT_ENDOFMIBVIEW );
				success = berStream.read<ENDOFMIBVIEW>( ENDOFMIBVIEW::throwaway );
				break;

			default:
				if( logger.system( Logger::DECODING ) ) {
					logger.out() << "Unknown type: " << (int)currentType << std::endl;
				}
				success = false;
		}
		if( ! success ) {
			return false;
		}

		return true;
	}
};

/**
 * This class allows for the encoding and decoding of VarBind lists.
 * Definition:
 *    SEQUENCE OF VarBind
 **/
class VARBINDLIST {
public:
	//! This is the name of this entity.
	static constexpr const char* NAME = "SEQUENCE OF VarBind";

	//! A VarBind list is a sequence:
	static const int TYPE = asn1::helper::SEQUENCE::TYPE;
	
	//! The value type that this class operates on is "VarBindList".
	typedef VarBindList value_type;

	static unsigned int length( const VarBindList& varbinds ) {
		unsigned int size = 0;
		for( const auto& varbind : varbinds ) {
			size += asn1::encodedSize<VARBIND>( varbind );
		}

		return size;
	}

	static bool write( const VarBindList& varbinds, char* buffer, unsigned int bufferSize ) {
		//! This is the ByteStream for processing the SEQUENCE OF VarBind.
		ByteStream byteStream;
		byteStream.linkFrom( buffer, bufferSize, ByteStream::READ_WRITE );

		BerStream berStream( &byteStream );

		for( const auto& varbind : varbinds ) {
			bool success = berStream.write<VARBIND>( varbind );
			if( ! success ) {
				return false;
			}
		}

		return true;
	}

	static bool read( value_type& varbinds, const char* buffer, unsigned int bufferSize ) {
		// Since a "SEQUENCE OF VarBind" is composed of multiple "VarBind" entries, we're going to
		// set up a ByteStream to handle it.
		
		//! This is the ByteStream for processing the SEQUENCE OF VarBind.
		ByteStream byteStream;
		byteStream.linkFrom( (char*)buffer, bufferSize, ByteStream::READ_ONLY ); //< TODO: Consider doing this in a "const" fashion.
		
		BerStream berStream( &byteStream );

		// Read each of the VarBinds.

		bool success = true;

		while( berStream.remainingReadLength() > 0 ) { //< TODO: Too much access?  Add more?
			std::unique_ptr<VarBind> varbind( new VarBind );
			success = berStream.read<VARBIND>( varbind );
			if( ! success ) {
				return false;
			}

			varbinds.push_back( std::move(varbind) );
		}

		return true;
	}
};

template<int tag,ConfirmedClass::Type confirmedClass>
class GenericPDU {
public:
	// Note: No "NAME" is specified, so this CANNOT be used for
	// encoding or decoding types.  You MUST create a class that
	// references this one as a templated parent.

	//! This is the type of the PDU.
	//! Note that the "tag" given to this class as a template
	//! value will determine the type of PDU.
	//! ---------------------------------
	//! | 8 | 7 | 6 | 5 | 4 | 3 | 2 | 1 |
	//! ---------------------------------
	//! |Class  |P/C| Tag Number        |
	//! ---------------------------------
	//! | 1 | 0 | 1 |               tag |
	//! ---------------------------------
	static constexpr const int TYPE = 0xa0 | tag;
	
	//! The value type that this class operates on is "PDU".
	typedef PDU value_type;

	//! This is the "confirmed class" of the PDU.
	static constexpr const ConfirmedClass::Type confirmed = confirmedClass;

	static unsigned int length( const value_type& pdu ) {
		unsigned int size = 0;
		size += asn1::encodedSize<INTEGER>( pdu.requestId );
		size += asn1::encodedSize<INTEGER>( pdu.errorStatus );
		size += asn1::encodedSize<INTEGER>( pdu.errorIndex );
		size += asn1::encodedSize<VARBINDLIST>( pdu.varbinds );
		return size;
	}

	static bool write( const value_type& pdu, char* buffer, unsigned int bufferSize ) {
		//! This is the ByteStream for processing the PDU.
		ByteStream byteStream;
		byteStream.linkFrom( (char*)buffer, bufferSize, ByteStream::READ_WRITE ); //< TODO: Consider doing this in a "const" fashion.
		
		BerStream berStream( &byteStream );

		bool success = true;
		
		// Write the header.

		success = berStream.write<INTEGER>( pdu.requestId );
		if( ! success ) {
			return false;
		}
		success = berStream.write<INTEGER>( pdu.errorStatus );
		if( ! success ) {
			return false;
		}
		success = berStream.write<INTEGER>( pdu.errorIndex );
		if( ! success ) {
			return false;
		}

		// Write the payload.

		success = berStream.write<VARBINDLIST>( pdu.varbinds );	
		if( ! success ) {
			return false;
		}

		return true;
	}

	static bool read( value_type& pdu, const char* buffer, unsigned int bufferSize ) {
		// Since a PDU is composed of multiple other entities, we're going to
		// set up a ByteStream to handle it.
		
		//! This is the ByteStream for processing the PDU.
		ByteStream byteStream;
		byteStream.linkFrom( (char*)buffer, bufferSize, ByteStream::READ_ONLY ); //< TODO: Consider doing this in a "const" fashion.
		
		BerStream berStream( &byteStream );

		// Read the header.

		bool success = true;

		success = berStream.read<INTEGER>( pdu.requestId );
		if( ! success ) {
			return false;
		}
		success = berStream.read<INTEGER>( pdu.errorStatus );
		if( ! success ) {
			return false;
		}
		success = berStream.read<INTEGER>( pdu.errorIndex );
		if( ! success ) {
			return false;
		}

		// Read the payload.

		success = berStream.read<VARBINDLIST>( pdu.varbinds );
		if( ! success ) {
			return false;
		}

		return true;
	}
};

/**
 * A GetRequest-PDU is a PDU with tag "0".
 **/
class GetRequestPDU : public GenericPDU<0,ConfirmedClass::Confirmed> {
public:
	static constexpr const char* NAME = "GetRequest-PDU";
};

/**
 * A GetNextRequest-PDU is a PDU with tag "1".
 **/
class GetNextRequestPDU : public GenericPDU<1,ConfirmedClass::Confirmed> {
public:
	static constexpr const char* NAME = "GetNextRequest-PDU";
};

/**
 * A Response-PDU is a PDU with tag "2".
 **/
class ResponsePDU : public GenericPDU<2,ConfirmedClass::Unconfirmed> {
public:
	static constexpr const char* NAME = "Response-PDU";
};

/**
 * A SetRequest-PDU is a PDU with tag "3".
 **/
class SetRequestPDU : public GenericPDU<3,ConfirmedClass::Confirmed> {
public:
	static constexpr const char* NAME = "SetRequest-PDU";
};

// NOTE: Trap-PDU is tag "4"; see RFC-1157.

// TODO: Technically, a #5 is a BulkPDU, but they are physically identical.
/**
 * A GetBulkRequest-PDU is a PDU with tag "5".
 **/
class GetBulkRequestPDU : public GenericPDU<5,ConfirmedClass::Confirmed> {
public:
	static constexpr const char* NAME = "GetBulkRequest-PDU";
};

/**
 * A InformRequest-PDU is a PDU with tag "6".
 **/
class InformRequestPDU : public GenericPDU<6,ConfirmedClass::Confirmed> {
public:
	static constexpr const char* NAME = "InformRequest-PDU";
};

/**
 * A SNMPv2-Trap-PDU is a PDU with tag "7".
 **/
class SnmpV2TrapPDU : public GenericPDU<7,ConfirmedClass::Unconfirmed> {
public:
	static constexpr const char* NAME = "SNMPv2-Trap-PDU";
};

/**
 * A Report-PDU is a PDU with tag "8".
 **/
class ReportPDU : public GenericPDU<8,ConfirmedClass::Unconfirmed> {
public:
	static constexpr const char* NAME = "Report-PDU";
};

} } }

