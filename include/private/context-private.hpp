#pragma once

#include "dangerous/snmp/context.hpp"

#include "iosystem.hpp"

namespace dangerous { namespace snmp {

class Context::PrivateData {
protected:
	PrivateData();

public:
	~PrivateData();

	IoSystem io;

	friend class Context;
};

} }

