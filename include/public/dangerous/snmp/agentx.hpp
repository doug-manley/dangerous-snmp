#pragma once

#include "context.hpp"
#include "exception.hpp"
#include "numericoid.hpp"
#include "variant.hpp"

#include <condition_variable>
#include <map>
#include <mutex>
#include <string>
#include <thread>

namespace dangerous { namespace snmp {

class AgentxPacket;
class Transport;


/**
 * An AgentX represents an AgentX sub-agent.  Such a sub-agent is loosely identified
 * by two things:
 *    1. An OID; and
 *    2. A description.

 * Neither is particularly necessary, but these help to distinguish (on the master
 * side) one agent from another.
 *
 * Each instance will have _its own thread_ for handling asynchronous operations
 * (such as those received from the master agent).  This is called the "mailbox"
 * thread.  This agent will be periodically receiving requests from the master agent;
 * those requests will be stored in the "inbox"; they will be answered appropriately
 * by the agent.
 *
 * Sometimes, these will call user-specified callbacks, sometimes they won't.
 **/
class AgentX {
public:
	//! This is the default value for the "priority" of an OID registered with
	//! the "Register" PDU.  This is defined in RFC-2741.
	static const uint8_t DEFAULT_PRIORITY;

	/**
	 * An OidRegistrationEntry represents an OID that has been registered.
	 * This contains:
	 * 1. The function to call when the OID is requested, which fills out the value.
	 * 2. The function to call when the next OID is requested, which fills out the next OID and its value.
	 *
	 * The exact behavior depends on the Type:
	 * * OBJECT and INSTANCE
	 *    * The OID passed to the GetFunction is the original registration OID.
	 *    * The GetNextFunction is ignored.
	 * * TREE
	 *    * The OID passed to the GetFunction is the full OID requested.
	 *    * The GetNextFunction is used.
	 **/
	class OidRegistrationEntry {
	public:
		/**
		 * The GetFunction is called when the value of an OID is requested.
		 *
		 * @tparam oid The OID whose value is requested.
		 * @tparam value The value of the OID; this should be updated by the function.
		 * @return true if value is valid; false otherwise.
		 **/
		typedef std::function<bool(const NumericOid& oid, Variant& value)> GetFunction;
		
		/**
		 * The GetNextFunction is called when the next OID is requested.
		 *
		 * @tparam oid The reference OID.
		 * @tparam nextOid The next OID after the reference OID; this should be updated by the function.
		 * @tparam value The value of the next OID, if it is valid.
		 * @return true if the next OID, and its value, are set; false if there is no next OID.
		 **/
		typedef std::function<bool(const NumericOid& oid, NumericOid& nextOid, Variant& value)> GetNextFunction;

		/**
		 * This represents the kind of OID that is being registered.
		 **/
		enum Type {
			//! This registration is for an individual OID.
			OBJECT,
			//! This registration is for an instance of a table-based OID.
			INSTANCE,
			//! This registration is for a tree; it will handle all OIDs *under* the given one.
			TREE
		};
	public:
		/**
		 * Constructor.
		 **/
		OidRegistrationEntry() : priority(0), type(OBJECT) {};

	public:
		//! This is the priority of the OID.
		//! The range is from 1 - 255, with a lower value having a higher priority than
		//! a higher value.  If this is set to zero, the the value will be AgentX::DEFAULT_PRIORITY.
		uint8_t priority;

		//! This is the type of registration TODO TODO TODO
		Type type;

		//! This function will be called when the value of the OID is requested.
		GetFunction getFunction;
		
		//! This function will be called when the next OID is requested.
		GetNextFunction getNextFunction;
	};

public:
	/**
	 * Constructor.
	 *
	 * @param context The Dangerous SNMP Context to use.
	 **/
	AgentX( const Context& context );

	/**
	 * Destructor.
	 *
	 * If the "mailbox" thread is running, then it is stopped.
	 **/
	virtual ~AgentX();

	/**
	 * This sets the URI for the client.  A URI is of the form:
	 *     {transport}://{address}
	 *
	 * Currently, the following transports are supported:
	 * 
	 * * udp
	 *    * The address is {ip address or hostname}[:{port number or name}]
	 * * tcp
	 *    * The address is {ip address or hostname}[:{port number or name}]
	 *
	 * When this is called, the transport is created, and a "mailbox" is created
	 * and started for the agent.
	 * 
	 * @param uri The URI to use.
	 * @throw ParseErrorException, UnknownTransportException, Exception
	 **/
	void uri( const std::string& uri ) throw( ParseErrorException, UnknownTransportException, Exception );

	/**
	 * This sets the identifier OID for the agent.
	 * This must be set _before_ a call to "open" in order to take effect.
	 *
	 * @param oid The new identifier OID.
	 **/
	void identifier( const NumericOid& oid ) { _identifier = oid; }

	/**
	 * This returns the identifier OID for the agent.
	 * @return The identifier OID.
	 **/
	NumericOid identifier() const { return _identifier; }

	/**
	 * This sets the description for the agent.
	 * This must be set _before_ a call to "open" in order to take effect.
	 * @param text The new description.
	 **/
	void description( const std::string& text ) { _description = text; }
	
	/**
	 * This returns the description for the agent.
	 * 
	 * @return The description.
	 **/
	std::string description() const { return _description; }

	/*
	 * Raw protocol operations.
	 */

	/**
	 * This sends an "AddAgentCaps" PDU to the master agent.
	 * This PDU tells the master agent about a "capability" of this agent.
	 * This information will be registered under "sysORID" and "sysORDescr".
	 *
	 * @param oid The OID that represents the capability.
	 * @param description The description.
	 **/
	void raw_addAgentCapabilities( const NumericOid& oid, const std::string& description );

	/**
	 * This sends a "RemoveAgentCaps" PDU to the master agent.
	 * This PDU tells the master agent that this agent no longer has
	 * the capability previously registered.
	 *
	 * @param oid The OID that represents the capability.
	 **/
	void raw_removeAgentCapabilities( const NumericOid& oid );
	
	/**
	 * This sends a "Register" PDU to the master agent.
	 * This PDU tells the master agent that this agent is responsible
	 * for any OIDs under the given OID, including that OID.
	 *
	 * @param oid The OID to register.
	 * @param priority The priority of the registration.  A lower value has a higher priority.
	 **/
	void raw_registerTree( const NumericOid& oid, uint8_t priority = DEFAULT_PRIORITY );

	/**
	 * This sends a "Register" PDU to the master agent with a flag
	 * signifying "instance" registration.
	 * This PDU tells the master agent that this agent is responsible
	 * for this exact OID.
	 *
	 * @param oid The OID to register.
	 * @param priority The priority of the registration.  A lower value has a higher priority.
	 **/
	void raw_registerOid( const NumericOid& oid, uint8_t priority = DEFAULT_PRIORITY );


	/*
	 * Logical protocol operations.
	 */

	/**
	 * This adds a new capability for the agent.
	 * If this capability has already been registered, then
	 * an exception will be thrown.
	 *
	 * @param oid The OID that represents the capability.
	 * @param description The description of the capability.
	 **/
	void addAgentCapabilities( const NumericOid& oid, const std::string& description ) throw( Exception );
	
	/**
	 * This removes a previously registered capability.
	 * If this capability has not already been registered, then
	 * an exception will be thrown.
	 *
	 * @param oid The OID that represents the capability.
	 * @throw Exception
	 **/
	void removeAgentCapabilities( const NumericOid& oid ) throw( Exception );

	/**
	 * This registers a new OID.
	 *
	 * @param oid The OID to register.
	 * @param entry The registration information.
	 * @throw Exception
	 **/
	void registerOid( const NumericOid& oid, const OidRegistrationEntry& entry ) throw ( Exception );

protected:
	//! This is a weak pointer to the Context.
	std::weak_ptr<Context::PrivateData> context;

	//! This is the OID that identifies the Agent; each Agent should have
	//! a unique identifying OID.  This will be given to the master agent
	//! upon registration.
	NumericOid _identifier;
	//! This is the description of the Agent.
	std::string _description;

	//! This is a pointer to the "Transport" instance that will be used
	//! to communicate with the AgentX "master".  This will be instantiated
	//! as a specific subclass of "Transport" when the URI is handled.
	std::unique_ptr<Transport> transport;

	//! This is a counter that is incremented for each packet _sent_ by
	//! this agent.
	uint32_t packetNumber;

	//! This is the session ID, as given by the master agent.
	uint32_t sessionId;

	/*
	 * Mailbox variables.
	 */

	//! This is the read mutex used for guaranteeing that only a single thread
	//! is working with the inbox for this agent at any one time.
	std::mutex readMutex;
	//! This is used to notify any waiting threads when a new packet is available
	//! from the inbox.
	std::condition_variable readConditionVariable;

	//! This is the write mutex used for guaranteeing that only a single thread
	//! is speaking for this agent at any one time.
	std::mutex writeMutex;

	//! This represents an "inbox" of packets received from the master agent.
	//! This maps a packet ID to its packet.
	std::map< uint32_t, std::unique_ptr<AgentxPacket> > inbox;

	//! This is whether or not the mailbox thread is "stopped"; if the thread has
	//! not been started, then it is stopped.
	bool isStopped;
	//! This is the thread that runs the main loop for handling the mailbox.
	std::thread mailboxThread;

	/*
	 * Capabilities.
	 */

	/**
	 * A CapabilityMap maps an OID to a string.
	 **/
	typedef std::map< NumericOid, std::string > CapabilityMap;

	//! This is the mapping of capability OID to its description; these are
	//! created by adding a new "capability" to the agent.
	CapabilityMap capabilityMap;

	/*
	 * OID registration.
	 */
	
	/**
	 * An OidRegistrationMap maps an OID to its registration entry.
	 **/
	typedef std::map< NumericOid, OidRegistrationEntry > OidRegistrationMap;

	//! This is the mapping of an OID to its registration entry.
	//! When operations are requested of us (Get, GetNext, GetBulk, Set),
	//! this will be consulted to (1) find the OID, and (2) perform the
	//! necessary action.
	OidRegistrationMap oidRegistrationMap;

	/**
	 * This returns the "oidRegistrationMap" iterator whose entry would be responsible
	 * for the OID given.
	 *
	 * If the responsible entry is an OBJECT or INSTANCE registration, then its OID
	 * will match the one given exactly.
	 *
	 * If the responsible entry is a TREE registration, then its OID will be the
	 * beginning of the one given.
	 *
	 * @param oid The OID to search for.
	 * @return The iterator to the entry in "oidRegistrationMap", or "oidRegistrationMap.end()" if it is not covered.
	 **/
	OidRegistrationMap::const_iterator registrationThatCovers( const NumericOid& oid );
	
	/**
	 * This reads an AgentxPacket from the transport.
	 *
	 * @param packet The packet in which to store the data.
	 **/
	void rawRead( AgentxPacket& packet );
	
	/**
	 * This writes an AgentxPacket to the transport.
	 *
	 * @param packet The packet to write.
	 **/
	void rawWrite( AgentxPacket& packet );

	/**
	 * This connects the agent to the master agent.  The identifier OID
	 * and description are sent to the master agent, and the "open" command
	 * is issued.
	 *
	 * A session ID is obtained from the master agent, and any pending communication
	 * with it is voided.
	 **/
	void open();
	
	/**
	 * This disconnects the agent from the master agent.
	 **/
	void close();


	/*
	 * Mailbox functions.
	 */

	/**
	 * This starts the mailbox thread, if it has not already been started.
	 **/
	void startMailboxThread();

	/**
	 * This stops the mailbox thread, if it has not already been stopped.
	 **/
	void stopMailboxThread();

	/**
	 * This attempts to obtain the packet, with the specified ID, from the
	 * mailbox.  If there is no such packet, then this will wait for at
	 * most 5 seconds before failing.
	 *
	 * Exception conditions:
	 * * The transport is not connected.
	 * * The packet could not be found within the time limit.
	 * 
	 * @param packet The packet to store the data from the mailbox.
	 * @param packetId The ID of the packet to retrieve.
	 * @throw Exception
	 **/
	void mailboxRead( AgentxPacket& packet, uint32_t packetId ) throw( Exception );

	/**
	 * This is the main loop for the mailbox thread.
	 **/
	void mailboxMain();
};

} }

