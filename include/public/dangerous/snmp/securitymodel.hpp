#pragma once

#include <string>

namespace dangerous { namespace snmp {

/**
 * This class is the base class for all security models.
 * A security model is basically just a way of describing
 * the security (however weak) of the SNMP communication.
 *
 * See RFC-3411.
 *
 * There are three standardized models:
 *    1. SNMPv1
 *       This is basically just stating that whatever SNMPv1
 *       did in terms of security (community strings), then
 *       this is that.
 *    2. SNMPv2
 *       This is basically just stating that whatever SNMPv2
 *       did in terms of security (community strings), then
 *       this is that.
 *    3. User-based Security Model (USM)
 *       This is the model that is _required_ to be supported
 *       by any SNMPv3 agent.
 *
 * Others are possible (including enterprise-specific ones).
 **/
class SecurityModel {
public:
	/**
	 * This identifies the security model.
	 **/
	enum Type {
		ANY = 0,
		SNMPv1 = 1,
		SNMPv2c = 2,
		USM = 3
	};

public:
	virtual ~SecurityModel() {}

	/**
	 * This returns the identifier of the security model that this
	 * instance represents.
	 * @return The type of security model.
	 **/
	Type type() const { return _type; }

	//! TODO: THIS
	virtual bool isAuthenticated() const = 0;

protected:
	//! This is the type of security model that this particular
	//! intance represents.
	Type _type;
};

} }

