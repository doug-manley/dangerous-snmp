#pragma once

#include <memory>

namespace dangerous { namespace snmp {

/**
 * A Dangerous SNMP "Context" is the backing for the rest of the
 * Dangerous SNMP libraries.  Its primary goal is to provide the
 * input/output system that is used for network communication.
 *
 * Typically, an application will need only a single context; however,
 * multiple contexts can coexist, so if a library uses Dangerous SNMP,
 * your application won't need to worry about conflicting settings
 * or global variables.  Everything necessary for Dangerous SNMP will
 * be contained here.
 **/
class Context {
public:
	/**
	 * Constructor.
	 * TODO
	 **/
	Context();
	/**
	 * Desctuctor.
	 * TODO
	 **/
	~Context();

public:
	class PrivateData;

protected:
	//! TODO: WHAT IS THIS
	std::shared_ptr<PrivateData> privateData;

	//! TODO: WHY?
	friend class Client;
	//! TODO: WHY?
	friend class AgentX;
};

} }

