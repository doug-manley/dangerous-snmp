#pragma once

#include <chrono>
#include <list>
#include <memory>
#include <string>
#include <stdexcept>

#include "context.hpp"
#include "exception.hpp"
#include "rawresponse.hpp"
#include "response.hpp"

#include "securitymodel.hpp"
#include "securitymodel/usm.hpp"
#include "types.hpp"

namespace dangerous { namespace snmp {

class ByteStream;
class MessageProcessor;
class Transport;


/**
 * A Client instance is used to communicate with an SNMP agent.
 *
 * In order to use a Client, you must first provide it with a URI for the
 * connection.  This URI will be used to determine the transport for the
 * connection, as well as the address of the agent.
 *
 * For example, you can connect to the local machine over UDP by using
 * "udp://127.0.0.1".
 *
 * The following transports are currently supported:
 * * tcp
 * * udp
 *
 * In addition to the transport, you must specify the version of SNMP to
 * use.  There are currently three available versions of SNMP; each have
 * benefits and drawbacks, and their use (in the case of a Client) will
 * depend entirely on the version used by the agent in question.
 *
 * SNMPv1 and SNMPv2c both use the concept of a "community string" that
 * will be validated by the agent.  Different community strings may be
 * given different access rights, as well.
 *
 * SNMPv3 supports a modular access control method, but the standard one
 * is the "User Security Model" (USM).  Here, a user name is required,
 * and additional aspects of the communication can be specified (such as
 * packet authentication and encryption).
 *
 * ----
 *
 * A Client may communicate via two kinds of operations:
 * 1. Raw operations, which represent simple protocol-level operations; and
 * 2. Logical operations, which represent logical operations that are built
 *    on the protocol-level ones.
 **/
class Client {
public:
	/**
	 * This constructs a new Client.
	 * Before it can be used, "uri" must be called.
	 *
	 * @param context The Dangerous SNMP Context to use.
	 **/
	Client( const Context& context );

	/**
	 * Destructor.
	 **/
	virtual ~Client();

	/**
	 * This returns the URI for the connection.
	 * If the connection has no URI, then the string returned will have
	 * zero length.
	 *
	 * @return The URI for the connection.
	 **/
	std::string uri() const;
	/**
	 * This sets the URI for the client.  A URI is of the form:
	 *    {transport}://{address}
	 * Currently, the following transports are supported:
	 *    udp
	 *       The address is {ip address or hostname}[:{port number or name}]
	 *    tcp
	 *       The address is {ip address or hostname}[:{port number or name}]
	 *
	 * @param uri The URI to use.
	 * @throw ParseErrorException, UnknownTransportException, Exception
	 **/
	void uri( const std::string& uri ) throw( ParseErrorException, UnknownTransportException, Exception );

	/**
	 * This returns the timeout for the connection.
	 *
	 * @return The timeout.
	 **/
	std::chrono::milliseconds timeout() const { return _timeout; }
	/**
	 * This sets the timeout for the connection.
	 *
	 * @param timeout The timeout.
	 **/
	void timeout( const std::chrono::milliseconds& timeout ) { _timeout = timeout; }

	/**
	 * This configures the client for SNMPv1.
	 *
	 * @param communityString The community string.
	 **/
	void useVersion1( const std::string& communityString );
	
	/**
	 * This configures the client for SNMPv2c.
	 *
	 * @param communityString The community string.
	 **/
	void useVersion2c( const std::string& communityString );

	/**
	 * This configures the client for SNMPv3/USM.
	 *
	 * @param securityModel The security information to use.
	 **/
	void useVersion3( const securitymodel::USM& securityModel );

	/**
	 * TODO: ?????????
	 * 
	 * @return TODO
	 **/
	bool isAuthenticated();

	/**
	 * TODO: ?????????
	 **/
	void authenticate();

	/*
	 * Raw protocol operations.
	 *
	 * These can be used to speak strict SNMP without the extra "magic" that we
	 * would otherwise do (such as retrying requests).
	 *
	 * All of these functions begin with "raw_".
	 */
	
	/**
	 * This issues a "get" request.
	 * The PDU will be sent as-is (with the exception of:
	 *    * requestId; this will be set internally.
	 *
	 * Note that if this returns a Report PDU, then _that_ PDU will be
	 * in the response.
	 *
	 * @param requestPdu The PDU to send.
	 * @return A unique pointer to a RawResponse.
	 * @throw Exception
	 **/
	std::unique_ptr<RawResponse> raw_get( PDU& requestPdu ) throw( Exception );

	/**
	 * This issues a "getnext" request.
	 * The PDU will be sent as-is (with the exception of:
	 *    * requestId; this will be set internally.
	 *
	 * Note that if this returns a Report PDU, then _that_ PDU will be
	 * in the response.
	 *
	 * @param requestPdu The PDU to send.
	 * @return A unique pointer to a RawResponse.
	 * @throw Exception
	 **/
	std::unique_ptr<RawResponse> raw_getnext( PDU& requestPdu ) throw( Exception );

	/**
	 * This issues a "getbulk" request.
	 * The PDU will be sent as-is (with the exception of:
	 *    * requestId; this will be set internally.
	 *
	 * Note that if this returns a Report PDU, then _that_ PDU will be
	 * in the response.
	 *
	 * @param requestPdu The PDU to send.
	 * @return A unique pointer to a RawResponse.
	 * @throw Exception
	 **/
	std::unique_ptr<RawResponse> raw_getbulk( PDU& requestPdu ) throw( Exception );

	/**
	 * This issues a "set" request.
	 * The PDU will be sent as-is (with the exception of:
	 *    * requestId; this will be set internally.
	 *
	 * Note that if this returns a Report PDU, then _that_ PDU will be
	 * in the response.
	 *
	 * @param requestPdu The PDU to send.
	 * @return A unique pointer to a RawResponse.
	 * @throw Exception
	 **/
	std::unique_ptr<RawResponse> raw_set( PDU& requestPdu ) throw( Exception );

	/*
	 * Logical operations.
	 *
	 * These operations do what is usually _expected_ of SNMP; they may re-issue
	 * a request, or they may break a PDU into multiple parts (for example), in
	 * order to get the best results.
	 */

	/**
	 * This issues a simple "get" request.
	 *
	 * @param oidList A list of OIDs to get.
	 * @return A Response instance.
	 **/
	std::unique_ptr<Response> get( const std::list<NumericOid>& oidList ) throw( Exception );
	
	/**
	 * This issues a simple "getnext" request.
	 *
	 * @param oidList A list of OIDs to get.
	 * @return A Response instance.
	 **/
	std::unique_ptr<Response> getNext( const std::list<NumericOid>& oidList );

	/**
	 * This issues a "getbulk" request.
	 *
	 * @param singleRequestOids A list of OIDs (strings) to get as in "getnext".
	 * @param multipleRequestLimit The number of times that "multipleRequestOids" should be requested.
	 * @param multipleRequestOids A list of OIDs (strings) to get as in "getnext", but repeated "multipleRequestLimit" times.
	 * @return A Response instance.
	 **/
	std::unique_ptr<Response> getBulk( const std::list<NumericOid>& singleRequestOids, unsigned int multipleRequestLimit, const std::list<NumericOid>& multipleRequestOids );
	
	/**
	 * This issues a "set" request.
	 * Due to the nature of the SNMP "set" command, this will be almost
	 * trivially equivalent to the "raw_set" method.  That is, a "set"
	 * command _must_ be issued atomically, so there will be no special
	 * actions taken within this method.
	 *
	 * @param varbinds A VarBindList containing the OIDs (and their respective values) to set.
	 * @return A Response instance.
	 **/
	std::unique_ptr<Response> set( const VarBindList& varbinds ) throw( Exception );

	/**
	 * This performs a "walk" operation.  A walk is a conceptual operation that
	 * takes an OID as a starting point and returns a list of OIDs (and their values)
	 * that are "under" the starting OID.  That is, the results are guaranteed to
	 * _begin_ with exactly the same OID sequence that was given.
	 *
	 * This operation is conceptually understood, in the simplest case, as performing
	 * "getnext" requests until the resulting OID no longer begins with the starting
	 * OID.
	 *
	 * This operation also includes the concept of "paging"; that is, a limit can be
	 * given to limit the number of OIDs that are returned.  If the limit is reached,
	 * then an additional call can be made using the last OID in the result as the
	 * "startingOid" for the new operation.
	 *
	 * @param oid The starting OID.
	 * @param limit If set, then this will return only this many results.
	 * @param startingOid If set, then this will begin the walk starting from this OID.
	 * @return A Response instance.
	 **/
	std::unique_ptr<Response> walk( const NumericOid& oid, unsigned int limit = 0, const NumericOid& startingOid = NumericOid() );

protected:
	/**
	 * Internal use only.
	 * This encodes and sends a PDU with the appropriate security model.
	 *
	 * @tparam PduEncoder The static encoder class to use to encode the PDU.
	 * @param outgoingSecurityModel TODO
	 * @param request The PDU to send.
	 * @throw Exception
	 **/
	template<typename PduEncoder>
	void lowLevelSend( SecurityModel* outgoingSecurityModel, PDU& request ) throw( Exception );

	/**
	 * Internal use only.
	 * This receives a ByteStream and updates the "incomingSecurityModel" with the
	 * relevant security information.
	 *
	 * @param incomingSecurityModel The security model to update.
	 * @return A ByteStream pointer that can be used to extract a PDU.  This is _never_ nullptr.
	 * @throw Exception
	 **/
	std::unique_ptr<ByteStream> lowLevelReceive( std::unique_ptr<SecurityModel>& incomingSecurityModel ) throw( Exception );

	/**
	 * Internal use only.
	 * This performs a "basic" send and receive combination.
	 * This is used by the major user-facing operations.
	 *
	 * @tparam PduEncoder The static encoder class to use to encode the PDU.
	 * @param request The PDU to send.
	 * @param desiredSecurityModel If this is null, then the Client's security model is used.  Otherwise, the one specified here will be used.
	 * @return A pointer to a RawResponse.  This will _never_ be nullptr.
	 * @throw Exception
	 **/
	template<typename PduEncoder>
	std::unique_ptr<RawResponse> basicRequest( PDU& request, SecurityModel* desiredSecurityModel = nullptr ) throw( Exception );

protected:
	//! This is a weak pointer to the Context.
	std::weak_ptr<Context::PrivateData> context;

	//! This is a pointer to the "Transport" instance that will be used
	//! to communicate with the SNMP agent.  This will be instantiated
	//! as a specific subclass of "Transport" when the URI is handled.
	std::unique_ptr<Transport> transport;

	//! This is a pointer to the "MessageProcessor" instance that will
	//! be handling the SNMP version envelope.  This will be instantiated
	//! as a specific subclass of "MessageProcessor" when the version
	//! is specified.
	std::unique_ptr<MessageProcessor> messageProcessor;

	//! This is the kind of security model that is being used.  For
	//! SNMP versions 1 and 2c, this is fairly trivial.  For version 3,
	//! things get a bit more exciting.
	std::unique_ptr<SecurityModel> securityModel;

	//! This is the current request ID.  This will be incremented for
	//! each request.
	int32_t requestId;

	//! This is the timeout for the connection.
	std::chrono::milliseconds _timeout;

	//! This is the URI that was given for the connection.
	std::string _uri;
};

} }

