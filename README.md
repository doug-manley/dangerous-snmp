Dangerous SNMP    {#index}
==============

Dangerous SNMP is the awesome way to use SNMP for Linux.

IANA number for Dangerous Linux: 41326.

What's done?

* libdangerous-snmp-base
    * I/O system
    * General data structures needed for all SNMP libraries.
        * BerStream
        * ByteStream
        * Context
        * NumericOid
    * Transport.
        * UDP.
        * TCP.

* libdangerous-snmp-client
    * Protocol operations.
        * "get".
        * "getnext".
        * "getbulk".
        * "set".
    * Logical operations.
        * "get"
        * "getnext"
        * "getbulk"
        * "walk"
    * SNMPv1.
    * SNMPv2c.
    * SNMPv3/USM.
        * No authentication.
        * HMAC-MD5-96 authentication (outgoing).
        * HMAC-MD5-96 authentication (incoming).
        * HMAC-SHA-96 authentication (outgoing).
        * HMAC-SHA-96 authentication (incoming).
        * CBC-DES encryption (outgoing).
        * CBC-DES encryption (incoming).
        * CFB128-AES-128 encryption (outgoing).
        * CFB128-AES-128 encryption (incoming).

* libdangerous-snmp-agentx
    * Basic open/close functionality.
    * SysOR registration ("add capabilities").
    * Fully-qualified OID registration.
    * OID tree registration.
    * Reconnect on failure.
    * Re-register on reconnect.

* examples
    * dangerous-snmp-agentx-process
        * Example implementation of DANGEROUS-LINUX-HOST-MIB.
    * snmp-client
    * test-agentx

What else is on the list?

* SNMPv3/USM.
    * Local notion of "client" engine.
    * Local notion of "agent" engine.
    * Local cache of "agent" engine information.
    * Triple-DES EDE encryption, usm3DESEDEPrivProtocol, defined in http://tools.ietf.org/html/draft-reeder-snmpv3-usm-3desede-00

* libdangerous-snmp-client
    * Support for various counters and such for statistics.
    * Sending traps.
    * Sending informs.
    * Consider using a mailbox-based approach, as in AgentX, to handle out-of-order messages, etc.

* libdangerous-snmp-agentx
    * More features.

* Agent libraries.
* MIB libraries. 
* More executables.
* Clean code.
* SNMPv2u (if it's easy).

## Command-line tools

The only useful command-line tool provided right now is "snmp-client".

### snmp-client

"snmp-client" is a general purpose SNMP command-line utility for communicating with
an SNMP agent.  It currently has support for the following operations:

* "get"
* "getnext"
* "getbulk"
* "walk"
* "raw-get"
* "raw-getnext"
* "raw-getbulk"

#### Examples

Simple SNMP "get" operation.

	snmp-client -v2c -cpublic udp://localhost get .1.3.6.1.2.1.1.1.0

Simple SNMP "getnext" operation.

	snmp-client -v2c -cpublic udp://localhost getnext .1.3.6.1.2.1.1.1.0

Trivial SNMP "getbulk" operation.

	snmp-client -v2c -cpublic udp://localhost getbulk .1.3.6.1.2.1.1.1.0

Useful SNMP "getbulk" operation.

	snmp-client -v2c -cpublic udp://localhost getbulk .1.3.6.1.2.1.1.1.0 --repetitions 20 1.3.6.1.2.1.2.2.1.1

A typical SNMP "walk" operation.

	snmp-client -v2c -cpublic udp://localhost walk .1.3.6.1.2.1

### test-agentx

This is only around for testing AgentX functionality as it's developed.

## MIBs

For information on /proc/PID/io, see this site: http://docs.1h.com/Proc_I/O_Explained

Current MIBs include:

* DANGEROUS-LINUX-MIB
* DANGEROUS-LINUX-HOST-MIB

## Compiling

### Ubuntu
The following packages are required to compile Dangerous SNMP:

* doxygen
* g++
* git
* libssl-dev
* libunittest++-dev

## Testing and such

If you're testing, then you may want to use "tshark" to inspect the SNMP
packets on the command line to make sure that things are looking the way
that they should.

I've been using this command to inspect the traffic to my local net-snmp
agent:

	sudo tshark -i lo port 161 -V -O snmp

And this one to test AgentX:

	sudo tshark -i lo port 705 -V -O agentx

To see the registered OIDs (and their sub-agents) on a Net-SNMP agentx master agent,
run this command:

	snmpwalk -v2c -cpublic localhost .1.3.6.1.4.1.8072.1.2

