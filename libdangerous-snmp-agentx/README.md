# libdangerous-snmp-agentx

AgentX is a protocol defined in [RFC-2257](http://www.ietf.org/rfc/rfc2257.txt) (but revised in [RFC-2741](http://www.ietf.org/rfc/rfc2741.txt))
that enables SNMP-related information (essentially, parts of the overall OID tree) to be communicated
between a "master" agent and other processes.

This allows for practically trivially arbitrarily extending the SNMP agent without needed to modify it at all.

## TODO

### IMMEDIATELY

* Add support for the re-registration of OidRegistrationEntry entries upon reconnect.
* Remove "oidMap" and all of its related code.
* Figure out how to implement the process agent using the new system.

### Evantually

* Keep track of the registered OID (trees) and how to obtain their values (address, function, etc.).

## Implementation

### Mailbox system

AgentX is _full duplex_; this means that you may _receive_ a request to perform some action at any time,
on the same socket where you might be waiting for a _response_ to some request that _you_ made previously.

So, what we've created is a "mailbox" system.  All _outgoing_ messages are handled normally; they are just
written to the "writer" stream.  However, all _incoming_ messages are processed by a "mailbox" thread.  If
the message is a _response_ to some previous request, then it is added to an "inbox", and any calls waiting
for that inbox are woken up.  If it is a _request_ for the agent to provide some kind of response, then that
request is handled immediately, and a response is sent.

