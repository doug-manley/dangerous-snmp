#pragma once

#include "dangerous/snmp/numericoid.hpp"

namespace dangerous { namespace snmp {

class NumericOidRange {
public:
	NumericOid start;
	bool startInclusive;
	NumericOid end;
	bool endInclusive;

public:
	NumericOidRange() : startInclusive(true), endInclusive(false) {}
};

} };

