#pragma once

#include "bytestream.hpp"
#include "pdu.hpp"


namespace dangerous { namespace snmp { namespace encoding { namespace agentx {

class Header {
public:
	enum Version {
		VERSION_1 = 1
	};

	enum Flags {
		INSTANCE_REGISTRATION = 0b00000001,
		NEW_INDEX = 0b00000010,
		ANY_INDEX = 0b00000100,
		NON_DEFAULT_CONTEXT = 0b00001000,
		NETWORK_BYTE_ORDER = 0b00010000,
		RESERVED_5 = 0b00100000,
		RESERVED_6 = 0b01000000,
		RESERVED_7 = 0b10000000
	};
public:
	Version version;
	PDU::Type type;
	uint8_t flags;

	uint32_t sessionId;
	uint32_t transactionId;
	uint32_t packetId;

	uint32_t payloadLength;

public:
	Header() {
		version = VERSION_1;
		type = (PDU::Type)0;
		flags = 0;
		sessionId = 0;
		transactionId = 0;
		packetId = 0;
		payloadLength = 0;
	}
};

class HEADER {
public:
	static constexpr const char* NAME = "Header";
	
	//! This class operates on a Header.
	typedef Header value_type;
		
	static bool write( const value_type& value, ByteStream& byteStream ) {
		char version = (char)(int)value.version;
		bool success = byteStream.writeByte( version );
		if( ! success ) {
			return false;
		}

		char type = (char)(int)value.type;
		success = byteStream.writeByte( type );
		if( ! success ) {
			return false;
		}

		char flags = (char)value.flags;
		success = byteStream.writeByte( flags );
		if( ! success ) {
			return false;
		}

		char zero = 0x00;
		success = byteStream.writeByte( zero );
		if( ! success ) {
			return false;
		}

		unsigned int bytesWritten = 0;

		success = byteStream.writeBytes( (char*)&value.sessionId, sizeof(value.sessionId), bytesWritten );
		if( ! success ) {
			return false;
		}
		success = byteStream.writeBytes( (char*)&value.transactionId, sizeof(value.transactionId), bytesWritten );
		if( ! success ) {
			return false;
		}
		success = byteStream.writeBytes( (char*)&value.packetId, sizeof(value.packetId), bytesWritten );
		if( ! success ) {
			return false;
		}
		success = byteStream.writeBytes( (char*)&value.payloadLength, sizeof(value.payloadLength), bytesWritten );
		if( ! success ) {
			return false;
		}

		return true;
	}

	static bool read( value_type& value, ByteStream& byteStream ) {
		char version = 0x00;
		bool success = byteStream.readByte( version );
		if( ! success ) {
			return false;
		}

		value.version = (Header::Version)version;

		char type = 0x00;
		success = byteStream.readByte( type );
		if( ! success ) {
			return false;
		}

		value.type = (PDU::Type)type;

		char flags = 0x00;
		success = byteStream.readByte( flags );
		if( ! success ) {
			return false;
		}

		value.flags = flags;

		char zero = 0x00;
		success = byteStream.readByte( zero );
		if( ! success ) {
			return false;
		}

		unsigned int bytesRead = 0;

		success = byteStream.readBytes( (char*)&value.sessionId, sizeof(value.sessionId), bytesRead );
		if( ! success ) {
			return false;
		}
		success = byteStream.readBytes( (char*)&value.transactionId, sizeof(value.transactionId), bytesRead );
		if( ! success ) {
			return false;
		}
		success = byteStream.readBytes( (char*)&value.packetId, sizeof(value.packetId), bytesRead );
		if( ! success ) {
			return false;
		}
		success = byteStream.readBytes( (char*)&value.payloadLength, sizeof(value.payloadLength), bytesRead );
		if( ! success ) {
			return false;
		}

		return true;
	}
};
	
} } } }

