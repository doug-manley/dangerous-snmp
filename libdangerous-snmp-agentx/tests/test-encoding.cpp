#include "dangerous/snmp/numericoid.hpp"
#include "bytestream.hpp"
#include "../encoding/agentx/agentx.hpp"
using namespace dangerous;

#include <cstring>
#include <iostream>

#include <unittest++/UnitTest++.h>

SUITE(Fast) {

TEST(TestEncodingNumericOid) {
	snmp::ByteStream byteStream;

	const char* OID_TEXT = ".1.3.6.1.2.1.6.13.1.1.0.0.0.0.46038.0.0.0.0.0";
	snmp::NumericOid oid( OID_TEXT );
	CHECK( snmp::encoding::agentx::OBJECT_IDENTIFIER::write( oid, byteStream ) );

	snmp::NumericOid testOid;
	CHECK( snmp::encoding::agentx::OBJECT_IDENTIFIER::read( testOid, byteStream ) );

	CHECK( oid.str().compare( OID_TEXT ) == 0 );
	CHECK( oid.equals( testOid ) );
	CHECK( testOid.str().compare( OID_TEXT ) == 0 );
}

TEST(TestEncodingNumericOidRfc2741_1) {
	snmp::ByteStream byteStream;

	const char* OID_TEXT = ".1.3.6.1.2.1.1.1.0";
	snmp::NumericOid oid( OID_TEXT );

	CHECK( snmp::encoding::agentx::OBJECT_IDENTIFIER::write( oid, byteStream ) );
	CHECK( byteStream.remainingReadLength() == 20 );
	CHECK( byteStream.compareWith( "\x04\x02\x00\x00" "\x01\x00\x00\x00" "\x01\x00\x00\x00" "\x01\x00\x00\x00" "\x00\x00\x00\x00", 20 ) == 0 );

	snmp::NumericOid testOid;
	CHECK( snmp::encoding::agentx::OBJECT_IDENTIFIER::read( testOid, byteStream ) );

	CHECK( oid.str().compare( OID_TEXT ) == 0 );
	CHECK( oid.equals( testOid ) );
	CHECK( testOid.str().compare( OID_TEXT ) == 0 );
}

TEST(TestEncodingNumericOidRfc2741_2) {
	snmp::ByteStream byteStream;

	const char* OID_TEXT = ".1.2.3.4";
	snmp::NumericOid oid( OID_TEXT );

	CHECK( snmp::encoding::agentx::OBJECT_IDENTIFIER::write( oid, byteStream ) );
	CHECK( byteStream.remainingReadLength() == 20 );
	CHECK( byteStream.compareWith( "\x04\x00\x00\x00" "\x01\x00\x00\x00" "\x02\x00\x00\x00" "\x03\x00\x00\x00" "\x04\x00\x00\x00", 20 ) == 0 );

	snmp::NumericOid testOid;
	CHECK( snmp::encoding::agentx::OBJECT_IDENTIFIER::read( testOid, byteStream ) );

	CHECK( oid.str().compare( OID_TEXT ) == 0 );
	CHECK( oid.equals( testOid ) );
	CHECK( testOid.str().compare( OID_TEXT ) == 0 );
}

TEST(TestEncodingNumericOidRfc2741_null) {
	snmp::ByteStream byteStream;

	const char* OID_TEXT = "";
	snmp::NumericOid oid( OID_TEXT );

	CHECK( snmp::encoding::agentx::OBJECT_IDENTIFIER::write( oid, byteStream ) );
	CHECK( byteStream.remainingReadLength() == 4 );
	CHECK( byteStream.compareWith( "\x00\x00\x00\x00", 4 ) == 0 );

	snmp::NumericOid testOid;
	CHECK( snmp::encoding::agentx::OBJECT_IDENTIFIER::read( testOid, byteStream ) );

	CHECK( oid.str().compare( OID_TEXT ) == 0 );
	CHECK( oid.equals( testOid ) );
	CHECK( testOid.str().compare( OID_TEXT ) == 0 );
}

TEST(TestOctetString_1) {
	snmp::ByteStream byteStream;

	std::string emptyString = "";

	CHECK( snmp::encoding::agentx::OCTET_STRING::write( emptyString, byteStream ) );
	CHECK( byteStream.remainingReadLength() == 4 );
	CHECK( byteStream.compareWith( "\x00\x00\x00\x00", 4 ) == 0 );

	std::string testString;
	CHECK( snmp::encoding::agentx::OCTET_STRING::read( testString, byteStream ) );

	CHECK( emptyString.compare( testString ) == 0 );
}

TEST(TestOctetString_2) {
	snmp::ByteStream byteStream;

	std::string emptyString = "test";

	CHECK( snmp::encoding::agentx::OCTET_STRING::write( emptyString, byteStream ) );
	CHECK( byteStream.remainingReadLength() == 8 );
	CHECK( byteStream.compareWith( "\x04\x00\x00\x00" "test", 8 ) == 0 );

	std::string testString;
	CHECK( snmp::encoding::agentx::OCTET_STRING::read( testString, byteStream ) );

	CHECK( emptyString.compare( testString ) == 0 );
}

TEST(TestOctetString_3) {
	snmp::ByteStream byteStream;

	std::string emptyString = "test1";

	CHECK( snmp::encoding::agentx::OCTET_STRING::write( emptyString, byteStream ) );
	CHECK( byteStream.remainingReadLength() == 12 );
	CHECK( byteStream.compareWith( "\x05\x00\x00\x00" "test" "1\x00\x00\x00", 12 ) == 0 );

	std::string testString;
	CHECK( snmp::encoding::agentx::OCTET_STRING::read( testString, byteStream ) );

	CHECK( emptyString.compare( testString ) == 0 );
}

TEST(TestVarBind_1) {
	snmp::ByteStream byteStream;

	snmp::VarBind varbind;
	varbind.oid = snmp::NumericOid( ".1.2" );
	varbind.value.set_null( snmp::Variant::SIMPLE_NULL );

	CHECK( snmp::encoding::agentx::VARBIND::write( varbind, byteStream ) );
	CHECK( byteStream.remainingReadLength() == 16 );
	CHECK( byteStream.compareWith( "\x05\x00\x00\x00" "\x02\x00\x00\x00" "\x01\x00\x00\x00" "\x02\x00\x00\x00", 16 ) == 0 );

	snmp::VarBind testVarbind;
	CHECK( snmp::encoding::agentx::VARBIND::read( testVarbind, byteStream ) );

	CHECK( testVarbind.value.type() == snmp::Variant::SIMPLE_NULL );
}

TEST(TestVarBind_2) {
	snmp::ByteStream byteStream;

	snmp::VarBind varbind;
	varbind.oid = snmp::NumericOid( ".1.2" );
	varbind.value.set_int32_t( snmp::Variant::SIMPLE_INTEGER, 0xbeef );

	CHECK( snmp::encoding::agentx::VARBIND::write( varbind, byteStream ) );
	CHECK( byteStream.remainingReadLength() == 20 );
	CHECK( byteStream.compareWith( "\x02\x00\x00\x00" "\x02\x00\x00\x00" "\x01\x00\x00\x00" "\x02\x00\x00\x00" "\xef\xbe\x00\x00", 20 ) == 0 );

	snmp::VarBind testVarbind;
	CHECK( snmp::encoding::agentx::VARBIND::read( testVarbind, byteStream ) );

	CHECK( testVarbind.value.type() == snmp::Variant::SIMPLE_INTEGER );
	CHECK( varbind.oid.equals( testVarbind.oid ) );
	CHECK( testVarbind.value.get_int32_t() == 0xbeef );
}

}

